/*
 * Created on Aug 10, 2016
 *
 */
package potentialModelsGP.tree;

import java.util.ArrayList;

import evaluator.NotFiniteValueException;
import evaluator.StructureNet.Bond;
import matsci.structure.Structure;

public class DistanceNode extends Node {

  public DistanceNode() {
	  super(new Node[0]);
//    super(new Node[] {new ConstantNode(1.0)});
  }
  
//  public DistanceNode(Node node) {
//	super(new Node[] {node});
//  }
  
  public String toString() {
    return " bondLength ";
  }
  
  @Override
  public double calculateValue(Bond bond, Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
//	  return m_ChildNodes[0].getValue(bond, definingSite, axis, daxis)*bond.getBondLength(definingSite, axis, daxis);
	  return bond.getBondLength(definingSite, axis, daxis);
  }
  
  @Override
  public Node generateNode(Node childNode) {
//    ConstantNode c = new ConstantNode(1.0);
//    return new DistanceNode(c);
	  return new DistanceNode();
  }

  @Override
  public double calculateDerivative(Bond bond, ConstantNode variable, Structure.Site definingSite, Integer axis) throws NotFiniteValueException {
    // With respect to the position of the central site in the x axis
    // Uses d/dx (fg) = f'g + fg'
  
//    //DistanceNode = node * r
//    double f = m_ChildNodes[0].getValue(bond, definingSite, axis, null);
//    double fPrime = m_ChildNodes[0].getDerivative(bond, variable, definingSite, axis);
//
//    double g = bond.getBondLength(definingSite, axis, null);
//    double gPrime = bond.getDerivative(variable, definingSite, axis);
//    
//    return (f * gPrime) + (g * fPrime);

	  return bond.getDerivative(variable, definingSite, axis);
  }
  
  @Override
  public Node copy() {
//    // TODO Why not just have a singleton distance node?  In case we want to store values on each node.		 
	    Node copy = new DistanceNode();
//	    Node copy = new DistanceNode(m_ChildNodes[0]);
		copy.setValues(this.getNumTimesSelected());
	    return copy;	
  }
  @Override
  public String getNodeType(){
	  return "Distance";
  }
  @Override
  public Integer getComplexity(){
	  return 1;
  }
  @Override
  public ArrayList<String> getExpression(boolean symbolic){
	  ArrayList<String> aL = new ArrayList<String>();
	  aL.add(0,"r");
	  
//	  ArrayList<String> aL = new ArrayList<String>();
//	  aL.add(0,"((");
//	  aL.addAll(1,this.getChildNode(0).getExpression(symbolic));
//	  aL.add(aL.size(),")*(");
//	  aL.add(aL.size(),"r");
//	  aL.add(aL.size(),"))");
	  
	  return aL;
  }
  @Override
  public Node getLeft(){
	  return null;
//	  return m_ChildNodes[0];
  }
  @Override
  public Node getRight(){
	  return null;
  }
  @Override
  public Double getFromKnownValues(Bond bond){  
//	  It is faster to calculate this value than to cache it
	  return null;
	  }
  @Override
  public void putInKnownValues(Bond bond, double value){
	  }

@Override
public Node generateNode(Node child0, Node child1) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
	  return null;
}

@Override
public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {	
}
@Override
public String getSymbol() {
	return "r";
}
}
