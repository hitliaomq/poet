/*
 * Created on August 9, 2016
 *
 */
package potentialModelsGP.tree;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Random;
import java.util.Stack;
import matsci.io.app.log.Status;
import matsci.util.arrays.ArrayUtils;
import potentialModelsGP.tree.neighbors.SiteSumNode;
import util.Statistics;

public class TreePotential {
  public static final Random GENERATOR = new Random();
  public static boolean m_ScaleDistances;
  private Node m_RootNode;
  private ConstantNode m_CutoffDistanceNode;
  private Node[] m_AllNodes;
  private ConstantNode[] m_ConstantNodes;
  private Integer m_Complexity;
  private Integer m_NumNodes;
  private double m_NumTimesSelected;
  private Double[] m_CumulativeWeights;
  
  public TreePotential(Node topNode, double initialCutoffDistance) {
    m_RootNode = topNode;
    m_CutoffDistanceNode = new ConstantNode(initialCutoffDistance);
    m_NumTimesSelected = 0.0;
  }
  
  public Node getTopNode() {
    return m_RootNode;
  }
  
  public double getCutoffDistance() {
    return m_CutoffDistanceNode.getValue();
  }
  
  public int numNodes() {
	if(m_NumNodes == null){
		Node[] nodes = this.getAllNodes();
		int num = 0;
		for (Node node : nodes) {
//			If a siteSumNode has a distance node as a child, then 
//			Smoothing spline has 17 nodes, but the siteSumNode also uses a multiplication node, so numNodes is 18
//			This does not consider the fact that the distances can cancel out by (r-r) or (r/r) for example.
//			This can be solved by simplifying the expression. For example, (r-r) would be replaced by 0.0
			if(node.getNodeType().equals("SiteSum")){ 
//				SiteSumNode siteSum = (SiteSumNode) node;
//				if(siteSum.hasDistanceNode()) {
					num += 18; //Number of nodes of the smoothing function
//				}
//				else {
//					num++;
//				}
			}
			else {
				num++;
			}
		}
		m_NumNodes = num;
	}
    return m_NumNodes;
  }
  
  // TODO: for multi-component systems, separate them by species.
  private void findAllNodes() {
    ArrayList<Node> nodes = new ArrayList<Node>();
    this.getAllNodes(nodes, m_RootNode);
    m_AllNodes = nodes.toArray(new Node[0]);

    int returnIndex = 0;
    ConstantNode[] returnArray = new ConstantNode[m_AllNodes.length];
    for (int nodeNum = 0; nodeNum < m_AllNodes.length; nodeNum++) {
      Node node = m_AllNodes[nodeNum];
      if (node instanceof ConstantNode) {
        returnArray[returnIndex++] = (ConstantNode) node;
      }
    }
    m_ConstantNodes = (ConstantNode[]) ArrayUtils.truncateArray(returnArray, returnIndex);
  }
  
  private void getAllNodes(ArrayList<Node> knownNodes, Node currentNode) {
    
    knownNodes.add((Node) currentNode);
    
    for (int childNodeNum = 0; childNodeNum < currentNode.numChildNodes(); childNodeNum++) {
      Node childNode = currentNode.getChildNode(childNodeNum);
      this.getAllNodes(knownNodes, childNode);
    }
  }
  
//  Source: http://www.graphviz.org/content/Genetic_Programming
  public ArrayList<String> getDOT() {
	  
	  ArrayList<String> list = new ArrayList<>();
	  list.add("graph \"\"");
	  list.add("{");
	  
	  //The size of the image
	  list.add("graph [ dpi = 300 ];");
	  
	  ArrayList<Integer> count = new ArrayList<Integer>();
	  this.getDOT(getTopNode(), list, count);
	  
	  list.add("}");
	  return list;
  }
  
  private void getDOT(Node currentNode,ArrayList<String> list,ArrayList<Integer> count) {
	    
	   	int c = count.size();
	   	list.add(c+"[label=\""+currentNode.getSymbol()+"\"];");
	   	count.add(1);
	    for (int childNodeNum = 0; childNodeNum < currentNode.numChildNodes(); childNodeNum++) {
	    	list.add(c+" -- "+count.size()+";");
	    	getDOT(currentNode.getChildNode(childNodeNum),list,count);
	    }
	  }
  
  public Node[] getConstantNodes() {
    
    if (m_ConstantNodes == null) {
      this.findAllNodes();
    }
    return (Node[]) ArrayUtils.copyArray(m_ConstantNodes);
  }
  
  public int numConstantNodes() {
    
    if (m_ConstantNodes == null) {
      this.findAllNodes();
    }
    return m_ConstantNodes.length;
  }
  
  public ConstantNode getConstantNode(int nodeNum) {
    
    if (m_ConstantNodes == null) {
      this.findAllNodes();
    }
    return m_ConstantNodes[nodeNum];
  }
  
  public TreePotential insertRandomNonTerminal(Node nodeToReLocate) {
    
    INodeFactory nodeFactory = getRandomNonTerminalFactory();
    
    Node newBranch = nodeFactory.generateNode(nodeToReLocate.deepCopy());
    
    Node topNode = this.getTopNode();
    Node newTopNode = this.copyAndReplace(topNode, nodeToReLocate, newBranch);
    
    return new TreePotential(newTopNode, this.getCutoffDistance()); 
  }
  
  public TreePotential replaceRandomNonTerminal(Node nodeToReplace) {
	    
	    INodeFactory nodeFactory = getRandomNonTerminalFactory();
	    
	    Node newBranch;
	    if(nodeToReplace.numChildNodes() == 2) {
	    	Node child0 = nodeToReplace.getChildNode(0).deepCopy();
	    	Node child1 = nodeToReplace.getChildNode(1).deepCopy();
	        newBranch = nodeFactory.generateNode(child0,child1);
	        if(newBranch.getNodeType().equals("SiteSum") && GENERATOR.nextBoolean()) {
	        	newBranch.replaceChildNode(0, child1);
	        }
	    }
	    else if(nodeToReplace.numChildNodes() == 1) {
	        newBranch = nodeFactory.generateNode(nodeToReplace.getChildNode(0).deepCopy());
	    }
	    else {
	    	int index = GENERATOR.nextInt(nodeToReplace.numChildNodes());
	        newBranch = nodeFactory.generateNode(nodeToReplace.getChildNode(index).deepCopy());
	    }
	    return this.replaceNode(newBranch, nodeToReplace);
	  }

public TreePotential replaceNodeWithAvgOverAllStructures(Node nodeToReplace, double average) {
	  	
	Node averageValueOverAllStructures = new ConstantNode(average);
	Node topNode = this.getTopNode();
	Node newTopNode = this.copyAndReplace(topNode, nodeToReplace, averageValueOverAllStructures);
	
	return new TreePotential(newTopNode, this.getCutoffDistance()); 
}

  
//method == 'g' is grow, method == 'f' is full
public static TreePotential generateTree(double cutoffDistance, int depth, char method){
	//Create a node. The leaves are not always at the same depth. 
	Node branch;
	TreePotential tree;
	do{
		 branch = TreePotential.deepGenerate(depth, method);
		tree = new TreePotential(branch,cutoffDistance);
	}while(tree.numNodes()<2);
	return tree;
}
  
public static TreePotential generateSmoothingSpline(double cutoffDistance, double rin, double rout) {
	
	double rinsq = Math.pow(rin, 2.0);
	double routsq = Math.pow(rout, 2.0);
	double cval = Math.pow(routsq-rinsq, 3.0);
	double aval = routsq;
	double bval = routsq - 3*rinsq;
			
	Node a = new ConstantNode(aval);
	Node b = new ConstantNode(bval);
	Node c = new ConstantNode(cval);
	Node r0 = new DistanceNode();
	Node r1 = new DistanceNode();
	Node pow0 = new PowerNode(r0,new ConstantNode(2.0));
	Node sub0 = new SubtractionNode(pow0,a);
	Node pow2 = new PowerNode(sub0,new ConstantNode(2.0));
	
	Node pow1 = new PowerNode(r1,new ConstantNode(2.0));
	Node mult1 = new MultiplicationNode(new ConstantNode(2.0),pow1);
	Node add0 = new AdditionNode(b,mult1);
	Node mult0 = new MultiplicationNode(pow2,add0);
	
	Node div = new DivisionNode(mult0,c);
	
	TreePotential tree = new TreePotential(div, cutoffDistance);
	
	return tree;
}
  
  
  public static TreePotential buildTree(String equation, double cutoffDistance){
//		Start pre-processing	  
	  equation = "("+equation+")";
	  equation = equation.replace("[", "(");
	  equation = equation.replace("]", ")");
	  equation = equation.replace("*10^-", "W");
	  equation = equation.replace("*10^", "E");
	  equation = equation.replace(",1", "");
	  
	  char[] tempArray1 = equation.toCharArray();

	  ArrayList<Character> tempList = new ArrayList<Character>();
	  for (int i = 0; i < tempArray1.length; i++) {
		char t = tempArray1[i];
		if(Character.isDigit(t)){
			if(tempArray1[i-1] == '('){
				tempList.remove(tempList.size()-1);
			}
			else{
				tempList.remove(tempList.size()-2);
			}
			while(Character.isDigit(t)||t == '.' || t == 'E'|| t == 'W'){
				tempList.add(t);
				i++;
				t = tempArray1[i];
			}
			i++;
			t = tempArray1[i];
		}
		else if(t =='r'){
			tempList.remove(tempList.size()-1);
			tempList.add(t);
			i++;
			i++;
			t = tempArray1[i];
		}
		tempList.add(t);
	  }
	  
	  char[] tempArray2 = new char[tempList.size()];
	  int cnt = 0;
	  for (Character chtr : tempList) {
		tempArray2[cnt] = chtr;
		cnt++;
	  }
	  equation = new String(tempArray2);
//		End pre-processing	  

//	  Split equation string
		ArrayList<String> list = new ArrayList<String>();
		int max = equation.length();
		int c = 0;
		int s = 0;
		while (c < max) {

			if(Character.isDigit(equation.charAt(c))){
				StringBuilder string = new StringBuilder();
				try{
//					if(list.get(s-1).equals("-") && !list.get(s-2).equals(")")){
					if(list.get(s-1).equals("-") && !list.get(s-2).equals(")")
							&& !list.get(s-2).equals("r") && !list.get(s-2).contains(".")){
						list.remove(s-1);
						s--;
						string.append('-');
					}
				}catch(IndexOutOfBoundsException e){
				}
				while(Character.isDigit(equation.charAt(c))||equation.charAt(c)=='.'
						||equation.charAt(c)=='E'||equation.charAt(c)=='W'){
					 string.append(equation.charAt(c));
					 c++;
				}
				list.add(string.toString());
				s++;
			}
			else if(equation.charAt(c)=='(' ||equation.charAt(c)==')'){
				try{
					StringBuilder string = new StringBuilder();
					string.append(equation.charAt(c));
					list.add(string.toString());
					s++;
					c++;
					c++;
				}catch(IndexOutOfBoundsException e){
				}
			}
			else{
				StringBuilder string = new StringBuilder();
				string.append(equation.charAt(c));
				list.add(string.toString());
				s++;
				c++;
			}
		}
		
	  Stack<NodeBuilder> pStack = new Stack<NodeBuilder>();
	  NodeBuilder node = new NodeBuilder();
	  NodeBuilder currentNode = node;
	  for (String string : list) {
		  if(string.equals("(")){
			  currentNode.m_ChildNodes[0] = new NodeBuilder();
			  pStack.push(currentNode);
			  currentNode = (NodeBuilder)currentNode.getChildNode(0);
		  }
		  else if(string.contains(".")){
			  currentNode.setTypeToBuild(string);
			  NodeBuilder parent = (NodeBuilder)pStack.pop();
			  currentNode = parent;
		  }
		  else if(string.equals("r")){
			  currentNode.setTypeToBuild(string);
			  NodeBuilder parent = (NodeBuilder)pStack.pop();
			  currentNode = parent;
		  }
		  else if(string.equals("+")||string.equals("-")||
				  string.equals("*")||string.equals("/")||string.equals("^")){
			  currentNode.setTypeToBuild(string);
			  currentNode.m_ChildNodes[1] = new NodeBuilder();
			  pStack.push(currentNode);
			  currentNode = (NodeBuilder)currentNode.getChildNode(1);			  
		  }
		  else if(string.equals("T")){
			  currentNode = pStack.pop();
			  currentNode.setTypeToBuild(string);
			  currentNode.m_ChildNodes[1] = new NodeBuilder();
			  pStack.push(currentNode);
			  currentNode = (NodeBuilder)currentNode.getChildNode(1);			  
		  }
		  else if(string.equals("F")){
			  currentNode = pStack.pop();
			  currentNode.setTypeToBuild(string);
			  currentNode.m_ChildNodes[1] = new NodeBuilder();
			  pStack.push(currentNode);
			  currentNode = (NodeBuilder)currentNode.getChildNode(1);			  
		  }
		  else if(string.equals(")")){
			  try{
				  currentNode = pStack.pop();
			  }catch(EmptyStackException e){
				  
			  }
		  }
	  }	  
	  
	  Node topNode = currentNode.build();
	  TreePotential tree = new TreePotential(topNode, cutoffDistance);

	  return tree;
  }



    
  public TreePotential getTreeForEnergyVsBondLength(double cutoff, String species){		
	  	double numNeighbors = 42.0;
//	  	Constant C (unitless)
	  	double cvalue = Double.NaN;
	  	double powervalue = Double.NaN;
	  	if(species.equals("Cu")){
	  		cvalue = 527.6214232;
	  		powervalue = 6;
	  	}
	  	else if(species.equals("Au")){
  			cvalue = 14878.01402*1;
	  		powervalue = 8;
	  	}
	  	
	  	else if(species.equals("Ag")){
	  		powervalue = 6;
  			cvalue = Math.pow(2.928323832, powervalue)*0.99;
	  	}

		Node r = new DistanceNode();
		Node oneHalf = new ConstantNode(0.5);
		Node pw = new ConstantNode(-powervalue);
//		Node minusA = new ConstantNode(-1.0);//A = 1 (Units of eV), minus because it's attractive
		Node C = new ConstantNode(numNeighbors*cvalue);//Use number of neighbors in FCC
		
		Node powerRho = new PowerNode(r,pw);
		Node multiplicationRho = new MultiplicationNode(C , powerRho);
		Node sqrtRho = new PowerNode(multiplicationRho,oneHalf);

		
//	  	Constant A lj (Units of eV*Angstrom). Later we divide by 2 to avoid double counting
	  	double aValue = Double.NaN;
	  	double powerValue = Double.NaN;
	  	if(species.equals("Cu")){
	  		aValue = 1289.04851;
	  		powerValue = 9;
	  	}
	  	else if(species.equals("Au")){
	  		powerValue = 10;
  			aValue = 16352.11869*1.001;
	  	}
	  	else if(species.equals("Ag")){
	  		powerValue = 12;
  			aValue = Math.pow(2.485883762, powerValue)*1.1;
	  	}
		
		Node Alj = new ConstantNode(numNeighbors*aValue/2);//Use number of neighbors in FCC. Multiply by 1/2 to avoid double-counting
		Node minnine = new ConstantNode(-powerValue);
		Node rlj = new DistanceNode();
		Node powerlj = new PowerNode(rlj,minnine);
		Node multiplj = new MultiplicationNode(Alj,powerlj);

		
		Node subtraction = new SubtractionNode(multiplj, sqrtRho);
		
		TreePotential tree = new TreePotential(subtraction,cutoff);
		return tree;
  }
  
  public Node generateSmoothing(){
//	  Math.exp(c/(site.getBond(neighborNum).getBondLength()-rc))
	  double rc = this.getCutoffDistance();
	  double c = 1.0;
	  double e = Math.E;
	  Node r = new DistanceNode();
	  Node cNode = new ConstantNode(c);
	  Node eN = new ConstantNode(e);
	  Node rcN = new ConstantNode(rc);
	  Node subtraction = new SubtractionNode(r,rcN);
	  Node division = new DivisionNode(cNode,subtraction);
	  Node smoothing = new PowerNode(eN,division);
	  return smoothing;
}
  

   
  public TreePotential replaceNode(Node newBranch, Node nodeToReplace) {
    
    Node topNode = this.getTopNode();
    Node newTopNode = this.copyAndReplace(topNode, nodeToReplace, newBranch);
    return new TreePotential(newTopNode, this.getCutoffDistance());
  }
  
  
//Get a copy of branchToCopy with replacementNode instead of nodeToReplace
  private Node copyAndReplace(Node branchToCopy, Node nodeToReplace, Node replacementNode) {
    if (branchToCopy == nodeToReplace) {
      return replacementNode;
    }
    branchToCopy = branchToCopy.copy();
    for (int childNum = 0; childNum < branchToCopy.numChildNodes(); childNum++) {
      Node childNode = branchToCopy.getChildNode(childNum);
      Node newChildNode = this.copyAndReplace(childNode, nodeToReplace, replacementNode);
      branchToCopy.replaceChildNode(childNum, newChildNode);
    }
    return branchToCopy;
  }
  

//For the same tree, this method can return arrays with nodes in different order
  public Node[] getAllNodes(){
	  if (m_AllNodes == null) {
	      this.findAllNodes();
	    }
	  return m_AllNodes;
  }

//private static INodeFactory getRandomNodeFactory() {
//	return NODE_FACTORIES[GENERATOR.nextInt(NODE_FACTORIES.length)];
//}
private static INodeFactory getRandomNonTerminalFactory() {
	return NON_TERMINAL_FACTORIES[GENERATOR.nextInt(NON_TERMINAL_FACTORIES.length)];
}

//private static INodeFactory[] NODE_FACTORIES = new INodeFactory[] {
//   new DistanceNode(),
//   new ConstantNode(),
//   new AdditionNode(),
//   new SubtractionNode(),
//    
//   new MultiplicationNode(),
//   new DivisionNode(),
////new SquareRootNode(),
//   new PowerNode(),
//// new ExponentialNode(),
////new NaturalLogNode(),
//// new GeneralizedMeanNode(),
//// new SiteProductNode(), 
//   new SiteSumNode()
//};  

private static INodeFactory[] NON_TERMINAL_FACTORIES = new INodeFactory[] {
		   new AdditionNode(),
		   new SubtractionNode(),
		   new MultiplicationNode(),
		   new DivisionNode(),
		//new SquareRootNode(),
		   new PowerNode(),
		// new ExponentialNode(),
		//new NaturalLogNode(),
		// new GeneralizedMeanNode(),
		// new SiteProductNode(), 
		   new SiteSumNode()
};  
private static INodeFactory[] TERMINAL_FACTORIES = new Node[] {
		   new DistanceNode(),
		   new ConstantNode()
};

private static INodeFactory[] ADD_SUBTRACT_FACTORIES = new Node[] {
		new AdditionNode(),
		new SubtractionNode()
};

public static Node combineAddOrSubtract(Node child0,Node child1) {
	INodeFactory nodeFactory = ADD_SUBTRACT_FACTORIES[GENERATOR.nextInt(ADD_SUBTRACT_FACTORIES.length)];
		Node node = nodeFactory.generateNode(child0,child1);
    return node;
  }

private static Node generateNonTerminal() {
	INodeFactory nodeFactory = TreePotential.NON_TERMINAL_FACTORIES[GENERATOR.nextInt(TreePotential.NON_TERMINAL_FACTORIES.length)];
		Node node = nodeFactory.generateNode();
    return node;
}

private static Node generateTerminal() {
		INodeFactory nodeFactory = TreePotential.TERMINAL_FACTORIES[GENERATOR.nextInt(TreePotential.TERMINAL_FACTORIES.length)];
		Node node = nodeFactory.generateNode();
		
		if(m_ScaleDistances) {
			if(node.getNodeType().equals("Distance")) {
				double value = GENERATOR.nextBoolean() ? 1.0 + Statistics.PRECISION*100.0 : 1.0 - Statistics.PRECISION*100.0;
				node = new MultiplicationNode(new ConstantNode(value),node.deepCopy());
			}
		}
		
		return node;
}

//method == 'g' is grow, method == 'f' is full
private static Node deepGenerate(int depth,char method) {
	   	if(depth == 0 || ((method == 'g') && (GENERATOR.nextDouble() < ((double)TreePotential.TERMINAL_FACTORIES.length)/((TreePotential.TERMINAL_FACTORIES.length+TreePotential.NON_TERMINAL_FACTORIES.length))))){
	   		return TreePotential.generateTerminal();
	   	}
		Node parent = TreePotential.generateNonTerminal();
		int numChildren = parent.numChildNodes();
	    for (int nodeNum = 0; nodeNum < numChildren; nodeNum++) {
	    	parent.m_ChildNodes[nodeNum] = TreePotential.deepGenerate(depth-1,method);
	    }
	    return parent;
}
  
  public int numSiteSums(){
	  Node[] allNodes = this.getAllNodes();
	  int numLoopNodes = 0;
	  for (Node node : allNodes) {
		  String string = node.getNodeType();
		  if(string.equals("SiteSum")){
			  numLoopNodes++;
		  }
	  }
	  return numLoopNodes;
  }
  
  public boolean hasDistance(){
	  Node[] allNodes = this.getAllNodes();
	  for (Node node : allNodes) {
		  String string = node.getNodeType();
		  if(string.equals("Distance")){
			  return true;
		  }
	  }
	  return false;
  }

public Integer getIndexOfConstant(Node constant) {
	int index = 0;
	for (Node thisnode : this.getConstantNodes()) {
		if(constant.equals(thisnode)) {
			return index;
		}
		index++;
	}
	return null;
}
  
//  Returns the number of siteSumNode descendants of the siteSumNode in the tree with the greatest
//  number of siteSumNode descendants
  public int maxSiteSumsUnderASiteSum() {
	  int max = 0;
	  Node[] allNodes = this.getAllNodes();
	  for (Node node : allNodes) {
		  String string = node.getNodeType();
		  if(string.equals("SiteSum")){
			  int num = node.numSiteSums(0) - 1;
			  if(max < num) {
				  max = num;
			  }
		  }
	  }
	  return max;
  }
  
//  public boolean allSiteSumsHaveDistance() {
//	  Node[] allNodes = this.getAllNodes();
//	  for (Node node : allNodes) {
//		  String string = node.getNodeType();
//		  if(string.equals("SiteSum")){
//			  SiteSumNode ss = (SiteSumNode)node;
//			  if(!ss.hasDistanceNode()) {
//				  return false;
//			  }
//		  }
//	  }
//	  return true;
//  }
  
  public int[] numEachNode(){
	  Node[] allNodes = this.getAllNodes();
	  int numLoopNodes = 0;
	  int numDistances = 0;
	  int numPowers = 0;
	  int numMult = 0;
	  int numDiv = 0;
	  int numAdd = 0;
	  int numSub = 0;
	  
	  for(int i = 0; i < allNodes.length; i++){
		  String string = allNodes[i].getNodeType();
		  if(string.equals("SiteSum")||string.equals("SiteProduct")||string.equals("GeneralizedMean")){
			  numLoopNodes++;
		  }
		  else if(string.equals("Distance")){
			  numDistances++;
		  }
		  else if(string.equals("Power")){
			  numPowers++;
		  }
		  else if(string.equals("Multiplication")){
			  numMult++;
		  }
		  else if(string.equals("Division")){
			  numDiv++;
		  }
		  else if(string.equals("Addition")){
			  numAdd++;
		  }
		  else if(string.equals("Subtraction")){
			  numSub++;
		  }

	  }
	  
	  return new int[]{	numLoopNodes, numPowers, numDiv, numMult, numAdd+numSub, numDistances, numConstantNodes(), numNodes()};
  }
  
  public boolean hasNotFiniteConstantNode(){
	  for(int i = 0; i < this.getConstantNodes().length; i++){
		  if(!Double.isFinite(this.getConstantNode(i).getValue())){
			  return true;
		  }
	  }
	  return false;
  }
  
//  public TreePotential simplify() throws NotFiniteValueException{
//	  TreePotential simplified = this;
//	  
//	//If a distance node in is not the successor of a neighbors node, then
//	//  we must replace this distance node with zero
//	  simplified = simplified.replaceDistances();
//	  
////	  If a node has only constant nodes as its children, then compute the value
////	  and replace the node with the value.
//	  simplified = simplified.replaceNodesWithValue();
//	  
////	  We added zeros instead of r when r had no neighbors node ancestor. We can
////	  replace some zeros. Only do for addition, subtraction and siteSum. Otherwise, we could
////	  substitute a non-finite node by a finite node which is zero. Example: we have zero
////	  multiplied by NaN. If we replace the result by zero then we would make the tree
////	  valid, but it should not be valid because it has NaN
//	  simplified = simplified.replaceZeros();
//	  	  
////	  Make sure we return a new tree so that we clear m_AllNodes and m_AllConstants from the tree
//	  TreePotential newSimplified = new TreePotential(simplified.getTopNode().deepCopy(),simplified.getCutoffDistance());
//	  
//	  return newSimplified;
//  }
  
////If a distanceNode in allDistanceNodes is not the successor of a neighbors node, then
////  we must replace this distanceNode with zero
//  private TreePotential replaceDistances(){
//	  TreePotential simplifiedTree = this;
//	  boolean simplified = true;
//	  while(simplified == true){
//		  Node[] allNodes = simplifiedTree.getAllNodes();
//		  for (int i = 0; i < allNodes.length; i++) {
//			if(allNodes[i].getNodeType().equals("Distance")){
//				boolean hasNeighborsAncestor = simplifiedTree.hasNighborsNodeAncestor(allNodes[i]);
//				if(!hasNeighborsAncestor){
//					Node zero = new ConstantNode(0.0);
//					Node newtopnode = simplifiedTree.copyAndReplace(simplifiedTree.getTopNode(), allNodes[i], zero);
//					simplifiedTree = new TreePotential(newtopnode.deepCopy(), simplifiedTree.getCutoffDistance());
//					break;
//				}
//			}
//			if(i == allNodes.length-1){simplified = false;}
//		  }
//	  }
//	  return simplifiedTree;
//  }
  
////  If a node has only constant nodes as its children, then compute the value
////  and replace the node with the value.
//  private TreePotential replaceNodesWithValue() throws NotFiniteValueException{
//	  TreePotential simplifiedTree = this;
//	  boolean simplified = true;
//	  while(simplified == true){
//		  Node replaceThisNodeByZero = null;
//		  Node[] allNodes = simplifiedTree.getAllNodes();
//		  for (int i = 0; i < allNodes.length; i++) {
//			boolean replace = true;
//			int length = allNodes[i].numChildNodes();
//			for (int j = 0; j < length; j++) {
//				if(!allNodes[i].getChildNode(j).getNodeType().equals("Constant")){
//					replace = false;
//					break;
//				}
//				else if(allNodes[i].getNodeType().equals("SiteSum")){
//					ConstantNode c = (ConstantNode)allNodes[i].getChildNode(j);
//					if(c.getValue() == 0.0){
//						replaceThisNodeByZero = allNodes[i];
//						break;
//					}
//				}
//			}
//			if(length == 0){replace = false;}
//			String type = allNodes[i].getNodeType();
//			if(type.equals("SiteSum")||type.equals("SiteProduct")||type.equals("GeneralizedMean")){replace = false;}
//			if(replace){
//				Node constant = new ConstantNode(allNodes[i].calculateValue(null, null, null, null));
//				Node newtopnode = simplifiedTree.copyAndReplace(simplifiedTree.getTopNode(), allNodes[i], constant);
//				simplifiedTree = new TreePotential(newtopnode.deepCopy(), simplifiedTree.getCutoffDistance());
//				break;
//			}
//			else if(replaceThisNodeByZero!=null){
//				Node zero = new ConstantNode(0.0);
//				Node newtopnode = simplifiedTree.copyAndReplace(simplifiedTree.getTopNode(), replaceThisNodeByZero, zero);
//				simplifiedTree = new TreePotential(newtopnode.deepCopy(), simplifiedTree.getCutoffDistance());
//				break;
//			}
//			if(i == (allNodes.length - 1)){simplified = false;}
//		  }
//	  }
//	  return simplifiedTree;
//  }
  
  
public Node getParent(Node child) {
	return getParent(child, this.getTopNode(), null);
}
private Node getParent(Node child, Node node, Node parent){
    if (node == null) {
        return null;
    } else if (!node.equals(child)) {
        parent = getParent(child, node.getLeft(), node);
        if (parent == null) {
            parent = getParent(child, node.getRight(), node);
        }
    }
    return parent;
}

private TreePotential simplifyNodesWithConstantChildren() {
	TreePotential returnTree;
	TreePotential simplifiedTree = this;
	do {
		returnTree = simplifiedTree;
		simplifiedTree = returnTree.replaceNodeWithConstantChildren();
	}while(simplifiedTree!=null);
	return new TreePotential(returnTree.getTopNode().deepCopy(), returnTree.getCutoffDistance());
}

public TreePotential addScalingToAllDistances() {
	TreePotential returnTree;
	TreePotential treeWithScalingFactors = this;
	do {
		returnTree = treeWithScalingFactors;
		treeWithScalingFactors = returnTree.addScalingToADistanceNode();
	}while(treeWithScalingFactors!=null);
	return new TreePotential(returnTree.getTopNode().deepCopy(), returnTree.getCutoffDistance());
}

public TreePotential simplify() {
	TreePotential returnTree = this;
//	Replace distances without ancestor siteSum with 0.0 or 1.0
	returnTree = returnTree.simplifyDistancesWithoutAncestorSiteSum();
//	Then, simplify subtractions and divisions with distances
	returnTree = returnTree.simplifySubtractionDivisionOrMultiplicationWithDistanceChildren();		
//	Then, simplify nodes with constant children
	returnTree = returnTree.simplifyNodesWithConstantChildren();
//	Replace constants that are only adding or subtracting
	returnTree = returnTree.simplifyConstantsAddingOrSubtracting();//TODO added this line
	return new TreePotential(returnTree.getTopNode().deepCopy(), returnTree.getCutoffDistance());
}

private TreePotential simplifySubtractionDivisionOrMultiplicationWithDistanceChildren() {
	TreePotential returnPotential = this;
	
//	First, replace subtraction or division nodes with distance nodes by 0 or 1
	Node nodeWithDistanceChildren = returnPotential.getSubtractionDivisionOrMultiplicationWithDistanceChildren();
	while(nodeWithDistanceChildren != null) {
		returnPotential = returnPotential.replaceSubtractionDivisionOrMultiplicationWithDistanceChildren(nodeWithDistanceChildren);
		nodeWithDistanceChildren = returnPotential.getSubtractionDivisionOrMultiplicationWithDistanceChildren();
	}
//	Second, remove the zeros and ones added
	TreePotential previousPotential;
	do {
		previousPotential = returnPotential;
		//Would return null if there are no more nodes to replace
		returnPotential = previousPotential.replaceNodeWith0or1();
	}while(returnPotential != null);
	returnPotential = previousPotential;
	
	return new TreePotential(returnPotential.getTopNode().deepCopy(), returnPotential.getCutoffDistance());
}

private TreePotential simplifyDistancesWithoutAncestorSiteSum() {
	TreePotential returnPotential = this;
	
//	First, replace subtraction or division nodes with distance nodes by 0 or 1
	Node nodeWithoutAncestorSiteSum = returnPotential.getDistanceWithoutAncestorSiteSum();
	while(nodeWithoutAncestorSiteSum != null) {
		returnPotential = returnPotential.replaceNode(new ConstantNode(1.0), nodeWithoutAncestorSiteSum);//TODO new ConstantNode(0.0)
		nodeWithoutAncestorSiteSum = returnPotential.getDistanceWithoutAncestorSiteSum();
	}
//	Second, remove the zeros added
	TreePotential previousPotential;
	do {
		previousPotential = returnPotential;
		//Would return null if there are no more nodes to replace
		returnPotential = previousPotential.replaceNodeWith0or1();
	}while(returnPotential != null);
	returnPotential = previousPotential;
	
	return new TreePotential(returnPotential.getTopNode().deepCopy(), returnPotential.getCutoffDistance());
}

/**
 * simplify the constants that are adding or subtracting to the main tree because
 * they only shift the energy by a constant value and don't affect the forces
 * @return
 */
private TreePotential simplifyConstantsAddingOrSubtracting() {
	TreePotential returnPotential = this;
	
	Node[] nodeWithoutAncestorSiteSum = returnPotential.getConstantWithOnlyAdditionOrSubtractionAncestors();
	while(nodeWithoutAncestorSiteSum != null) {
		Node newBranch = nodeWithoutAncestorSiteSum[0].getChildNode(0) != nodeWithoutAncestorSiteSum[1] ? nodeWithoutAncestorSiteSum[0].getChildNode(0) : nodeWithoutAncestorSiteSum[0].getChildNode(1);
		returnPotential = returnPotential.replaceNode(newBranch, nodeWithoutAncestorSiteSum[0]);
		nodeWithoutAncestorSiteSum = returnPotential.getConstantWithOnlyAdditionOrSubtractionAncestors();
	}
//	Second, remove the ones added
	TreePotential previousPotential;
	do {
		previousPotential = returnPotential;
		//Would return null if there are no more nodes to replace
		returnPotential = previousPotential.replaceNodeWith0or1();
	}while(returnPotential != null);
	returnPotential = previousPotential;
	
	return new TreePotential(returnPotential.getTopNode().deepCopy(), returnPotential.getCutoffDistance());
}

private Node getSubtractionDivisionOrMultiplicationWithDistanceChildren() {
	for (Node node : this.getAllNodes()) {
		if(node.getNodeType().equals("Distance")) {
			Node parent = this.getParent(node);
			if(parent != null) {
				String type = parent.getNodeType();
				if(type.equals("Division")||type.equals("Subtraction")||type.equals("Multiplication")) {
					boolean hasNonDistanceChild = false;
					for (Node child : parent.m_ChildNodes) {
						hasNonDistanceChild |= !child.getNodeType().equals("Distance");
					}
					if(!hasNonDistanceChild) {
						return parent;
					}
				}
			}
		}
	}
	return null;
}

private Node getDistanceWithoutAncestorSiteSum() {
	for (Node node : this.getAllNodes()) {
		if(node.getNodeType().equals("Distance")) {
			if(!this.hasSiteSumAncestor(node)) {
				return node;
			}
		}
	}
	return null;
}

private Node[] getConstantWithOnlyAdditionOrSubtractionAncestors() {
	for (Node node : this.getConstantNodes()) {
		if(this.hasOnlyAdditionSubtractionAncestors(node)) {
			return new Node[] {this.getParent(node), node};
		}
	}
	return null;
}

private ArrayList<Node> getAncestors(Node node) {
	ArrayList<Node> returnlist = new ArrayList<Node>();
	Node ancestor = this.getParent(node);
	while(ancestor != null) {
		returnlist.add(ancestor);
		ancestor = this.getParent(ancestor);
	}
	return returnlist;
}

private boolean hasSiteSumAncestor(Node node) {
	ArrayList<Node> ancestors = this.getAncestors(node);
	for (Node node2 : ancestors) {
		if(node2.getNodeType().equals("SiteSum")) {
			return true;
		}
	}
	return false;
}

/**
 * 
 * @param node
 * @return false if node is the top node
 */
private boolean hasOnlyAdditionSubtractionAncestors(Node node) {
	if(this.getTopNode().equals(node)) {
		return false;
	}
	ArrayList<Node> ancestors = this.getAncestors(node);
	for (Node node2 : ancestors) {
		if(!node2.getNodeType().equals("Addition") && !node2.getNodeType().equals("Subtraction")) {
			return false;
		}
	}
	return true;
}

protected TreePotential replaceNodeWith0or1() {
	Node[] constantNodes = this.getConstantNodes();
	for (Node node : constantNodes) {
		Node parent = this.getParent(node);
		if(parent == null) {return null;}
		Double child0Value = Double.NaN;
		Double child1Value = Double.NaN;
		int c = 0;
		for (Node child : parent.m_ChildNodes) {
			boolean isConstant = child.getNodeType().equals("Constant");
			if(c==0 && isConstant) {
				child0Value = ((ConstantNode)child).getValue();
			}
			if(c==1 && isConstant) {
				child1Value = ((ConstantNode)child).getValue();
			}
			c++;
		}
		boolean bchild01 = Statistics.compare(child0Value, 1.0) == 0;
		boolean bchild00 = Statistics.compare(child0Value, 0.0) == 0;
		boolean bchild11 = Statistics.compare(child1Value, 1.0) == 0;
		boolean bchild10 = Statistics.compare(child1Value, 0.0) == 0;
//		if(child0Value == 1.0 || child0Value == 0.0 || child1Value == 0.0 || child1Value == 1.0) {
		if(bchild01 || bchild00 || bchild11 || bchild10) {
			String type = parent.getNodeType();
			if(type.equals("Addition")) {
//				if(child0Value == 0.0) {
				if(bchild00) {
					return this.replaceNode(parent.getChildNode(1).deepCopy(), parent);
				}
//				else if(child1Value == 0.0) {
				else if(bchild10) {
					return this.replaceNode(parent.getChildNode(0).deepCopy(), parent);
				}
			}
			else if(type.equals("Subtraction")) {
//				if(child1Value == 0.0) {
				if(bchild10) {
					return this.replaceNode(parent.getChildNode(0).deepCopy(), parent);
				}
			}
			else if(type.equals("Multiplication")) {
//				if(child0Value == 0.0 || child1Value == 0.0) {
				if(bchild00 || bchild10) {
					return this.replaceNode(new ConstantNode(0.0), parent);
				}
				else if(bchild01) {//child0Value == 1.0) {
					return this.replaceNode(parent.getChildNode(1).deepCopy(), parent);
				}
				else if(bchild11) {//child1Value == 1.0) {
					return this.replaceNode(parent.getChildNode(0).deepCopy(), parent);
				}
			}
			else if(type.equals("Division")) {
				if(bchild00) {//child0Value == 0.0) {
					return this.replaceNode(new ConstantNode(0.0), parent);
				}
				else if(bchild11) {//child1Value == 1.0) {
					return this.replaceNode(parent.getChildNode(0).deepCopy(), parent);
				}
			}
			else if(type.equals("Power")) {
				if(bchild01) {//child0Value == 1.0) {
					return this.replaceNode(new ConstantNode(1.0), parent);
				}
				else if(bchild11) {//child1Value == 1.0) {
					return this.replaceNode(parent.getChildNode(0).deepCopy(), parent);
				}
				else if(bchild10) {//child1Value == 0.0) {
					return this.replaceNode(new ConstantNode(1.0), parent);
				}
			}
			else if(type.equals("SiteSum")) {
				if(bchild00) {//child0Value == 0.0) {
					return this.replaceNode(new ConstantNode(0.0), parent);
				}
			}
		}
	}
	return null;
}

/**
 * Example: replace 8 + 2 with 10
 * @return
 */
private TreePotential replaceNodeWithConstantChildren() {
	for (Node constant : this.getConstantNodes()) {
		Node parent = this.getParent(constant);
		if(parent != null) {
			boolean hasNonConstantChild = false;
			for (Node child : parent.m_ChildNodes) {
				hasNonConstantChild |= !child.getNodeType().equals("Constant");
			}
			if(!hasNonConstantChild) {
				int numChildren = parent.numChildNodes();
				double[] values = new double[numChildren];
				for (int i = 0; i < numChildren; i++) {
					values[i] = ((ConstantNode)parent.getChildNode(i)).getValue();
				}
				Double value = null;
				if(parent.getNodeType().equals("Addition")) {value = values[0]+values[1];}
				else if(parent.getNodeType().equals("Subtraction")) {value = values[0]-values[1];}
				else if(parent.getNodeType().equals("Multiplication")) {value = values[0]*values[1];}
				else if(parent.getNodeType().equals("Division")) {value = values[0]/values[1];}
				else if(parent.getNodeType().equals("Power")) {value = Math.pow(values[0], values[1]);}
				if(value != null) {
					Node cn = new ConstantNode(value);
					return this.replaceNode(cn, parent);
				}
			}
		}
	}
	return null;
}
/**
 * replace a distance node (r) (that does not have a scaling factor) with the node (1.0 * r)
 * @return
 */
private TreePotential addScalingToADistanceNode() {
	for (Node node : this.getAllNodes()) {
		if(node.getNodeType().equals("Distance")) {
			Node parent = this.getParent(node);
			boolean distanceHasScaling = false;
			if(parent != null) {
				if(parent.getNodeType().equals("Multiplication") || parent.getNodeType().equals("Division")) {
					for (int i = 0; i < parent.numChildNodes(); i++) {
						if(!parent.getChildNode(i).getNodeType().equals("Distance")) {
							distanceHasScaling = true;
							break;
						}
					}
				}
			}
			if(!distanceHasScaling) {
				Node distanceWithScaling = new MultiplicationNode(new ConstantNode(1.0),new DistanceNode());
				return this.replaceNode(distanceWithScaling, node);
			}
		}
	}
	return null;
}


private TreePotential replaceSubtractionDivisionOrMultiplicationWithDistanceChildren(Node nodeWithDistanceChildren) {
	Double value = null;
	TreePotential returnPotential = this;
	String nodeType = nodeWithDistanceChildren.getNodeType();
	if(nodeType.equals("Subtraction")) {value = 0.0;}
	else if(nodeType.equals("Division")) {value = 1.0;}
	if(value != null) {
		Node cn = new ConstantNode(value);
		return this.replaceNode(cn, nodeWithDistanceChildren);
	}
	if(nodeType.equals("Multiplication")) {
		Node power = new PowerNode(new DistanceNode(),new ConstantNode(2.0));
		return this.replaceNode(power, nodeWithDistanceChildren);
	}
	return new TreePotential(returnPotential.getTopNode().deepCopy(), returnPotential.getCutoffDistance());

}

////  Replace addition and subtraction nodes with one child node of zero with the other child node
//  private TreePotential replaceZeros(){
//	  TreePotential simplifiedTree = this;
//	  boolean simplified = true;
//	  while(simplified == true){
//		  Node[] allNodes = simplifiedTree.getAllNodes();
//		  for (int i = 0; i < allNodes.length; i++) {
//			Node replacementNode = null;
//			boolean replace = false;
//			String type = allNodes[i].getNodeType();
//			boolean st1 = type.equals("Addition")||type.equals("Subtraction");
//			if(st1){
//				for (int j = 0; j < 2; j++) {
//					if(allNodes[i].getChildNode(j).getNodeType().equals("Constant")){
//						ConstantNode constantNode = (ConstantNode)allNodes[i].getChildNode(j);
//						if(constantNode.getValue()==0.0){
//							if(j==0){
//								replacementNode=allNodes[i].getChildNode(1);
//								replace = true;
//								break;
//							}
//							else {
//								replacementNode=allNodes[i].getChildNode(0);
//								replace = true;
//								break;
//							}
//						}
//					}
//				}
//			}
//			if(replace){
//				Node newtopnode = simplifiedTree.copyAndReplace(simplifiedTree.getTopNode(), allNodes[i], replacementNode);
//				simplifiedTree = new TreePotential(newtopnode.deepCopy(), simplifiedTree.getCutoffDistance());
//				break;
//			}
//			if(i == allNodes.length-1){simplified = false;}
//		  }
//	  }
//	  return simplifiedTree;
//  }
  
//  public boolean hasDivisionByZero(){	  
//	  Node[] allNodes = this.getAllNodes();
//	  for (int i = 0; i < allNodes.length; i++) {
//		  if(allNodes[i].getNodeType().equals("Division")){
//			 if(allNodes[i].getChildNode(1).getNodeType().equals("Constant")){
//				 ConstantNode constant = (ConstantNode)allNodes[i].getChildNode(1);
//				 if(constant.getValue() == 0.0){
//					 return true;
//				 }
//			 }
//		  	}
//	  }
//	  return false;
//  }
  
//  private boolean hasNighborsNodeAncestor(Node node){
//	  ArrayList<String> ancestry = this.getAncestryIDs(node);
//	  boolean hasNeighborsNodeAncestor = false;
//	  for(String id : ancestry){
//		  String string = this.getNode(id).getNodeType();
//		  if(string.equals("SiteSum")||string.equals("SiteProduct")||string.equals("GeneralizedMean")){
//			 hasNeighborsNodeAncestor = true;
//			 break;
//		 }
//	  }
//	  return hasNeighborsNodeAncestor;
//  }
  
//  private Node getNode(String id){
//	  Node[] allnodes = this.getAllNodes();
//	  for (int i = 0; i < allnodes.length; i++) {
//		if(id.equals(allnodes[i].getID())){
//			return allnodes[i];
//		}
//	  }
//	  return null;
//  }
  
public String getExpression(boolean symbolic){
	String expression = "";
	ArrayList<String> aL = this.getTopNode().getExpression(symbolic);
	for(int i = 0; i<aL.size(); i++){
		expression += aL.get(i);
	}	
	return expression;
}

//  private Node getParent(String id) {
//	    return getParent(id, this.getTopNode(), null);
//	}
  
//// this should be adapted for Generalized mean node
//  private Node getParent(String id, Node node, Node parent){
//	    if (node == null) {
//	        return null;
//	    } 
//	    else if (!node.getID().equals(id)) {
//	        parent = getParent(id, node.getLeft(), node);
//	        if (parent == null) {
//	            parent = getParent(id, node.getRight(), node);
//	        }
//	    }
//	    return parent;
//	}
	
//	private ArrayList<String> getAncestryIDs(Node node){
//		ArrayList<String> indices = new ArrayList<String>();
//		Node parent;
//		do{
//			String id = node.getID();
//			parent = this.getParent(id);
//			if(parent == null){
//				break;
//			}
//			String idp = parent.getID();
//			indices.add(idp);
//			node = parent;
//		}while(parent!=null);
//		return indices;
//	}
	
	public Node getNonCommutative(){
		Node[] allNodes = this.getAllNodes();
		ArrayList<Node> nonCommutative = new ArrayList<Node>();
		for (int i = 0; i < allNodes.length; i++) {
			String type = allNodes[i].getNodeType();
			if(type.equals("Division")||type.equals("Power")||type.equals("Subtraction")){
				nonCommutative.add(allNodes[i]);
			}
		}
		int size = nonCommutative.size();
		if(size==0){return null;}
		int index = (int)(GENERATOR.nextDouble()*size);
		return nonCommutative.get(index);
	}
	
	
//	From: http://cswww.essex.ac.uk/staff/poli/gp-field-guide/24RecombinationandMutation.html
//	The uniform selection of crossover points leads to crossover operations frequently 
//	exchanging only very small amounts of genetic material (i.e., small subtrees); 
//	many crossovers may in fact reduce to simply swapping two leaves.
//	Koza ( 1992) suggested the widely used approach of 
//	choosing functions 90% of the time and leaves 10% of the time. 
//	But we use a weighted probability to select the nodes for crossover
//	Get a node with a probability weighted by the number of descendants
	public Node getNodeWP(boolean useTopNode){
		Node[] allNodes = this.getAllNodes();
		
		if(m_CumulativeWeights == null) {
			Double[] cumulativeWeights = new Double[allNodes.length];
			double sumNumDescendants = 0;
			for (int i = 0; i < allNodes.length; i++) {
//				Add one because otherwise, the leaf nodes would have weight of 0
				sumNumDescendants += (allNodes[i].numDescendants() + 1.0);
			}
			double cumulative = 0.0;
			for (int i = 0; i < allNodes.length; i++) {
//				Add one because otherwise, the leaf nodes would have weight of 0				
				cumulative += ((allNodes[i].numDescendants() + 1.0)/sumNumDescendants);
				cumulativeWeights[i] = cumulative;
			}
			m_CumulativeWeights = cumulativeWeights;
		}
		
		if(useTopNode) {
			double value = GENERATOR.nextDouble();
			for (int i = 0; i < allNodes.length; i++) {
				if(value < (m_CumulativeWeights[i])){
					return allNodes[i];
				}
			}
		}
		else {
			if(this.numNodes() == 1) {return null;}
			Node returnNode = null;
			do {
				double random = GENERATOR.nextDouble();
				for (int i = 0; i < allNodes.length; i++) {
					if(random < (m_CumulativeWeights[i])){
						returnNode = allNodes[i];
						break;
					}
				}
			}while(returnNode.equals(this.getTopNode()));
			return returnNode;
		}
		return null;
	}
	
public Node getNonTerminal(boolean useTopNode) {
//	Verify that it has a non-terminal node
	boolean hasNonTerminal = false;
	for (Node node : this.getAllNodes()) {
		if(!node.getNodeType().equals("Distance") && !node.getNodeType().equals("Constant")) {
			boolean isTop = node.equals(this.getTopNode());
			if(!isTop) {
				hasNonTerminal = true;
				break;
			}
			else if(isTop && useTopNode) {
				hasNonTerminal = true;
				break;
			}
		}
	}
	if(hasNonTerminal) {
		Node returnNode;
		do {
			returnNode = this.getNodeWP(useTopNode);
		}while(returnNode.getNodeType().equals("Distance")||returnNode.getNodeType().equals("Constant"));
		return returnNode;
	}
	return null;
}
public int getComplexity(){
	if(m_Complexity == null){
//			//From: GENETIC PROGRAMMING THEORY AND PRACTICE II, page 83, Chapter 17
//			//PARETO-FRONT EXPLOITATION IN SYMBOLIC
//			//REGRESSION
//			//Guido F. Smits ; and Mark Kotanchek
//			//https://doi.org/10.1007/0-387-23254-0_17
//			Node[] allNodes = this.getAllNodes();
//			Integer complexity = 0;
//			for(Node node : allNodes){
//				complexity += node.getComplexity(0);
//			}
//			m_Complexity = complexity;
		m_Complexity = this.getTopNode().getComplexity(0);
	}
	return m_Complexity;
}

public void increaseNumTimesSelected() {
	Node[] nodes = this.getAllNodes();
	for (int i = 0; i < nodes.length; i++) {
		nodes[i].increaseNumTimesSelected();
	}
	m_NumTimesSelected = this.numTimesSelected() + this.numNodes();
}
	
public double numTimesSelected(){
	if(m_NumTimesSelected == 0.0){
		Node[] nodes = this.getAllNodes();
		double numTimes = 0;
		for (int i = 0; i < nodes.length; i++) {
			numTimes += nodes[i].getNumTimesSelected();
		}
		m_NumTimesSelected = numTimes;
	}
	return m_NumTimesSelected;
}
	
public int getDepth(){
	return this.getDepth(this.getTopNode());
}
private int getDepth(Node node) {
    if (node == null){
        return -1;
    }
    else{
        /* compute the depth of each subtree */
    	int numChildren = node.numChildNodes();
        int lDepth;
        int rDepth;
    	if(numChildren == 0	){
    		lDepth = this.getDepth(null);
        	rDepth = this.getDepth(null);
    	}
    	else if(numChildren == 1){
    		lDepth = this.getDepth(node.getChildNode(0));
        	rDepth = this.getDepth(null);
    	}
    	else if(numChildren == 2){
    		lDepth = this.getDepth(node.getChildNode(0));
        	rDepth = this.getDepth(node.getChildNode(1));
    	}
    	else{
    		lDepth = Integer.MAX_VALUE;
    		rDepth = Integer.MAX_VALUE;
    		Status.basic("Problem getting depth of tree in class TreePotential");
        		System.exit(0);
        	}
  
            /* use the larger one */
        if (lDepth > rDepth){
            return (lDepth + 1);
        }
         else{
            return (rDepth + 1);
         }
    }
}
    
public void setComplexity(int complexity){
	m_Complexity = complexity;
}
public void setNumNodes(int numNodes){
	m_NumNodes = numNodes;
}
    
public int numDistanceNodes() {
  int num = 0;
  for (Node node : this.getAllNodes()) {
	if(node.getNodeType().equals("Distance")) {
		num++;
	}
  }
  return num;
}   
    
public int depthSums(){
	return this.depthSums(this.getTopNode());
}
private int depthSums(Node node) {
    if (node == null){
        return -1;
    }
    else{
        /* compute the depth of each subtree */
    	int numChildren = node.numChildNodes();
        int lDepth;
        int rDepth;
    	if(numChildren == 0	){
    		lDepth = this.depthSums(null);
        	rDepth = this.depthSums(null);
    	}
    	else if(numChildren == 1){
    		lDepth = this.depthSums(node.getChildNode(0));
        	rDepth = this.depthSums(null);
    	}
    	else if(numChildren == 2){
    		lDepth = this.depthSums(node.getChildNode(0));
        	rDepth = this.depthSums(node.getChildNode(1));
    	}
    	else{
    		lDepth = Integer.MAX_VALUE;
    		rDepth = Integer.MAX_VALUE;
    		Status.basic("Problem getting depth of tree in class TreePotential");
        		System.exit(0);
        	}
  
            /* use the larger one */
        if (lDepth > rDepth){
        	if(node.getNodeType().equals("SiteSum")) {
        		return lDepth + 1;
        	}
            return lDepth;
        }
         else{
         	if(node.getNodeType().equals("SiteSum")) {
        		return rDepth + 1;
        	}
            return rDepth;
         }
    }
}
   
}
