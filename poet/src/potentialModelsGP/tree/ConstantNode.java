/*
 * Created on Aug 9, 2016
 *
 */
package potentialModelsGP.tree;

import java.text.DecimalFormat;
import java.util.ArrayList;

import evaluator.StructureNet.Bond;

public class ConstantNode extends Node {
  private double m_Value;
  
  public ConstantNode() {
    super(new Node[0]);
  }
  
  public ConstantNode(double value) {
    super (new Node[0]);
    m_Value = value;
  }
  public ConstantNode(Double randomValue) {
	    super (new Node[0]);
	    if(randomValue!=null){m_Value = randomValue;}
	    else{m_Value = Node.generateRandomValue();}
	  }
  public double getValue() {
    return m_Value;
  }

  public String toString() {
    return "" + m_Value;
  }

  @Override
  public double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) {
    return m_Value;
  }
  
  public void setValue(double value) {
    m_Value = value;
  }

  @Override
  public Node generateNode(Node childNode) {
	return new ConstantNode(Node.generateRandomValue());
  }

  @Override
  public double calculateDerivative(Bond bond, ConstantNode variable, matsci.structure.Structure.Site definingSite, Integer axis) {
	if(axis != null){return 0;}
	if(variable == null){return 0;}
    if (variable == this) {return 1;}
    return 0;
  }

  @Override
  public Node copy() { 
    Node copy = new ConstantNode(m_Value);
	copy.setValues(this.getNumTimesSelected() );
    return copy;	        
  }
  
  @Override
  public String getNodeType(){
	  return "Constant";
  }
  @Override
  public Integer getComplexity(){
	  return 1;
  }
  @Override
  public ArrayList<String> getExpression(boolean symbolic){
	  if(symbolic) {
		  ArrayList<String> aL = new ArrayList<String>();
		  aL.add("c");
		  return aL;
	  }
	  else {
		  ArrayList<String> aL = new ArrayList<String>();
		  String stringValue = Double.toString(this.m_Value);
		  stringValue = stringValue.replace("E", "*10^");
		  aL.add(0,stringValue);
		  return aL;
	  }
  }
  @Override
  public Node getLeft(){
	  return null;
  }
  @Override
  public Node getRight(){
	  return null;
  }
  @Override
  public Double getFromKnownValues(Bond bond){
//	  It is faster to calculate this value than to cache it
	  return null;
	  }
  @Override
  public void putInKnownValues(Bond bond, double value){
	  }

@Override
public Node generateNode(Node child0, Node child1) {
	return null;
}

@Override
public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
	return null;
}

@Override
public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {
}
@Override
public String getSymbol() {
	DecimalFormat df = new DecimalFormat("0.0E0");
	return df.format(m_Value);
}
}
