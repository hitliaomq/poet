/*
 * Created on Aug 29, 2016
 *
 */
package potentialModelsGP.tree;

import java.util.ArrayList;

import evaluator.NotFiniteValueException;
import evaluator.StructureNet.Bond;

public class DivisionNode extends Node {

  public DivisionNode() {
    super(new Node[0]);
  }
  
  public DivisionNode(Node numerator, Node denominator) {
    super(new Node[] {numerator, denominator});
  }

  @Override
  public double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
	double denominator = m_ChildNodes[1].getValue(bond, definingSite, axis, daxis);
	
////  Protected division.
////	Potentially avoids classifying a tree with good nodes just because it has a single bad node with division by zero
//	if(denominator == 0.0) {return 1.0;}
	
    return m_ChildNodes[0].getValue(bond, definingSite, axis, daxis) / denominator;
  }

  @Override
  public Node generateNode(Node childNode) {
    boolean isChildNumerator = GENERATOR.nextBoolean();
    if (isChildNumerator) {
      Node denominator = new ConstantNode(Node.generateRandomValue());
      return new DivisionNode(childNode, denominator);
    }
    Node numerator = new ConstantNode(Node.generateRandomValue());
    return new DivisionNode(numerator, childNode);
  }

  @Override
  public double calculateDerivative(Bond bond, ConstantNode variable, matsci.structure.Structure.Site definingSite, Integer axis) throws NotFiniteValueException {
    //
    // d/dx (f/g) = (-f/g^2)g' + (f'/g)
    //
    double f = m_ChildNodes[0].getValue(bond, definingSite, axis, null);
    double g = m_ChildNodes[1].getValue(bond, definingSite, axis, null);
    
////  Protected division.
////	Potentially avoids classifying a tree with good nodes just because it has a single bad node with division by zero
//	if(g == 0.0) {return 0.0;}
	
    double fPrime = m_ChildNodes[0].getDerivative(bond, variable, definingSite, axis);
    double gPrime = m_ChildNodes[1].getDerivative(bond, variable, definingSite, axis);
    
    return (-f/(g*g))*gPrime + fPrime / g;
  }

  @Override
  public Node copy() {			 
	    Node copy = new DivisionNode(m_ChildNodes[0], m_ChildNodes[1]);
	    copy.setValues(this.getNumTimesSelected());
	    return copy;
  }

  @Override
  public String getNodeType(){
	  return "Division";
  }
  @Override
  public Integer getComplexity(){
	  return 1;
  }
  @Override
  public ArrayList<String> getExpression(boolean symbolic){
	  ArrayList<String> aL = new ArrayList<String>();
	  aL.add(0,"((");
	  aL.addAll(1,m_ChildNodes[0].getExpression(symbolic));
	  aL.add(aL.size(),")/(");
	  aL.addAll(aL.size(),m_ChildNodes[1].getExpression(symbolic));
	  aL.add(aL.size(),"))");
	  return aL;
  }
  @Override
  public Node getLeft(){
	  return m_ChildNodes[0];
  }
  @Override
  public Node getRight(){
	  return m_ChildNodes[1];
  }
  @Override
  public Double getFromKnownValues(Bond bond){
//	  It is faster to calculate this value than to cache it
	  return null;
	  }
  @Override
  public void putInKnownValues(Bond bond, double value){
	  }

@Override
public Node generateNode(Node child0, Node child1) {
    return new DivisionNode(child0, child1);
}

@Override
public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {
	// TODO Auto-generated method stub
	
}
@Override
public String getSymbol() {
	return "/";
}
}

