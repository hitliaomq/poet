/*
 * Created on Nov 4, 2016
 *
 */
package potentialModelsGP.tree;

import java.util.ArrayList;

import evaluator.NotFiniteValueException;
import evaluator.StructureNet.Bond;

public class SubtractionNode extends Node {

  public SubtractionNode() {
    super(new Node[0]);
  }
  
  public SubtractionNode(Node child1, Node child2) {
    super (new Node[] {child1, child2});
  }

  @Override
  public double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
	return m_ChildNodes[0].getValue(bond, definingSite, axis, daxis) - m_ChildNodes[1].getValue(bond, definingSite, axis, daxis);
  }

  @Override
  public Node generateNode(Node childNode) {
	    Node value = new ConstantNode(Node.generateRandomValue());//0
	    if(GENERATOR.nextBoolean()) {
	    	return new SubtractionNode(value,childNode);
	    }
	    return new SubtractionNode(childNode, value);
  }

  @Override
  public double calculateDerivative(Bond bond, ConstantNode variable, matsci.structure.Structure.Site definingSite, Integer axis) throws NotFiniteValueException {
    return m_ChildNodes[0].getDerivative(bond, variable, definingSite, axis) - m_ChildNodes[1].getDerivative(bond, variable, definingSite, axis);
  }

  @Override
  public Node copy() {
      Node copy = new SubtractionNode(m_ChildNodes[0], m_ChildNodes[1]);
      copy.setValues(this.getNumTimesSelected());
      return copy; 
  }
  
  @Override
  public String getNodeType(){
      return "Subtraction";
  }
  @Override
  public Integer getComplexity(){
      return 1;
  }
  @Override
  public ArrayList<String> getExpression(boolean symbolic){
      ArrayList<String> aL = new ArrayList<String>();
      aL.add(0,"((");
      aL.addAll(1,m_ChildNodes[0].getExpression(symbolic));
      aL.add(aL.size(),")-(");
      aL.addAll(aL.size(),m_ChildNodes[1].getExpression(symbolic));
      aL.add(aL.size(),"))");
      return aL;
  }
  @Override
  public Node getLeft(){
	  return m_ChildNodes[0];
  }
  @Override
  public Node getRight(){
	  return m_ChildNodes[1];
  }
  @Override
  public Double getFromKnownValues(Bond bond){
//	  It is faster to calculate this value than to cache it
	  return null;
	  }
  @Override
  public void putInKnownValues(Bond bond, double value){
	  }

@Override
public Node generateNode(Node child0, Node child1) {
    return new SubtractionNode(child0, child1);
}

@Override
public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
	return null;
}

@Override
public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {
}
@Override
public String getSymbol() {
	return "-";
}
}
