package evaluator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import matsci.Species;
import matsci.io.app.log.Status;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.LinearBasis;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.util.MSMath;
import potentialModelsGP.tree.ConstantNode;
import util.Statistics;
import util.Util;

public class StructureNet{	
	
	private final Structure m_Structure;
	private final Bond[] m_DefiningBonds;
	private final double m_CutoffDistance;
	private final Bond[][] m_BondsByDefiningSiteIncludingBondWithItself;
	private final int m_NumDefiningSites;
	private StructureNet[][] m_NetsStress = new StructureNet[6][2];
	private StructureNet[] m_Nets10PercentLinear;
	public final static double m_dStrainStress = 1E-6;
	
	public StructureNet(Structure structure, double cutoffDistance){
		m_NumDefiningSites = structure.numDefiningSites();
		m_CutoffDistance = cutoffDistance;
		m_Structure = structure;
		// Each element of the first dimension corresponds to a defining site.
		m_BondsByDefiningSiteIncludingBondWithItself = new Bond[structure.numDefiningSites()][];

		ArrayList<Bond> definingBonds = new ArrayList<Bond>();
		
		// Fill m_BondsByDefiningSite with the bonds for each defining site
		for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
			
			Structure.Site definingsite = structure.getDefiningSite(siteNum);
			StructureNet.Site netSite = new StructureNet.Site(definingsite);
			Structure.Site[] neighbors = structure.getNearbySites(definingsite.getCoords(), m_CutoffDistance, false);
			
			// Each element of the first dimension contains an array of bonds
			
			Bond[] bonds = new Bond[neighbors.length+1];
			//The bond at [siteNum][0] is a bond of the defining site with itself
			Bond bond0 = new Bond(netSite, netSite);
			bonds[0] = bond0;
			definingBonds.add(bond0);
			
			m_BondsByDefiningSiteIncludingBondWithItself[siteNum] = bonds;
			
			// Fill bonds.
			for(int neighborNum = 0; neighborNum < neighbors.length; neighborNum++){
				
				Structure.Site neighbor = neighbors[neighborNum];
				StructureNet.Site netNeighbor = new StructureNet.Site(neighbor);
				Bond bond = new Bond(netSite, netNeighbor);
				
			check0:
				
				// For each new bond, check to see if a site in the same coordinates has already been created
				for(int i = 0; i < m_BondsByDefiningSiteIncludingBondWithItself.length; i++){
					//Start at j=1 because we don't want to replace the bond of the defining site with itself
					for(int j = 1; j < m_BondsByDefiningSiteIncludingBondWithItself[i].length; j++){
						if(m_BondsByDefiningSiteIncludingBondWithItself[i][j]==null){break check0;}
						Coordinates otherCoords = m_BondsByDefiningSiteIncludingBondWithItself[i][j].getNeighborSiteOfBond().getStructureSite().getCoords();
						if(neighbor.getCoords().isCloseEnoughTo(otherCoords)){
							bond = new Bond(netSite,m_BondsByDefiningSiteIncludingBondWithItself[i][j].getNeighborSiteOfBond());
							break check0;
						}
					}
				}
				definingBonds.add(bond);
				bonds[neighborNum+1] = bond;
			}	
			m_BondsByDefiningSiteIncludingBondWithItself[siteNum] = bonds;
		}
		m_DefiningBonds = new Bond[definingBonds.size()];
		definingBonds.toArray(m_DefiningBonds);
	}
	
	public Bond[] getDefiningBonds(){
		return m_DefiningBonds;
	}
	
	/**
	 * Each element of the first dimension corresponds to a defining site.
	 * Each element of the second dimension corresponds to a bond between
	 * the defining site and a neighbor
	 * The first element of the second dimension if the bond of the defining site with itself
	 * @return
	 */
	public Bond[][] getBondsByDefiningSiteIncludingBondWithItself(){
		return m_BondsByDefiningSiteIncludingBondWithItself;
	}
	
	public Structure getStructure() {
	  return m_Structure;
	}
	
	public int numDefiningSites() {
	  return m_NumDefiningSites;
	}
	
	public int numSites(){
		ArrayList<Structure.Site> sites = new ArrayList<Structure.Site>();
//		Initialize by adding a site
		sites.add(this.getDefiningSite(0));
		for (int i = 0; i < m_BondsByDefiningSiteIncludingBondWithItself.length; i++) {
			for (int j = 0; j < m_BondsByDefiningSiteIncludingBondWithItself[i].length; j++) {
				Structure.Site thisSite = m_BondsByDefiningSiteIncludingBondWithItself[i][j].m_NeighborSite.m_StructureSite;
//				if the coordinates are not close to the coordinates of a site in sites, then add thisSite to sites
				boolean add = true;
				for (Structure.Site site : sites) {
					if(thisSite.getCoords().isCloseEnoughTo(site.getCoords())){
						add = false;
						break;
					}
				}
				if(add){
					sites.add(thisSite);
				}
			}
		}
		return sites.size();
	}
	
	public double numPairs(){
		int numPairs = 0;
		for (Bond[] bondArray : m_BondsByDefiningSiteIncludingBondWithItself) {
			numPairs += (bondArray.length - 1);
		}
		return numPairs/2.0;
	}
	
	public double getCutoffDistance() {
		return m_CutoffDistance;
	}
	
	public int avgCoordinationNumber() {
		int avg = 0;
		for (Bond[] bonds : m_BondsByDefiningSiteIncludingBondWithItself) {
			avg += bonds.length - 1;
		}
		avg /= m_BondsByDefiningSiteIncludingBondWithItself.length;
		return avg;
	}
	public double minOrMaxBondLength(boolean max) {
		double value;
		if(max) {value=Double.NEGATIVE_INFINITY;}
		else {value=Double.POSITIVE_INFINITY;}
		for (Bond[] bonds : m_BondsByDefiningSiteIncludingBondWithItself) {
			for (Bond bond : bonds) {
				double bl = bond.getBondLength();
				if(max && bl > value) {value = bl;}
				else if(!max && bl < value && (Statistics.compare(bl, 0)!=0)) {value = bl;}
			}
		}
		return value;
	}
	public Structure.Site getDefiningSite(int siteNum){
		return m_BondsByDefiningSiteIncludingBondWithItself[siteNum][0].getCentralSiteOfBond().getDefiningSite();
	}
	
//	/**
//	 * Create a new StructureNet which is identical to this one, except for the coordinates. 
//	 * @param cellVectorsAndcoordinates The first 9 indices of cellVectorsAndcoordinates correspond to the cell vectors
//	 * @return StructureNet with new coordinates and cell vectors
//	 */
//	public StructureNet getNewStructureNet(double[] cellVectorsAndcoordinates) {
//		StructureBuilder builder = new StructureBuilder(this.getStructure());
//		builder.removeAllSites();
//		
//		Vector[] cellVectors  = new Vector[3];
//		for (int i = 0; i < cellVectors.length; i++) {
//			double[] array = new double[3];
//			//The first 9 indices correspond to the cell vectors
//			for (int j = 0; j < array.length; j++) {
//				array[j] = cellVectorsAndcoordinates[3*i+j];
//			}
//			cellVectors[i] = new Vector(array);
//		}
//		
//		builder.setCellVectors(cellVectors);
//		
//		//Counter for coordinates
//		int c = 0;
//		for (int i = 0; i < this.numDefiningSites(); i++) {
//			double[] siteCoords = new double[3];
//			for (int j = 0; j < 3; j++) {
//				siteCoords[j] = cellVectorsAndcoordinates[c];
//				c++;
//			}
//			Coordinates coords = new Coordinates(siteCoords, CartesianBasis.getInstance());
//			builder.addSite(coords, this.getDefiningSite(i).getSpecies());
//		}
//		
//		return new StructureNet(new Structure(builder), this.getCutoffDistance());
//	}
	
//	/**
//	 * Create a new StructureNet which is identical to this one, except for the coordinates and the cell vectors. 
//	 * If one of the parameters is null, then it is ignored
//	 * @return StructureNet with new coordinates and, or cell vectors
//	 */
//	public StructureNet getNewStructureNet(double[] cellVectors, double[] coordinates) {
//		StructureBuilder builder = new StructureBuilder(this.getStructure());
//		
//		if(cellVectors!=null) {
//			//Keep the same fractional coordinates
//			Coordinates[] fractionalCoordinates = new Coordinates[this.numDefiningSites()];
//			
//			for (int i = 0; i < this.numDefiningSites(); i++) {
//				fractionalCoordinates[i] = this.getDefiningSite(i).getCoords();
//			}
//			
//			Vector[] vectors  = new Vector[3];
//			for (int i = 0; i < vectors.length; i++) {
//				double[] array = new double[3];
//				for (int j = 0; j < array.length; j++) {
//					array[j] = cellVectors[3*i+j];
//				}
//				vectors[i] = new Vector(array);
//			}
//			builder.setCellVectors(vectors);
//			builder.setSiteCoordinates(fractionalCoordinates);
//		}
//		
//		if(coordinates != null) {
//			//Counter for coordinates
//			builder.removeAllSites();
//			int c = 0;
//			for (int i = 0; i < this.numDefiningSites(); i++) {
//				double[] siteCoords = new double[3];
//				for (int j = 0; j < 3; j++) {
//					siteCoords[j] = coordinates[c];
//					c++;
//				}
//				Coordinates coords = new Coordinates(siteCoords, CartesianBasis.getInstance());
//				builder.addSite(coords, this.getDefiningSite(i).getSpecies());
//			}
//		}
//		
//		return new StructureNet(new Structure(builder), this.getCutoffDistance());
//	}
	
	/**
	 * 
	 * @param newCellVectors
	 * @return StructureNet with new cell vectors, but same fractional coordinates with respect to the cell vectors
	 */
	public StructureNet getStructureNet(Vector[] newCellVectors) {
		LinearBasis basis0 = new LinearBasis(this.getStructure().getCellVectors());
		LinearBasis basis1 = new LinearBasis(newCellVectors);
		StructureBuilder builder = new StructureBuilder(this.getStructure());
		Coordinates[] coordinates = new Coordinates[this.numDefiningSites()];
		for (int i = 0; i < this.numDefiningSites(); i++) {
			double[] coords = this.getDefiningSite(i).getCoords().getCoordArray(basis0);
			coordinates[i] = new Coordinates(coords, basis1);
		}		
		
		builder.setCellVectors(newCellVectors);
		builder.setSiteCoordinates(coordinates);
		Structure structure = new Structure(builder);
		
		return new StructureNet(structure, this.getCutoffDistance());
	}
	
	
	public static Structure removeVacancies(Structure structure) {
		StructureBuilder builder = new StructureBuilder(structure);
		builder.removeAllSites();
		//Remove vacancies
		for (int i = 0; i < structure.numDefiningSites(); i++) {
			if(structure.getDefiningSite(i).getSpecies() != Species.getVacancy()) {
				builder.addSite(structure.getDefiningSite(i).getCoords(), structure.getDefiningSite(i).getSpecies());
			}
		}	
		return new Structure(builder);
	}
	
	/**
	 * 
	 * @param newCoordinates
	 * @return StructureNet with new coordinates but same cell vectors
	 */
	public StructureNet getStructureNet(Coordinates[] newCoordinates) {
		
		StructureBuilder builder = new StructureBuilder(this.getStructure());
		
		builder.setCellVectors(this.getStructure().getCellVectors());
		builder.setSiteCoordinates(newCoordinates);
		
		StructureNet returnNet = new StructureNet(new Structure(builder), this.getCutoffDistance());
		
		return returnNet;
	}
	
	/**
	 * X = 0, Y = 1, Z = 2. Example: 01 means XY
	 * @param direction of displacement
	 * @param plane that is displaced (specifically, the normal to that plane)
	 * @return the engineering shear strain with respect to orthogonal cell vectors
	 */
	public double getShearStrain(int direction, int plane) {
		//the cell vector that corresponds to the plane
		Vector v = this.getStructure().getCellVectors()[plane];
		//the component of the vector along the direction of the plane
		double vplane = v.getCartesianDirection()[plane];
		//the component of the vector along the direction of displacement
		double vdirection = v.getCartesianDirection()[direction];
		return Math.tan(vdirection/vplane);
	}
	
	/**
	 * get a net with a deformation (in units of length) in the component vectorComponent of vector, 
	 * but maintaining the fractional coordinates
	 * @param vector
	 * @param vectorComponent
	 * @param deformation
	 * @return
	 */
	public StructureNet getNet(int vector, int vectorComponent, double deformation) {
		Vector[] newVectors = new Vector[3];
		for (int i = 0; i < 3; i++) {
			double[] coords = this.getStructure().getCellVectors()[i].getCartesianDirection();
			if(i == vector) {
				coords[vectorComponent] += deformation;
			}
			newVectors[i] = new Vector(coords);
		}
		
		return this.getStructureNet(newVectors);
	}
	
	/**
	 * 
	 * @param strain = normal strain. X = 0, Y = 1, Z = 2. Example: 0 implies strain along XX
	 * @param direction = direction of the displacement
	 * @return StructureNet deformed by the normal strain
	 */
	
	public StructureNet getStructureNetNormalStrain(int direction, double dStrain) {
//		StructureBuilder builder = new StructureBuilder(this.getStructure());
		
		Vector[] vectors0 = this.getStructure().getCellVectors();
		Vector[] vectors = new Vector[3];
		for (int i = 0; i < vectors.length; i++) {
			double[] cartArray	= vectors0[i].getCartesianDirection();
			cartArray[direction] *= (1.0+dStrain);
			vectors[i] = new Vector(cartArray);
		}
		return this.getStructureNet(vectors);
//		builder.setCellVectors(vectors);
//		
//		Coordinates[] coords = new Coordinates[this.getStructure().numDefiningSites()];
//		for (int i = 0; i < this.getStructure().numDefiningSites(); i++) {
//			Coordinates oldCoords = this.getStructure().getDefiningSite(i).getCoords();
//			double[] newCoordArr = oldCoords.getCartesianArray();
//			newCoordArr[direction] *= (1.0+dStrain);
//			Coordinates newCoords = new Coordinates(newCoordArr, CartesianBasis.getInstance());
//			coords[i] = newCoords;
//		}
//		
//		builder.setSiteCoordinates(coords);
//		
//		return new StructureNet(new Structure(builder), this.getCutoffDistance());
	}	
	
	/**
	 * 
	 * @param strain = shear strain. X = 0, Y = 1, Z = 2. Example: 01 implies strain along XY
	 * @param direction
	 * @param plane
	 * @return sheared StructureNet
	 */
	public StructureNet getStructureNetShear( int direction, int plane, double dStrain) {
		Vector[] vectors0 = this.getStructure().getCellVectors();
		Vector[] vectors = this.getStructure().getCellVectors();

		for (int i = 0; i < vectors.length; i++) {
			double[] cartArray	= vectors0[i].getCartesianDirection();
			double magVectorPlane = cartArray[plane];
			double magVectorDir0 = cartArray[direction];

			//If the vector does not have a component in the index "plane"
			//(i.e., if the vector is on the plane), then do not modify it
			if(Statistics.compare(magVectorPlane, 0.0)!=0) {
				//the displacement is proportional to the component along the plane
				cartArray[direction] = magVectorDir0 + dStrain*magVectorPlane;
			}
			vectors[i] = new Vector(cartArray);
		}
		return this.getStructureNet(vectors);
		
	}
	
	/**
	 * 
	 * @param dstrain Note that X = 0, Y = 1, Z = 2. Example: 01 means strain XY
	 * @param direction
	 * @param plane
	 * @return
	 */
	public StructureNet getNetForStress(int direction, int plane, int dStrainIndex) {
		int index;
		if(direction==0 && plane==0) 	  {index = 0;}//XX
		else if(direction==1 && plane==1) {index = 1;}//YY
		else if(direction==2 && plane==2) {index = 2;}//ZZ
		else if(direction==0 && plane==1) {index = 3;}//XY
		else if(direction==1 && plane==2) {index = 4;}//YZ
		else if(direction==2 && plane==0) {index = 5;}//ZX
		else{
			index=Integer.MIN_VALUE;
			Status.basic("Invalid index");
			new Exception().printStackTrace();
		}
		if(m_NetsStress[index][0]==null) {
			if(direction == plane) {
				m_NetsStress[index][0] = this.getStructureNetNormalStrain(direction, -m_dStrainStress);
				m_NetsStress[index][1] = this.getStructureNetNormalStrain(direction, m_dStrainStress);
			}
			else {
				m_NetsStress[index][0] = this.getStructureNetShear(direction,plane, -m_dStrainStress);
				m_NetsStress[index][1] = this.getStructureNetShear(direction,plane, m_dStrainStress);
			}
		}
		return m_NetsStress[index][dStrainIndex];

	}
	/**
	 * Useful to compute lattice parameter
	 * @return this structure with a linear strain in XX, YY and ZZ of 10% or 1%
	 */
	public StructureNet[] getNetsStrain(boolean tenpercent) {
		if(tenpercent && m_Nets10PercentLinear != null) {
			return m_Nets10PercentLinear;
		}
		StructureNet[] sS = new StructureNet[5];
		double[] linStrains = new double[sS.length];
		double strain = tenpercent ? -0.1 : -0.01;
		for (int i = 0; i < linStrains.length; i++) {
			//In case you want to convert to linear from volumetric strain: linStrain = Math.pow(volStrain + 1, 1.0/3) - 1.0;
			linStrains[i] = strain;
			strain += tenpercent ? 0.05 : 0.005;
		}
		for (int i = 0; i < linStrains.length; i++) {
			StructureNet net = this.getStructureNetNormalStrain(0, linStrains[i]);
			net = net.getStructureNetNormalStrain(1, linStrains[i]);
			net = net.getStructureNetNormalStrain(2, linStrains[i]);
			sS[i] = net;
		}
		if(tenpercent) {
			m_Nets10PercentLinear = sS;
		}
		return sS;
	}
	
	/**
	 * Rotate the net to align the cell vector a with the x axis and the cell vector b in the x-y plane. Useful because LAMMPS outputs structures in this way.
	 * @return 
	 */
	public StructureNet getAlignedLAMMPS() {
		double[] a0 = this.getStructure().getCellVectors()[0].getCartesianDirection();
		double[] b0 = this.getStructure().getCellVectors()[1].getCartesianDirection();
		double[] c0 = this.getStructure().getCellVectors()[2].getCartesianDirection();
		
		double[][] matrix1 = Util.getRotationMatrix(MSMath.normalize(a0), new double[] {1,0,0});
		double[] b1 = MSMath.matrixTimesVector(matrix1, b0);
		
		//Rotation around the x axis to put the cell vector b in the XY plane

		//angle of YZ projection of vector with respect to Y axis
		double[] b1YZ = new double[] {0,b1[1],b1[2]};
		double angle = Util.getAngle(b1YZ, new double[] {0,1,0});

		//Consider that rotations are counterclockwise
		if(b1[2]>0) {
			angle = 2*Math.PI - angle;
		}

		//Rotation around x axis to align vector b on the xy plane
		double[][] matrix2 = Util.getRotationMatrix(angle, 0);
				
		matrix2 = MSMath.matrixMultiply(matrix2, matrix1);
		double[] a2 = MSMath.matrixTimesVector(matrix2, a0);
		double[] b2 = MSMath.matrixTimesVector(matrix2, b0);
		double[] c2 = MSMath.matrixTimesVector(matrix2, c0);
		
		Vector[] newCellVectors = new Vector[] {new Vector(a2),new Vector(b2),new Vector(c2)};
		
		return this.getStructureNet(newCellVectors);
	}
	
	/**
	 * Rotate the net to align the cell vector c with the z axis and the cell vector b in the y-z plane. Useful because VASP outputs structures in this way from MD.
	 * @return
	 */
	public StructureNet getAlignedVASP() {
		double[] a0 = this.getStructure().getCellVectors()[0].getCartesianDirection();
		double[] b0 = this.getStructure().getCellVectors()[1].getCartesianDirection();
		double[] c0 = this.getStructure().getCellVectors()[2].getCartesianDirection();
		
		double[][] matrix1 = Util.getRotationMatrix(MSMath.normalize(c0), new double[] {0,0,1});
		double[] b1 = MSMath.matrixTimesVector(matrix1, b0);
		
		//Rotation around the z axis to put the cell vector b in the YZ plane
		
		//angle of XY projection of vector with respect to Y axis
		double[] b1XY = new double[] {b1[0], b1[1], 0};
		double angle = Util.getAngle(b1XY, new double[] {0,1,0});
		
		//Consider that rotations are counterclockwise
		if(b1[0] < 0) {
			angle = 2*Math.PI - angle;
		}
		
		//Rotation around z axis for cell vector b aligned on the YZ plane
		double[][] matrix2 = Util.getRotationMatrix(angle, 2);

		matrix2 = MSMath.matrixMultiply(matrix2, matrix1);
		
		double[] a2 = MSMath.matrixTimesVector(matrix2, a0);
		double[] b2 = MSMath.matrixTimesVector(matrix2, b0);
		double[] c2 = MSMath.matrixTimesVector(matrix2, c0);
		
		Vector[] newCellVectors = new Vector[] {new Vector(a2),new Vector(b2),new Vector(c2)};
		
		return this.getStructureNet(newCellVectors);
	}

	public class Site {
		private final Structure.Site m_StructureSite;
//		We need it to optimize the method getNeighbor(int) which is called by SiteSumNode
		private final int m_Index;
		private final Coordinates m_Coords;
		private Integer m_NumNeighbors;
		private Structure.Site m_DefiningSite;
		private Bond[] m_Bonds;
		
		public Site (Structure.Site structureSite){
			m_StructureSite = structureSite;
			m_Index = structureSite.getIndex();
			m_Coords = structureSite.getCoords();
			m_NumNeighbors = null;
			m_DefiningSite = m_Structure.getDefiningSite(m_Index);
			m_Bonds = null;
		}
	  public Structure.Site getStructureSite() {
		  return m_StructureSite;
	  }
	  public double getDistanceFrom(StructureNet.Site neighbor){
		  return m_Coords.distanceFrom(neighbor.m_Coords);
	  }
	  public int numBonds() {
			if(m_NumNeighbors==null){m_NumNeighbors = m_BondsByDefiningSiteIncludingBondWithItself[m_Index].length-1;return m_NumNeighbors;}
		    return m_NumNeighbors;
	  }
	  
	  public int numNeighbors() {
		if(m_NumNeighbors==null){m_NumNeighbors = m_BondsByDefiningSiteIncludingBondWithItself[m_Index].length-1;return m_NumNeighbors;}
	    return m_NumNeighbors;
	  }
	  	  
	  public Bond getBond(int bondNum) {
		if(m_Bonds == null){m_Bonds = m_BondsByDefiningSiteIncludingBondWithItself[m_Index];}
		return m_Bonds[bondNum+1];
	  }
	  
	  public Structure.Site getDefiningSite() {
		    return m_DefiningSite;
		  }
	}
	
	public class Bond {
		private final StructureNet.Site m_CentralSite;
		private final StructureNet.Site m_NeighborSite;
		private final double m_BondLength;
		private HashMap<InfoHashBond, Double> m_KnownBondLengthsNumApprox = new HashMap<InfoHashBond, Double>();
		
		public Bond (StructureNet.Site centralSite, StructureNet.Site neighborSite){
			m_CentralSite =  centralSite;
			m_NeighborSite = neighborSite;
			m_BondLength = m_CentralSite.getDistanceFrom(m_NeighborSite);
		}
		
		public double getBondLength(){
			return m_BondLength;
		}
		
		public StructureNet.Site getCentralSiteOfBond(){
			return m_CentralSite;
		}
		
		public StructureNet.Site getNeighborSiteOfBond(){
			return m_NeighborSite;			
		}

		public double getBondLength(Structure.Site definingSite, Integer axis, Double daxis) {
			if(definingSite == null){return m_BondLength;}
			else if(daxis == null){return m_BondLength;}
			if(m_CentralSite.equals(m_NeighborSite)) {return 0.0;}
			int index = definingSite.getIndex();
			if(m_CentralSite.m_Index != index && m_NeighborSite.m_Index != index){return m_BondLength;}

			Double value = m_KnownBondLengthsNumApprox.get(new InfoHashBond(index, axis));
			if(value != null){return value;}
			else{
				  Site central = m_CentralSite;
				  double[] centralCoords = central.getStructureSite().getCoords().getCartesianArray();
				  Site neighbor = m_NeighborSite;
				  double[] neighborCoords = neighbor.getStructureSite().getCoords().getCartesianArray();
				  			  
				  if(central.getDefiningSite().equals(definingSite)){
					  centralCoords[axis] += daxis;
				  }
				  else if(neighbor.getDefiningSite().equals(definingSite)){
					  neighborCoords[axis] += daxis; 
				  }
			
				  double x = centralCoords[0]-neighborCoords[0];
				  double y = centralCoords[1]-neighborCoords[1];
				  double z = centralCoords[2]-neighborCoords[2];
				  value = Math.sqrt((x*x)+(y*y)+(z*z));
				  
				  m_KnownBondLengthsNumApprox.put(new InfoHashBond(index, axis), value);
				  return value;
			}
		}
		
		public double getDerivative(ConstantNode variable, Structure.Site definingSite, Integer axis){
		  if(variable != null){return 0.0;}
		  if(m_CentralSite.equals(m_NeighborSite)) {return 0.0;}
		  
		  StructureNet.Site central = m_CentralSite;
		  StructureNet.Site neighbor = m_NeighborSite;
		  boolean centralIsDefining = central.getDefiningSite().equals(definingSite);
		  boolean neighborIsDefining = neighbor.getDefiningSite().equals(definingSite);

		  boolean return0 = !centralIsDefining && !neighborIsDefining;
		  if(return0){return 0.0;}
		  
		  double positionNeihborAxis = neighbor.getStructureSite().getCoords().getCartesianArray()[axis];
		  double positionCentralAxis = central.getStructureSite().getCoords().getCartesianArray()[axis];
		  
		  if(centralIsDefining){return -(positionNeihborAxis - positionCentralAxis)/this.getBondLength();}
		  else if(neighborIsDefining){return (positionNeihborAxis - positionCentralAxis)/this.getBondLength();}
		  else{return Double.NaN;}
		}
	}
	  public class InfoHashBond {
		    
		  	private int m_DefiningSite;
		    private int m_Axis;
		    
		    public InfoHashBond(int definingSite, int axis) {
		    	m_DefiningSite = definingSite;
		    	m_Axis = axis;
		    }
		    
		    @Override
		    public boolean equals(Object o) {
		        if (this == o) return true;
		        if (!(o instanceof InfoHashBond)) return false;
		        InfoHashBond infoHash = (InfoHashBond) o;
		        
		        return (infoHash.m_DefiningSite == m_DefiningSite) && (infoHash.m_Axis == m_Axis);
		    }

		    @Override
		    public int hashCode() {
		    	int num = Objects.hash(m_DefiningSite, m_Axis);
		        return num;
		    }
		  }
	
}


