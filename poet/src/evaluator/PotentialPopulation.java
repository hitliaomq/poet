package evaluator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import gAEngine.GAEngine;
import gAEngine.IIndividual;
import gAEngine.IPopulation;
import matsci.io.app.log.Status;
import potentialModelsGP.tree.TreePotential;
import util.Statistics;

public class PotentialPopulation implements IPopulation{

	private static final Random GENERATOR = new Random();
	public static int m_MaxOperations;
	public static int m_DynamicFrequency;
	public static String m_GlobalDirectory;
	public static int m_ProcessorNum;
	public static int m_NumProcessors;
    public static int m_numValidPerMethodPerDepth;
    public static int m_NumIsValidCalls = 0;
	
	private static PotentialSubPopulation m_Population;

	private int m_NumOperations = 0;
	private double m_AverageSize = 0;
	private double m_AverageDepth = 0;
	private double m_AverageComplexity = 0;
	
	private String m_SelectionMethod;
	private PotentialSubPopulation[] m_GlobalTrainingFrontierLC;
	private PotentialSubPopulation[] m_GlobalTrainingFrontierMSE;
	private ArrayList<IIndividual> m_ProcessorsIndividuals;
	private DataSet m_DataSet;
	private boolean m_ConvergedMSE = false;
	protected static int m_Tournament_Size = 10;
	
public PotentialPopulation(PotentialSubPopulation population, String selectionMethod,PotentialSubPopulation[] globalTrainingFrontierMSE,PotentialSubPopulation[] globalTrainingFrontierLC, DataSet dataSet, ArrayList<IIndividual> individualsFromOtherProcessors){
	m_DataSet = dataSet;
	m_Population = population;
	m_SelectionMethod = selectionMethod;
	m_NumOperations = 0;
	
	m_GlobalTrainingFrontierMSE = globalTrainingFrontierMSE;
	
	m_GlobalTrainingFrontierLC = globalTrainingFrontierLC;
	m_ProcessorsIndividuals = individualsFromOtherProcessors;
	this.updateGlobal();
}
	
//protected boolean add(InfoAddIndividual info, boolean fitnessByLC){
//	 boolean added = info.added();
//	 boolean converged = info.converged();
//	 ArrayList<IIndividual> individuals = info.getIndividuals();
//	 int popIndex = info.getPopIndex()+1;
//	 if(!added || individuals.size()==0 || converged){return converged;}
//	 
//	 for (IIndividual prospectiveIndividual : individuals) {
//		 InfoAddIndividual infotemp = this.addToPopulation(prospectiveIndividual, popIndex, fitnessByLC);
//		 this.add(infotemp,fitnessByLC);
//	 }
//	 return false;
//}
	

//private InfoAddIndividual addToPopulation(IIndividual prospectiveIndividual, int popIndex, boolean fitnessByLC){
//					
//	InfoAddIndividual info = null;
//	if(popIndex >= m_NumPops){
//		ArrayList<IIndividual> individuals = new ArrayList<IIndividual>();
//		info = new InfoAddIndividual(false, individuals, m_NumPops);
//		return info;
//	}
//	boolean added = false;
//	ArrayList<PotentialSubPopulation> subPopulations;
//	if(fitnessByLC){
//		subPopulations= m_SubPopulationsLC;
//	}
//	else{
//		subPopulations= m_SubPopulationsMSE;
//	}
//	int count = 0;
//	for (PotentialSubPopulation pop : subPopulations) {
//
//		if(count >= popIndex){
//			info = pop.add(prospectiveIndividual,null, null, fitnessByLC);
//			added = info.added();
//		}
//		if(added){
//			return info;
//		}
//		count++;
//	}
//	return info;
//}
	
//This method returns true if the added individual is the solution

private void writeAllInstancesFinished(String endConditioni) {
	//Check that all instances printed "End condition met"
	ArrayList<String> messages = new ArrayList<String>();
	for (int i = 1; i < m_NumProcessors+1; i++) {
		if(i == m_ProcessorNum) {
			messages.add(endConditioni);
			continue;
		}
	    try (Scanner scanner = new Scanner(new File(m_GlobalDirectory+"/"+i+"/log.java.run"))) {
		    //now read the file line by line...
		    while (scanner.hasNextLine()) {
		        String line = scanner.nextLine();
		        if(line.contains("End condition met")) { 
		            messages.add(line);
		            break;
		        }
		    }
//		    scanner.close();
	    } catch(FileNotFoundException e) { 
			Status.basic(e.toString());
			Status.basic(e.getMessage());
		}		
	}

	if(messages.size() == m_NumProcessors) {
		//Write a file indicating that all processors finished.
		try (PrintWriter pw1 = new PrintWriter(new BufferedWriter(new FileWriter(m_GlobalDirectory+"/Completed_all_instances.log")))) {
			int count = 1;
			for (String string : messages) {
				pw1.println("Instance "+count+" "+string);
				count++;
			}
		} catch (IOException e) {
			Status.basic(e.toString());
			Status.basic(e.getMessage());
		}
	}
}

@Override
public boolean add(IIndividual prospectiveIndividual){
	if(m_ConvergedMSE) {
		Status.basic("End condition met: Solution found!");
		this.updateGlobal();
		this.print(m_NumOperations);
		this.writeAllInstancesFinished("End condition met: Solution found!");
		return true;
	}
	
	m_NumOperations++;
	
	this.updateStatistics((PotentialEvaluator)prospectiveIndividual, m_NumOperations);

	if(m_ProcessorNum==2 && Statistics.compare(m_NumOperations % 1000, 0)==0) {
		//Optimize every individual with the CMA-ES only for one processor every n operations
		this.updateAll();
		
		for (PotentialSubPopulation potentialSubPopulation : m_GlobalTrainingFrontierMSE) {
			Status.basic("Optimizing with CMA-ES with default population size");
			Status.basic("SubsetName "+this.getDataSet().getSubSet());
			for (IIndividual ind : potentialSubPopulation.getIndividuals()) {
				Status.basic("Model Initial"+((PotentialEvaluator)ind).getPotential().getExpression(false));
				Status.basic(((PotentialEvaluator)ind).getDataSet().getTitle()+" Fitness Initial "+((PotentialEvaluator)ind).getFitness(null, false));
				ind = ((PotentialEvaluator)ind).optimizeConstants(new String[] {"CMA-ES"});
				Status.basic("Model Final"+((PotentialEvaluator)ind).getPotential().getExpression(false));
				Status.basic(((PotentialEvaluator)ind).getDataSet().getTitle()+" Fitness Final "+((PotentialEvaluator)ind).getFitness(null, false));
				this.addToPopulation(ind);
			}
		}
	} 
	
	this.addToPopulation(prospectiveIndividual);
	if(m_NumOperations >= m_MaxOperations){
		Status.basic("End condition met: maximum number of operations performed");
		this.updateGlobal();
		this.print(m_MaxOperations);
		this.writeAllInstancesFinished("End condition met: maximum number of operations performed");
		return false;
	}
	
	//Update the global population, refresh local copies, and change dynamic indices (keeping 
	//good individuals and deleting the rest)
	//Print the global population to the log file if the processor is the appropriate one
	if(Statistics.compare(m_NumOperations % m_DynamicFrequency, 0)==0){
		this.updateAll();
	}
	else if(Statistics.compare(m_NumOperations % 100, 0)==0 && m_NumOperations < 1010){
		this.updateAll();
	}
	
	//Refresh local copies 4 times the frequency of m_DynamicFrequency
	else if(Statistics.compare(m_NumOperations % (m_DynamicFrequency/4.0), 0)==0){
    	this.refreshLocalCopyOfGlobal("globalTrainingFrontierLC",true);
    	this.refreshLocalCopyOfGlobal("globalTrainingFrontierMSE",true);
		this.refreshLocalCopyOfProcessors();
	}

	return false;
}

private void updateAll() {
	this.refreshLocalCopyOfProcessors();
	this.updateGlobal();
	//Only one processor prints the global population to its log file
	//This processor is the only one that has m_PrintFrequency < Integer.MAX_VALUE/2
	if(m_ProcessorNum == 1) {
		this.print(m_NumOperations);
	}
	else {
		Status.basic("NumOperations "+m_NumOperations +" NumIsValidCalls "+m_NumIsValidCalls);
		Status.basic("AverageSize "+m_AverageSize);
		Status.basic("AverageDepth "+m_AverageDepth);
		Status.basic("AverageComplexity "+m_AverageComplexity);		
	}
	
	//Keep good individuals and delete the rest
	this.keepGoodIndividuals(50);
	//After keeping good individuals and deleting the rest, print them to 
	//make them available for other processors
	this.printProcessorIndividuals();
			
	//Create individuals to increase diversity
	//TODO Create only 6 individuals?
	this.createIndividuals(6, PotentialEvaluator.m_MinDepthRandomTree, PotentialEvaluator.m_MaxDepthRandomTree,(PotentialEvaluator)this.getIndividual());
}

private void updateStatistics(PotentialEvaluator evaluator, int numOperations) {
	double avgDepth = m_AverageDepth;
	double thisDepth = evaluator.getPotential().getDepth();
	m_AverageDepth = (avgDepth*(numOperations-1)+thisDepth)/(numOperations);
	double avgSize = m_AverageSize;
	double thisSize = evaluator.getPotential().numNodes();
	m_AverageSize = (avgSize*(numOperations-1)+thisSize)/(numOperations);
	double avgCmplx = m_AverageComplexity;
	double thisCmplx = evaluator.getPotential().getComplexity();
	m_AverageComplexity = (avgCmplx*(numOperations-1)+thisCmplx)/(numOperations);

}

private void updateGlobal() {
	//build a convex hull using MSE and one using PCC from the local population
	//then try to add the individuals from these frontiers to the global frontiers
	//Bin according to number of summations
	ArrayList<ArrayList<IIndividual>> bins = new ArrayList<ArrayList<IIndividual>>();
	for (int i = 0; i < PotentialEvaluator.m_MaxLoops; i++) {
		bins.add(new ArrayList<IIndividual>());
	}
	for (IIndividual indiv : m_Population.getIndividuals()) {
		bins.get(((PotentialEvaluator)indiv).getPotential().numSiteSums()-1).add(indiv);
	}
	ArrayList<IIndividual> mseFrontier = new ArrayList<IIndividual>();
	ArrayList<IIndividual> pccFrontier = new ArrayList<IIndividual>();;
	for (ArrayList<IIndividual> arrayList : bins) {
		if(arrayList.size()>0) {
			mseFrontier.addAll(PotentialSubPopulation.getLowerConvexHullFromParetoFrontier(PotentialSubPopulation.getParetoFrontier(arrayList, false), false));
			pccFrontier.addAll(PotentialSubPopulation.getLowerConvexHullFromParetoFrontier(PotentialSubPopulation.getParetoFrontier(arrayList, true), true));
		}
	}

	ArrayList<PotentialSubPopulation> subpopulations = new ArrayList<PotentialSubPopulation>();
	for (PotentialSubPopulation potentialSubPopulation : m_GlobalTrainingFrontierLC) {
		subpopulations.add(potentialSubPopulation);
	}
	for (PotentialSubPopulation potentialSubPopulation : m_GlobalTrainingFrontierMSE) {
		subpopulations.add(potentialSubPopulation);
	}
	subpopulations.add(m_Population);
	
	this.getDataSet().updateDynamicIndices(subpopulations, true);

	//Update global using the energies and forces indicated in globalIndices
	//because this processor might not know which are the latest globalIndices
	//Also, there is no notion of fitness with respect to the global indices during the
	//local optimization steps
	this.getDataSet().setSubset("MiniBatch");
//	this.getDataSet().setSubset("All");//TODO delete
	
	//Try to add these potentially good individuals to the global population	
	int num;
	if(m_Population.getIndividuals().size() < 200) {num = (int)(0.1*m_Population.getIndividuals().size());}
	else {num = 50;}
	ArrayList<IIndividual> goodPCC = this.getGoodIndividualsPareto(num,true);
	ArrayList<IIndividual> goodMSE = this.getGoodIndividualsPareto(num,false);

	if(!PotentialPopulation.lock(m_GlobalDirectory+"/global.lock")){
		Status.basic("Couldn't lock from updateGlobal()!");
		return;
	}
	
	this.refreshLocalCopyOfGlobal("globalTrainingFrontierLC",false);
	//Try to add the individuals in the global population to the global population (itself) using the new global indices
	int nS = 0;
	for (PotentialSubPopulation psp : m_GlobalTrainingFrontierLC) {
		ArrayList<IIndividual> indsLC = psp.getIndividuals();
		psp.setIndividuals(new ArrayList<IIndividual>());
		for (IIndividual iIndividual : indsLC) {
			if(((PotentialEvaluator)iIndividual).getPotential().numSiteSums() == nS + 1) {
				psp.add(iIndividual, null, null, true);
			}
		}
		for (IIndividual iIndividual : pccFrontier) {
			if(((PotentialEvaluator)iIndividual).getPotential().numSiteSums() == nS + 1) {
				psp.add(iIndividual, null, null, true);
			}
		}
		for (IIndividual iIndividual : goodPCC) {
			if(((PotentialEvaluator)iIndividual).getPotential().numSiteSums() == nS + 1) {
				psp.add(iIndividual, null, null, true);
			}
		}
		nS++;
	}
	this.printGlobal("globalTrainingFrontierLC");

	this.refreshLocalCopyOfGlobal("globalTrainingFrontierMSE",false);
	//Try to add the individuals in the global population to the global population (itself) using the new global indices
	nS = 0;
	for (PotentialSubPopulation psp : m_GlobalTrainingFrontierMSE) {
		ArrayList<IIndividual> indsMSE = psp.getIndividuals();
		psp.setIndividuals(new ArrayList<IIndividual>());
		for (IIndividual iIndividual : indsMSE) {
			if(((PotentialEvaluator)iIndividual).getPotential().numSiteSums() == nS + 1) {
				psp.add(iIndividual, null, null, false);
			}
		}
		for (IIndividual iIndividual : mseFrontier) {
			if(((PotentialEvaluator)iIndividual).getPotential().numSiteSums() == nS + 1) {
				psp.add(iIndividual, null, null, false);
			}
		}
		for (IIndividual iIndividual : goodMSE) {
			if(((PotentialEvaluator)iIndividual).getPotential().numSiteSums() == nS + 1) {
				psp.add(iIndividual, null, null, false);
			}
		}
		//Check for convergence
		for (IIndividual individual : psp.getIndividuals()) {
			if(Statistics.compare(individual.getFitness(null, false), PotentialEvaluator.m_Convergence) == -1) {
				Status.basic("End condition met: Solution found! "+individual.getFitness(null, false)+" "+individual.getComplexity()+" "+((PotentialEvaluator)individual).numNodes()+" "+((PotentialEvaluator)individual).getExpression());
				m_ConvergedMSE = true;
			}
		}
		nS++;
	}
	
	this.printGlobal("globalTrainingFrontierMSE");

	
//	Update local dynamic indices
	this.getDataSet().setSubset("Local");
	this.getDataSet().updateDynamicIndices(subpopulations, false);
	
	if(!PotentialPopulation.unlock(m_GlobalDirectory+"/global.lock")){
		Status.basic("Couldn't unlock from updateGlobal()!");
		return;	
	}
}
private void printProcessorIndividuals(){
	String population = "local";
	String path = m_GlobalDirectory+"/"+m_ProcessorNum;
	File file = new File(path+"/"+population);
	File filebackup = new File(path+"/"+population+".backup");
	try{
		//Backup in case something (e.g., SLURM) kills job
		boolean d = file.renameTo(filebackup);
		if(!d){
			Status.basic("Couldn't rename "+population+" to "+population+". Trying again");
		}
		int c0 = 0;
		while(!d){
			c0++;
			d = file.renameTo(filebackup);
			if(d){
				Status.basic("Renamed "+population+" to "+population+".backup. After "+c0+" attempts");
			}
		}
		boolean e = file.createNewFile();
		if(!e){
			Status.basic("Couldn't create "+population+". Trying again");
		}
		int c1 = 0;
		while(!e){
			c1++;
			e = file.createNewFile();
			if(e){
				Status.basic("Created "+population+".  After "+c1+" attempts");
			}
		}
		m_Population.print(true, path, population, 0);
		//Delete the backup after writing to global archives
		boolean db = filebackup.delete();
		if(!db){
			Status.basic("Couldn't delete "+population+".backup. Trying again");
		}
		int c0b = 0;
		while(!db){
			c0b++;
			db = filebackup.delete();
			if(db){
				Status.basic("Deleted "+population+".backup. After "+c0b+" attempts");
			}
		}	
	}catch(IOException e){
		Status.basic("Problem in printProcessorIndividuals()!");
		e.printStackTrace();
		return;
	}
}

private void printGlobal(String population){
	File filepopulation = new File(m_GlobalDirectory+"/"+population);
	File filepopulationbackup = new File(m_GlobalDirectory+"/"+population+".backup");
	File fileiterations = new File(m_GlobalDirectory+"/"+population+"_iterations");
	File fileiterationsbackup = new File(m_GlobalDirectory+"/"+population+"_iterations.backup");

	PotentialPopulation.handleFiles("rename", filepopulation, filepopulationbackup);
	PotentialPopulation.handleFiles("create", filepopulation, null);
	PotentialPopulation.handleFiles("copy", fileiterations, fileiterationsbackup);

	this.printGlobal2(population);
	
	PotentialPopulation.handleFiles("appendtoiterations",filepopulation, fileiterations);
	
	PotentialPopulation.handleFiles("delete", fileiterationsbackup, null);
	PotentialPopulation.handleFiles("delete", filepopulationbackup, null);
}

private static void handleFiles(String option, File file1, File file2) {
	if(option.equals("rename")) {
		if(!file1.renameTo(file2)) {
			Status.basic("Couldn't rename "+file1.getName()+" to "+file2.getName()+"! Trying again");
			int c = 0;
			while(!file1.renameTo(file2)) {
				c++;
			}
			Status.basic("Renamed "+file1.getName()+" to "+file2.getName()+". After "+c+" attempts");
		}
	}
	else if(option.equals("create")) {
		try {
			if(!file1.createNewFile()) {
				Status.basic("Couldn't create "+file1.getName()+"! Trying again");
				int c = 0;
				while(!file1.createNewFile()) {
					c++;
				}
				Status.basic("Created "+file1.getName()+". After "+c+" attempts");
			}
		} catch (IOException e) {
			Status.basic("Couldn't create "+file1.getName()+"!");
			e.printStackTrace();
		}
	}
	else if(option.equals("delete")) {		
		int c = 0;
		if(!file1.delete()) {
			Status.basic("Couldn't delete "+file1.getName()+"! Trying again.");
			while(!file1.delete()) {
				c++;
			}
			Status.basic("Deleted "+file1.getName()+". After "+c+" attempts");
		}
	}
	else if(option.equals("appendtoiterations")){
		try {
	        BufferedReader iterationreader = new BufferedReader(new FileReader(file2.getAbsolutePath()));
	        String line;
	        String lastIterationNum = "";
	        while ((line = iterationreader.readLine()) != null) {
	        	if(line.startsWith("IterationNum")) {
	        		lastIterationNum = line;
	        	}
	        }
	        Integer IterationNum = Integer.parseInt(lastIterationNum.split(" ")[1])+1;
	        PrintWriter outiterations = new PrintWriter(new BufferedWriter(new FileWriter(file2.getAbsolutePath(),true)));
	        iterationreader.close();
	        
	        BufferedReader populationreader = new BufferedReader(new FileReader(file1.getAbsolutePath()));
	        ArrayList<String> lines = new ArrayList<String>();
	        while ((line = populationreader.readLine()) != null) {
	        	lines.add(line);
	        }
	        outiterations.println("IterationNum "+IterationNum+" "+lines.size()+" "+PotentialPopulation.getTime());
	        for (String string : lines) {
	        	outiterations.println(string);
			}
	        outiterations.close();
	        populationreader.close();
		}catch(IOException e) {
	        Status.basic("Couldn't append to "+file1.getName()+" !");
	        e.printStackTrace();
		}
	}
	else if(option.equals("copy")) {
		try {
			Files.copy(file1.toPath(),file2.toPath());
		} catch (IOException e) {
			Status.basic("Couldn't copy "+file1.getName()+" "+file2.getName()+"!");
			e.printStackTrace();
		}
	}
}
private void printGlobal2(String population) {
//	PotentialSubPopulation subpopulation;
//	if(population.equals("globalTrainingFrontierLC")){
//		subpopulation = m_GlobalTrainingFrontierLC;
//	}
//	else if(population.equals("globalTrainingFrontierMSE")){
//		subpopulation = m_GlobalTrainingFrontierMSE;
//	}
//	else{
//		return;
//	}
//	subpopulation.print(printToGlobal, m_GlobalDirectory,population,(int)(((double)m_NumOperations)/m_DynamicFrequency));
	
	PotentialSubPopulation[] psps;
	Boolean lc = null;
	if(population.equals("globalTrainingFrontierLC")){
		psps = m_GlobalTrainingFrontierLC;
		lc = true;
	}
	else if(population.equals("globalTrainingFrontierMSE")){
		psps = m_GlobalTrainingFrontierMSE;
		lc = false;
	}
	else{
		return;
	}
	try{
	    FileWriter fw = new FileWriter(m_GlobalDirectory+"/"+population);
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter out = new PrintWriter(bw);
		
		IIndividual minInd = psps[0].getIndividuals().get(0);
		for (PotentialSubPopulation psp : psps) {
			for (IIndividual indiv : psp.getIndividuals()) {
				if(((PotentialEvaluator)indiv).getFitness(null, lc)<minInd.getFitness(null, lc)) {
					minInd = indiv;
				}
			}
		}
		
		for (PotentialSubPopulation psp : psps) {
			psp.setIndividuals(PotentialSubPopulation.sortIndividualsParetoOrLowerConvexHull(psp.getIndividuals()));
			for (IIndividual individual : psp.getIndividuals()) {
				if(!individual.equals(minInd)) {
					PotentialEvaluator evaluator = (PotentialEvaluator) individual;
					out.println(evaluator.getFitness(null, true)+" "+evaluator.getFitness(null, false)+" "+evaluator.getComplexity()+" "+evaluator.numNodes()+" "+evaluator.getExpression());
				}
			}
		}
		
		PotentialEvaluator evaluator = (PotentialEvaluator)minInd;
		out.println(evaluator.getFitness(null, true)+" "+evaluator.getFitness(null, false)+" "+evaluator.getComplexity()+" "+evaluator.numNodes()+" "+evaluator.getExpression());
		
		out.close();
		return;
	}catch(IOException e){
		Status.basic("Problem printing from subpopulation!");
		e.printStackTrace();
		return;
	}

}


private void refreshLocalCopyOfGlobal(String population,boolean lock) {
	if(lock) {
		if(!PotentialPopulation.lock(m_GlobalDirectory+"/global.lock")){
			Status.basic("Couldn't lock from refreshLocalCopyOfGlobal()!");
			return;
		}
	}
	PotentialEvaluator evalInPop = (PotentialEvaluator) this.getIndividual();
	ArrayList<IIndividual> individuals = new ArrayList<IIndividual>();
	String file = m_GlobalDirectory+"/"+population;

	try{
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while((line = br.readLine())!= null){
			String[] row = line.split(" ");
			PotentialEvaluator eval = evalInPop.createEvaluator(row[4]);
			individuals.add((IIndividual)eval);
		}
		br.close();
	}catch(IOException e){
		Status.basic("Problem in refreshLocalCopyOfGlobal()!");
		e.printStackTrace();
		return;
	}
	
	PotentialSubPopulation[] psps = null;
	Boolean b = null;
	if(population.equals("globalTrainingFrontierLC")){
//		m_GlobalTrainingFrontierLC.setIndividuals(individuals);
		psps = m_GlobalTrainingFrontierLC;
		b = true;
	}
	else if(population.equals("globalTrainingFrontierMSE")){
//		m_GlobalTrainingFrontierMSE.setIndividuals(individuals);
		psps = m_GlobalTrainingFrontierMSE;
		b = false;
	}
	int numSumm = 1;
	for (PotentialSubPopulation psp : psps) {
		psp.setIndividuals(new ArrayList<IIndividual>());
		for (IIndividual iIndividual : individuals) {
			PotentialEvaluator pe = (PotentialEvaluator)iIndividual;
			int nS = pe.getPotential().numSiteSums();
			if(nS == numSumm) {
//				//TODO start delete
//				if(population.equals("globalTrainingFrontierMSE")) {
//					this.getDataSet().setSubset("All");
//					System.out.println(pe.getFitness(null, false)+" "+pe.getComplexity()+" "+pe.getExpression());
//				}
//				//TODO end delete

				if(psp.getIndividuals().size()<1) {
					psp.getIndividuals().add(pe);
				}
				else {
					psp.add(pe, null, 0, b);
				}
			}
		}
		numSumm++;
	}
//	//TODO start delete
//	if(population.equals("globalTrainingFrontierMSE")) {
//		System.exit(0);
//	}
//	//TODO end delete
	if(lock) {
		if(!PotentialPopulation.unlock(m_GlobalDirectory+"/global.lock")){
			Status.basic("Couldn't unlock from refreshLocalCopyOfGlobal()!");
			return;
		}
	}
}
private void refreshLocalCopyOfProcessors() {
	ArrayList<IIndividual> individuals = new ArrayList<IIndividual>();
	PotentialEvaluator evalInPop = (PotentialEvaluator) this.getIndividual();
	
	int c = 1;
	while(c <= m_NumProcessors ) {
//		if(c == m_ProcessorNum) {c++;}
//		if(!( c <= m_NumProcessors)) {break;}
		
		String path = m_GlobalDirectory+"/"+c;

		if(!PotentialPopulation.lock(path+"/local.lock")){
			Status.basic("Couldn't lock from refreshLocalCopyOfProcessors()!");
			return;
		}
		String file =path+"/local";
		
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while((line = br.readLine())!= null){
				String[] row = line.split(" ");
				PotentialEvaluator eval = evalInPop.createEvaluator(row[4]);
				//Don't set the fitness because every processor can use a different training sub-set
				//eval.setFitness(Double.parseDouble(row[0]), Double.parseDouble(row[1])); 
				eval.setComplexity((int)Double.parseDouble(row[2]));
				eval.setNumNodes((int)Double.parseDouble(row[3]));
				//Add every individual from this processor to the list of individuals in the processors
				individuals.add((IIndividual)eval);
			}
			br.close();
		}catch(IOException e){
			Status.basic("Problem in refreshLocalCopyOfProcessors()!");
			e.printStackTrace();
			return;
		}
		if(!PotentialPopulation.unlock(path+"/local.lock")){
			Status.basic("Couldn't unlock from refreshLocalCopyOfProcessors()!");
			return;
		}
		c++;
	}
	m_ProcessorsIndividuals = individuals;
}

private boolean addToPopulation(IIndividual prospective){
	
	return m_Population.add(prospective, null, null, false).converged();
//	//start adding at population 0
//	else if(m_PopType.equals("ParetoFrontiers")){
//		boolean st1 = this.add(this.addToPopulation(prospective, 0, true), true);
//		boolean st2 = this.add(this.addToPopulation(prospective, 0, false), false);
//		return st2 || st1;
//	}
//	else if(m_PopType.equals("BinsHFC")){return this.addHFC(prospective);}
//	else{return false;}	
	
}

	
//public int numPopulations() {
//	return m_NumPops;
//}

private static IIndividual getIndividualTournament(int size, boolean lc, ArrayList<IIndividual> individuals){
//	ArrayList<PotentialSubPopulation> subPopulations;
//	if(lc) {subPopulations = m_SubPopulationsLC;}
//	else {subPopulations = m_SubPopulationsMSE;}
//	subPopulations = m_SubPopulationsMSE;
//	if(m_PopType.equals("List")) {subPopulations = m_SubPopulationsMSE;}
//	int numPops = subPopulations.size();
	
	int numIndividuals = individuals.size();
	if(numIndividuals < 1) {
		return null;
	}
	IIndividual bestIndividual = null;
	double bestFitness = Double.POSITIVE_INFINITY;
	int count = 0;
	
	while(count < size){
		IIndividual individual = individuals.get(GENERATOR.nextInt(numIndividuals));
		if(Statistics.compare(individual.getFitness(null, lc), bestFitness) < 1){
			bestIndividual = individual;
			bestFitness = bestIndividual.getFitness(null, lc);
		}
		count++;
	}
	return bestIndividual;		
}

@Override
public void print(double numOperations){
	Status.basic("NumOperations "+numOperations +" NumIsValidCalls "+PotentialPopulation.m_NumIsValidCalls);
	Status.basic("AverageSize "+m_AverageSize);
	Status.basic("AverageDepth "+m_AverageDepth);
	Status.basic("AverageComplexity "+m_AverageComplexity);		
	int num = (int)(((double)numOperations)/m_DynamicFrequency);
//	m_GlobalTrainingFrontierLC.print(false, null, "globalTrainingFrontierLC", num);
//	m_GlobalTrainingFrontierMSE.print(false, null, "globalTrainingFrontierMSE", num);
	
	int c = 0;
	while(c < 2) {
		boolean lc = c < 1;
		PotentialSubPopulation[] psps = lc ? m_GlobalTrainingFrontierLC : m_GlobalTrainingFrontierMSE;
		String population = "globalTrainingFrontier";
		population += lc ? "LC" : "MSE";
		
		int numindividuals = 0;
		IIndividual minInd = psps[0].getIndividuals().get(0);
		for (PotentialSubPopulation psp : psps) {
			numindividuals += psp.getIndividuals().size();
			for (IIndividual indiv : psp.getIndividuals()) {
				if(((PotentialEvaluator)indiv).getFitness(null, lc)<minInd.getFitness(null, lc)) {
					minInd = indiv;
				}
			}
		}
		Status.basic(population+" "+(num)+" "+numindividuals);
		for (PotentialSubPopulation psp : psps) {
			psp.setIndividuals(PotentialSubPopulation.sortIndividualsParetoOrLowerConvexHull(psp.getIndividuals()));
			for (IIndividual individual : psp.getIndividuals()) {
				if(!individual.equals(minInd)) {
					PotentialEvaluator evaluator = (PotentialEvaluator) individual;
					Status.basic(evaluator.getFitness(null, true)+" "+evaluator.getFitness(null, false)+" "+evaluator.getComplexity()+" "+evaluator.numNodes()+" "+evaluator.getExpression());
				}
			}
		}
		PotentialEvaluator evaluator = (PotentialEvaluator)minInd;
		Status.basic(evaluator.getFitness(null, true)+" "+evaluator.getFitness(null, false)+" "+evaluator.getComplexity()+" "+evaluator.numNodes()+" "+evaluator.getExpression());
		c++;
	}
}
	
//	private void printParetoFrontiers(int numPopulations, double numOperations,boolean fitnessByLC){
//		
////		PotentialEvaluator fittestValidation = (PotentialEvaluator)m_ValidationFrontierLC.getFittest("Validation",fitnessByLC);
////		if(fittestValidation != null){
////			Status.basic("FittestValidation "+" FitnessByLC "+fitnessByLC+" "+(fittestValidation.getFitness(false, null,fitnessByLC))+" "+(fittestValidation.getFitness(true, null,fitnessByLC))+" "+fittestValidation.getComplexity()+" "+fittestValidation.numNodes()+" "+fittestValidation.getExpression());
////		}
////		PotentialEvaluator fittestTraining = this.getFittest(fitnessByLC);
////		if(fittestTraining != null){
////			Status.basic("FittestTraining "+"FitnessByLC "+fitnessByLC+" "+(fittestTraining.getFitness(false, null,fitnessByLC))+" "+(fittestTraining.getFitness(true, null,fitnessByLC))+" "+fittestTraining.getComplexity()+" "+fittestTraining.numNodes()+" "+fittestTraining.getExpression());
////		}
//
//		int count = 0;
//		if(m_OccamRazor){numPopulations = this.numPopulations();}
//		ArrayList<PotentialSubPopulation> subPopulations;
//		if(fitnessByLC){
//			subPopulations = m_SubPopulationsLC;
//		}
//		else{
//			subPopulations = m_SubPopulationsMSE;
//		}
//		for (PotentialSubPopulation pop : subPopulations) {
//			if(count<numPopulations){
//				pop.print(fitnessByLC);
//			}
//			count++;
//		}
////		Status.basic("NumOperations "+numOperations);
////		Status.basic("TotalNumIndividuals "+(totalInds-m_NumPops));
//	}
	
//	private void printBinsHFC(int numPopulations, double numOperations){
//		Status.basic("ValidationFrontier");
//		m_ValidationPopulation.print();
//
//		Status.basic("NumOperations "+numOperations);
//		PotentialEvaluator fittest = this.getFittest();
//		
//		if(fittest != null){
//			Status.basic("FittestTraining "+(fittest.getFitness(true, null))+", "+(fittest.getFitness(false, null))+", "+fittest.getComplexity()+", "+fittest.getPotential().getExpression());
//		}
//		
//		int totalInds = 0;
//		int count = 0;
//		for (PotentialSubPopulation pop : m_SubPopulationsLC) {
//			int size = pop.getIndividuals().size();
//			totalInds+= size;
//			if(size > 0 && count < numPopulations){
//				pop.print();
//				count++;
//			}
//		}
//		Status.basic("NumOperations "+numOperations);
//		Status.basic("TotalNumIdividuals: "+totalInds);
//	}
	
//	private PotentialEvaluator getFittest(boolean fitnessByLC){
//		PotentialEvaluator fittest = null;
//		double bestFitness = Double.MAX_VALUE;
////		analyze 3 populations
//		int analyze = 1;
//		if(m_OccamRazor){analyze = numPopulations();}
//		int count = 0;
//		for (PotentialSubPopulation pop : m_SubPopulationsLC) {
//			if(count>=analyze){return fittest;}
//			PotentialEvaluator fittestInPop = (PotentialEvaluator)pop.getFittest("Training",fitnessByLC);
//			double fitnessInPop = Double.MAX_VALUE;
//			if(fittestInPop != null){
//				fitnessInPop = fittestInPop.getFitness(true, null,fitnessByLC);
//				count++;
//			}
//			if(fitnessInPop < bestFitness){
//				fittest = fittestInPop;
//				bestFitness = fitnessInPop;
//			}
//		}
//		return fittest;
//	}

//	@Override
//	public boolean converged() {
////		analyze 3 populations
//		int analyze = 1;
//		if(m_OccamRazor){analyze = this.numPopulations();}
//		int count = 0;
//		for (PotentialSubPopulation pop : m_Populations) {
//			if(count>=analyze){return false;}
//			int numIndividuals = pop.getIndividuals().size();
//			if(numIndividuals > 0){
//				if(pop.converged()){return true;}
//				count++;
//			}
//		}
//		return false;
//		
//	}
	
//protected boolean addHFC(IIndividual prospective){
//	for (PotentialSubPopulation pop : m_SubPopulationsLC) {
//		InfoAddIndividual info = pop.add(prospective,null, m_NumPops, false);
//		boolean added = info.added();
//		boolean converged = info.converged();
//		if(added || converged){
//			return converged;
//		}
//	}
//	return false;
//}
	
//bias is the estimated number of nodes of solution
//	Source: Selection Based on the Pareto Nondomination
//			Criterion for Controlling Code Growth in
//			Genetic Programming
//	Source: A niched Pareto genetic algorithm for
//			multiobjective optimization
private static IIndividual getIndividualTournamentPareto(int size, boolean lc, PotentialSubPopulation subPopulation){

	IIndividual prospective = null;
	//Return individual only if it is not dominated by others
	do{
		prospective = subPopulation.getIndividual("Equal");
		double prospFitness = prospective.getFitness(null, lc);
		int prospComplx = prospective.getComplexity();
		//Compare the prospective with other individuals using Pareto non-dominance criterion
		int c = 0;
		while(c < size){
			IIndividual individual = subPopulation.getIndividual("Equal");
			double fitness = individual.getFitness(null, lc);
			int complexity = individual.getComplexity();
			boolean st1 = fitness < prospFitness && complexity <= prospComplx;
			boolean st2 = fitness <= prospFitness && complexity < prospComplx;
			boolean indivDominatesProsp = st1 || st2;
			//If individual dominates prospective, then start over
			if(indivDominatesProsp){
				prospective = null;
				break;
			}
			c++;
		}
		
	}while(prospective == null);
	return prospective;	
}

//	private int numIndividuals(){
//		if(m_PopType.equals("ParetoFrontiers")){
//			int totalInds = 0;
//			for (PotentialSubPopulation pop : m_SubPopulationsLC) {
//				int size = pop.getIndividuals().size();
//				totalInds += size;
//			}
//			for (PotentialSubPopulation pop : m_SubPopulationsMSE) {
//				int size = pop.getIndividuals().size();
//				totalInds += size;
//			}
//			return totalInds-2*this.numPopulations();
//		}
//		else if(m_PopType.equals("BinsHFC")){
//			int totalInds = 0;
//			for (PotentialSubPopulation pop : m_SubPopulationsLC) {
//				int size = pop.getIndividuals().size();
//				totalInds += size;
//			}
//			return totalInds;
//		}
//		else{return 0;}
//	}
	
//	private synchronized void limit(double maxNumIndivs){
//		double numInds = this.numIndividuals();
//		if(numInds > maxNumIndivs){
//			double maxPerPop = maxNumIndivs/m_NumPops;
//			for (PotentialSubPopulation pop : m_SubPopulationsLC) {
//				ArrayList<IIndividual> individuals = pop.getIndividuals();
//				int size = individuals.size();
//				if( size > maxPerPop){
//					ArrayList<IIndividual> sorted = this.sortIndividualsFitness(individuals);
//					int removeIndex = size - 1;
//					while(removeIndex > maxPerPop - 1){
//						sorted.remove(removeIndex);
//						removeIndex--;
//					}
//					pop.setIndividuals(sorted);
//				}
//				
//			}
//		}
//	}
	
//	private ArrayList<IIndividual> sortIndividualsFitness(ArrayList<IIndividual> indivs){
//		ArrayList<IIndividual> individuals = new ArrayList<>(indivs);
//		ArrayList<IIndividual> sorted = new ArrayList<IIndividual>();
//		while(individuals.size() > 0){
//			int index = this.getIndexFittest(individuals);
//			sorted.add(individuals.get(index));
//			individuals.remove(index);
//		}
//		return sorted;
//	}
	
//	private int getIndexFittest(ArrayList<IIndividual> individuals){
//	int index = 0;
//	int count = 0;
//	IIndividual fittest =  individuals.get(0);
//	double bestFitness = fittest.getFitness(true, null);
//	
//	for(IIndividual indiv : individuals){
//	double fitness = indiv.getFitness(true, null);
//		if(fitness < bestFitness){
//			index = count;
//			bestFitness = fitness;
//		}
//		count++;
//	}
//	return index;
//	}
public static boolean lock(String string){
	File file = new File(string);
	try{
		boolean createdLock = file.mkdir();
		if(!createdLock){
			Status.basic("Couldn't create "+string+". Trying again");
		}
		int c = 0;
		while(!createdLock){
			TimeUnit.MILLISECONDS.sleep(1);
			c++;
			createdLock = file.mkdir();
			if(createdLock){
				Status.basic("Created global.lock. After "+c+" attempts");
			}
		}
	}
	catch(InterruptedException e){
		Status.basic("Problem locking!");
		e.printStackTrace();
		return false;
	}

	return true;
}

public static boolean unlock(String string){
	File file = new File(string);
	if(file.exists()){
		if(!file.mkdir()){
			boolean b = file.delete();
			if(!b){
				Status.basic("Couldn't delete global.lock. Trying again");
			}
			int c = 0;
			while(!b){
				c++;
				b = file.delete();
				if(b){
					Status.basic("Deleted global.lock. After "+c+" attempts");
				}
			}
			return true;
		}
		else{
			Status.basic("Couldn't unlock! Because couldn't mkdir() global.lock");
			return false;
		}
	}
	else{
		Status.basic("Couldn't unlock! Because file global.lock doesn't exist");
		return false;
	}
}
	
public boolean createInitialPopulation(int numValidPerMethodPerDepth){	
	PotentialEvaluator evaluator = (PotentialEvaluator)this.getIndividual();
	return this.createIndividuals(numValidPerMethodPerDepth, PotentialEvaluator.m_MinDepthRandomTree, PotentialEvaluator.m_MaxDepthRandomTree,evaluator);
}
	
private boolean createIndividuals(int numValidPerMethodPerDepth, int minDepth, int maxDepth, PotentialEvaluator evaluator) {
//		  For each depth, numValidPerMethodPerDepth is the number of valid trees that we create (full and not full) 
//		  and try to add to the populations.
//		  Add trees of depths from depth to maxDepth
	  int depth = minDepth;

	  while(depth <= maxDepth){
		  
//			  This counter corresponds to the attempts to add valid initial individuals to the populations
		  int numValidCreated = 1;
		  while(numValidCreated <= 2*numValidPerMethodPerDepth){
			  	boolean isValidFullDepth = false;
			  	while(!isValidFullDepth){
//						  Create initial individuals
			  		  char c;
			  		  if(GENERATOR.nextBoolean()) {
			  			  c = 'g';
			  		  }
			  		  else {
			  			  c = 'f';
			  		  }
				      TreePotential fullTree = TreePotential.generateTree(evaluator.getPotential().getCutoffDistance(),depth, c);
				      PotentialEvaluator tempIndividual = new PotentialEvaluator(fullTree, this.getDataSet());
				      if(tempIndividual != null){
				    	  isValidFullDepth = tempIndividual.isValid();
					      if(isValidFullDepth){
					    	  this.addToPopulation(tempIndividual);
					      }
				      }
			  	}
			  	
//			  	boolean isValidDepth = false;
//			  	while(!isValidDepth){
////						  Create initial individuals
//				      TreePotential growTree = TreePotential.generateTree(evaluator.getPotential().getCutoffDistance(),depth,'g');
//				      PotentialEvaluator tempIndividual = new PotentialEvaluator(growTree, this.getDataSet());
//				      if(tempIndividual != null){
//				    	  isValidDepth = tempIndividual.isValid();
//					      if(isValidDepth){
//					    	  this.addToPopulation(tempIndividual);
//					      }
//				      }
//			  	}
			  	numValidCreated++;
		  }
		  depth++;
	  }
	  return depth >= maxDepth;
}

private DataSet getDataSet() {
	return m_DataSet;
}

//TODO how is the numToKeep determined?
private void keepGoodIndividuals(int numToKeep) {
	ArrayList<IIndividual> goodIndividuals = new ArrayList<>();
	
	//Select good individuals
	ArrayList<IIndividual> indsPCC = PotentialSubPopulation.getLowerConvexHullFromParetoFrontier(PotentialSubPopulation.getParetoFrontier(m_Population.getIndividuals(), true), true);
	//Add PCC individuals to goodIndividuals
	for (IIndividual iIndividual : indsPCC) {goodIndividuals.add(iIndividual);}
	ArrayList<IIndividual> indsMSE = PotentialSubPopulation.getLowerConvexHullFromParetoFrontier(PotentialSubPopulation.getParetoFrontier(m_Population.getIndividuals(), false), false);
	//Add MSE individuals to goodIndividuals only there is no other individual with the same expression
	for (IIndividual iIndividual : indsMSE) {
		String expression = ((PotentialEvaluator)iIndividual).getPotential().getExpression(true);
		boolean unique = true;
		for (IIndividual goodInd : goodIndividuals) {
			if(((PotentialEvaluator)goodInd).getPotential().getExpression(true).equals(expression)) {unique = false;break;}
		}
		if(unique) {goodIndividuals.add(iIndividual);}
	}
	
	ArrayList<IIndividual> goodPCC = this.getGoodIndividualsPareto(numToKeep/2,true);
	//Add goodPCC individuals to goodIndividuals only there is no other individual with the same expression
	for (IIndividual iIndividual : goodPCC) {
		String expression = ((PotentialEvaluator)iIndividual).getPotential().getExpression(true);
		boolean unique = true;
		for (IIndividual goodInd : goodIndividuals) {
			if(((PotentialEvaluator)goodInd).getPotential().getExpression(true).equals(expression)) {unique = false;break;}
		}
		if(unique) {goodIndividuals.add(iIndividual);}
	}
	ArrayList<IIndividual> goodMSE = this.getGoodIndividualsPareto(numToKeep/2,false);
	//Add goodMSE individuals to goodIndividuals only there is no other individual with the same expression
	for (IIndividual iIndividual : goodMSE) {
		String expression = ((PotentialEvaluator)iIndividual).getPotential().getExpression(true);
		boolean unique = true;
		for (IIndividual goodInd : goodIndividuals) {
			if(((PotentialEvaluator)goodInd).getPotential().getExpression(true).equals(expression)) {unique = false;break;}
		}
		if(unique) {goodIndividuals.add(iIndividual);}
	}
	//replace all individuals with the good individuals
	m_Population.setIndividuals(PotentialSubPopulation.sortIndividualsParetoOrLowerConvexHull(goodIndividuals));
}

private ArrayList<IIndividual> getGoodIndividualsPareto(int num, boolean lc){
	ArrayList<IIndividual> goodIndividuals = new ArrayList<IIndividual>();
	int c = 0;
	while(c<num) {
		IIndividual individual;
		boolean containsSameExpression;
		int attempts = 0;
		int maxAttempts = 10*num;
		do {
			containsSameExpression = false;
			//TODO size should be fraction of the number of individuals?
			individual = PotentialPopulation.getIndividualTournamentPareto(m_Tournament_Size, lc,m_Population);
			String express1 = ((PotentialEvaluator)individual).getPotential().getExpression(true);
			for (IIndividual indiv : goodIndividuals) {
				String express2 = ((PotentialEvaluator)indiv).getPotential().getExpression(true);
				if(express1.equals(express2)) {containsSameExpression = true;break;}
			}
			attempts ++;
		}while(containsSameExpression && attempts < maxAttempts);
		if(!(attempts < maxAttempts)) {
			Status.basic("Check attempts in getGoodIndividualsPareto(int, boolean) in PotentialPopulation!");
		}
		goodIndividuals.add(individual);
		c++;
	}
	return goodIndividuals;
}

//private void deleteAllIndividuals() {
//	PotentialEvaluator evaluator = PotentialEvaluator.generateRandomEvaluator(true, Main.m_CutoffDistance);
////	for (PotentialSubPopulation sp : m_SubPopulationsLC) {
////		ArrayList<PotentialEvaluator> newIndividuals = new ArrayList<PotentialEvaluator>();
////		newIndividuals.add(evaluator);
////		sp.setIndividuals(newIndividuals);
////	}
//
////	for (PotentialSubPopulation sp : m_SubPopulationsMSE) {
////		ArrayList<PotentialEvaluator> newIndividuals = new ArrayList<PotentialEvaluator>();
////		newIndividuals.add(evaluator);
////		sp.setIndividuals(newIndividuals);
////	}
//	ArrayList<PotentialEvaluator> newIndividuals = new ArrayList<PotentialEvaluator>();
//	newIndividuals.add(evaluator);
//	m_Population.setIndividuals(newIndividuals);
//}

//	private void addAll(ArrayList<PotentialEvaluator> individualsList) {
//		PotentialEvaluator[] individuals = new PotentialEvaluator[individualsList.size()];
//		  int c = 0;
//		  for (PotentialEvaluator individual : individualsList) {
//			  individuals[c] = individual;
//			  c++;
//		  }
//		  
//		  int threshold = 1+individuals.length/m_NumThreads;
//		  
//		  Status.basic("Computing fitness of "+individuals.length+" individuals in "+m_NumThreads
//				  +" threads with threshold "+threshold);
//		  
//		  AddAll addAll = new AddAll(this, individuals, 0, individuals.length, threshold);
//		  
//		  ForkJoinPool forkJoinPool = new ForkJoinPool(m_NumThreads);
//		  forkJoinPool.invoke(addAll);
//	}

//	private PotentialEvaluator[] createIndividuals(int numIndividuals, int sizeOrDepth, char type, PotentialEvaluator evaluator) {
//		PotentialEvaluator[] individuals = new PotentialEvaluator[numIndividuals];
//		  
//		int threshold = 1 + individuals.length/m_NumThreads;
//		  
//		Status.basic("Creating "+individuals.length+" individuals in "+m_NumThreads
//				  +" threads with threshold "+threshold);
//		  
//		CreateIndividuals createIndividuals = new CreateIndividuals(this ,individuals, 0, individuals.length, threshold, evaluator, sizeOrDepth, type);
//		  
//		ForkJoinPool forkJoinPool = new ForkJoinPool(m_NumThreads);
//		forkJoinPool.invoke(createIndividuals);
//		  
//		return individuals;
//	}
	
@Override
public IIndividual getIndividual() {

	IIndividual returnIndiv;
	double random = GENERATOR.nextDouble();
	
	//TODO use fraction of size instead of tournament size?
	int tournamentSize = m_Tournament_Size;
	boolean lc = GENERATOR.nextBoolean();
	if(random < 1/3.0){
//		returnIndiv = m_ProcessorsIndividuals.get(GENERATOR.nextInt(size));
		returnIndiv = PotentialPopulation.getIndividualTournament(tournamentSize, lc, m_ProcessorsIndividuals);
	}
	else if(random < 2/3.0){
		returnIndiv = this.getIndividualGlobal();
	}
	else{

		if(m_SelectionMethod.equals("Tournament")){
			returnIndiv = PotentialPopulation.getIndividualTournament(tournamentSize, lc, m_Population.getIndividuals());
		}
		else if(m_SelectionMethod.equals("Equal")){
//			if(lc){
//				returnIndiv = m_SubPopulationsLC.get(GENERATOR.nextInt(m_SubPopulationsLC.size())).getIndividual("Equal");
//			}
//			else{
//				returnIndiv = m_SubPopulationsMSE.get(GENERATOR.nextInt(m_SubPopulationsMSE.size())).getIndividual("Equal");
//			}
			returnIndiv = m_Population.getIndividual("Equal");
		}
		else if(m_SelectionMethod.equals("TournamentPareto")){
			returnIndiv = PotentialPopulation.getIndividualTournamentPareto(tournamentSize, lc, m_Population);
		}
		else {
			returnIndiv = null;
			Status.basic("Problem getting individual!");
		}
//		else{
//			if(lc){
//				returnIndiv = m_SubPopulationsLC.get(GENERATOR.nextInt(m_SubPopulationsLC.size())).getIndividual(m_SelectionMethod);
//			}
//			else{
//				returnIndiv = m_SubPopulationsMSE.get(GENERATOR.nextInt(m_SubPopulationsMSE.size())).getIndividual(m_SelectionMethod);
//			}
//		}
	}

	if(returnIndiv == null) {
		Status.basic("Problem getting individual using random value "+random+"!");
		return null;}
//	returnIndiv.increaseNumTimesSelected();
	return returnIndiv;
}

private IIndividual getIndividualGlobal() {
//	if(GENERATOR.nextBoolean()){
//		return m_GlobalTrainingFrontierLC.getIndividual("Equal");
//	}
//	else{
//		return m_GlobalTrainingFrontierMSE.getIndividual("Equal");
//	}
	if(GENERATOR.nextBoolean()){
		int random;
		do {
			random = GENERATOR.nextInt(m_GlobalTrainingFrontierLC.length);
		}while(random < 1);
		return m_GlobalTrainingFrontierLC[random].getIndividual("Equal");
	}
	else{
		int random;
		do {
			random = GENERATOR.nextInt(m_GlobalTrainingFrontierMSE.length);
		}while(random < 1);
		return m_GlobalTrainingFrontierMSE[random].getIndividual("Equal");
	}
}	

public static double msedistanceToConvexHull(IIndividual individual) {
	return PotentialSubPopulation.distanceToConvexHull(individual, false, m_Population.getIndividuals(), true);
}

/**
 * 
 * @return the time in hours
 */
private static double getTime() {
	double time = 0.0;
	try {
		ProcessBuilder builder = new ProcessBuilder("sqme");
		builder.redirectErrorStream(true);
		Process p = builder.start();
	    BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
	    String line;
	    r.readLine();//The date and time
	    StringTokenizer header = new StringTokenizer(r.readLine());
	    int column = 0;
	    while(!header.nextToken().equals("TIME")) {
	    	column++;
	    }
	    while ((line = r.readLine())!=null) {
	    	String globalRunNum = PotentialPopulation.m_GlobalDirectory.split("/")[PotentialPopulation.m_GlobalDirectory.split("/").length-1];
	    	if(line.contains(globalRunNum+"/")) {
		    	StringTokenizer tokenizer = new StringTokenizer(line);
		    	int c = 0;
		    	while(c < column) {
		    		tokenizer.nextToken();
		    		c++;
		    	}
		        int[] times = PotentialPopulation.getHoursMinutesSeconds(tokenizer.nextToken());
		        time += times[0];
		        time += times[1]/60.0;
		        time += times[2]/3600.0;
	    	}

	    }
	} catch (IOException e) {
		Status.basic("Warning: could not read time! The time that the program had been running at each specific iteration will not be shown.");
		return Double.NaN;
	}
	return time;
}

private static int[] getHoursMinutesSeconds(String hoursminutessecconds) {
	StringTokenizer tokenizer = new StringTokenizer(hoursminutessecconds, ":");
	ArrayList<Integer> hms = new ArrayList<Integer>();
	while(tokenizer.hasMoreTokens()) {
		String token = tokenizer.nextToken();
		if(token.contains("-")) {
			StringTokenizer t2 = new StringTokenizer(token, "-");
			//Days plus hours
			String days = t2.nextToken();
			String hoursString = t2.nextToken();
			try {
				int hours = 24*Integer.parseInt(days) + Integer.parseInt(hoursString);
				hms.add(hours);
			}catch(IllegalArgumentException e) {
				hms.add(0);
				Status.basic("Tried to parse as int "+days+" and "+hoursString+" in "+m_NumIsValidCalls+" m_NumIsValidCalls");
				e.printStackTrace();
			}
		}
		else {
			try {
				hms.add(Integer.parseInt(token));
			}catch(IllegalArgumentException e) {
				hms.add(0);
				Status.basic("Tried to parse as int "+token+" in "+m_NumIsValidCalls+" m_NumIsValidCalls");
				e.printStackTrace();
			}
		}
	}
	if(hms.size()>2) {
		return new int[] {hms.get(0),hms.get(1),hms.get(2)};
	}
	if(hms.size()>1) {
		return new int[] {0,hms.get(0),hms.get(1)};
	}
	return new int[] {0,0,hms.get(0)};
}

//		protected void computeDirectly(){
////			System.out.println("Computing "+Thread.currentThread().getName());
//			for (int i = 0; i < m_Individuals.length; i++) {
////				m_Individuals[i].getFitness(true, null);
////				m_Individuals[i].getFitness(false, null);
//				m_Population.add(m_Individuals[i],false);
//			}
////			System.out.println("Done with "+Thread.currentThread().getName());
//
//		}
//		
//	    @Override
//	    protected void compute() {
//	        if (m_Length <= m_Threshold) {
//	            computeDirectly();
//	            return;
//	        }
//	        
//	        int split = m_Length / 2;
//	        
//	        invokeAll(new AddAll(m_Population, m_Individuals, m_Start, split, m_Threshold),
//	                new AddAll(m_Population, m_Individuals, m_Start + split, m_Length - split, m_Threshold));
//	    }
//	}
	
//	public class CreateIndividuals extends RecursiveAction{
//		
//		private static final long serialVersionUID = 2723503095872621866L;
//		private IIndividual[] m_Individuals;
//		private int m_Start;
//		private int m_Length;
//		private int m_Threshold;
//		private PotentialEvaluator m_Evaluator;
//		private int m_SizeOrDepth;
//		private char m_Type;		
//		private PotentialPopulation m_Population;
//		
//		public CreateIndividuals(PotentialPopulation population, IIndividual[] individuals, int start, int length, int threshold, PotentialEvaluator evaluator, int sizeOrDepth, char type){
//			m_Individuals = individuals;
//			m_Start = start;
//			m_Length = length;
//			m_Threshold = threshold;
//			m_Evaluator = evaluator;
//			m_SizeOrDepth = sizeOrDepth;//Size or depth of the tree
//			m_Type = type; //'f' for Full, 'g' for Grow, else for Size
//			m_Population = population;
//		}
		
//		protected void computeDirectly(){
////			System.out.println("Computing "+Thread.currentThread().getName());
//			for (int index = m_Start; index < m_Start + m_Length; index++) {
//				  boolean isValidFullDepth = false;
////			  	Try to add a tree created with generateRandomFullTreeDepth(double, int)
//				  while(!isValidFullDepth){
////				  Create initial individuals
//				  
//				  TreePotential tree;
//				  
//				  if(m_Type == 'f'){
//					  tree = TreePotential.generateTree(m_Evaluator.getPotential().getCutoffDistance(),m_SizeOrDepth,'f');
//				  }
//				  else if(m_Type == 'g'){
//					  tree = TreePotential.generateTree(m_Evaluator.getPotential().getCutoffDistance(),m_SizeOrDepth, 'g');
//				  }
//				  else{
//					  tree = null;
//				  }
//				  
//				  PotentialEvaluator tempIndividual = m_Evaluator.createEvaluator(tree);
//				  if(tempIndividual != null){
//					  isValidFullDepth = tempIndividual.isValid();
//				      if(isValidFullDepth){
//				    	  m_Population.add(tempIndividual,false);
//				      }
//				  }
//			  	}
//			}
////			System.out.println("Done with "+Thread.currentThread().getName());
//
//		}
		
//	    @Override
//	    protected void compute() {
//	        if (m_Length <= m_Threshold) {
//	            computeDirectly();
//	            return;
//	        }
//	        
//	        int split = m_Length / 2;
//	        
//	        invokeAll(new CreateIndividuals(m_Population, m_Individuals, m_Start, split, m_Threshold, m_Evaluator, m_SizeOrDepth, m_Type),
//	                new CreateIndividuals(m_Population, m_Individuals, m_Start + split, m_Length - split, m_Threshold, m_Evaluator, m_SizeOrDepth, m_Type));
//	    }
//	}
}