package evaluator;
/*
 * Created on Aug 9, 2016
*
*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.apache.commons.math4.analysis.MultivariateFunction;
import org.apache.commons.math4.analysis.MultivariateVectorFunction;
import org.apache.commons.math4.exception.MaxCountExceededException;
import org.apache.commons.math4.fitting.PolynomialCurveFitter;
import org.apache.commons.math4.fitting.WeightedObservedPoints;
import org.apache.commons.math4.linear.Array2DRowRealMatrix;
import org.apache.commons.math4.linear.LUDecomposition;
import org.apache.commons.math4.linear.RealMatrix;
import org.apache.commons.math4.optim.ConvergenceChecker;
import org.apache.commons.math4.optim.InitialGuess;
import org.apache.commons.math4.optim.MaxEval;
import org.apache.commons.math4.optim.MaxIter;
import org.apache.commons.math4.optim.OptimizationData;
import org.apache.commons.math4.optim.PointValuePair;
import org.apache.commons.math4.optim.SimplePointChecker;
import org.apache.commons.math4.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math4.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math4.optim.nonlinear.scalar.ObjectiveFunctionGradient;
import org.apache.commons.math4.optim.nonlinear.scalar.gradient.NonLinearConjugateGradientOptimizer;

import evaluator.StructureNet;
import matsci.engine.IContinuousFunctionState;
import matsci.engine.minimizer.CGMinimizer;
import matsci.engine.minimizer.GRLinearMinimizer;
import matsci.io.app.log.Status;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.structure.Structure;
import potentialModelsGP.tree.ConstantNode;
import potentialModelsGP.tree.Node;
import potentialModelsGP.tree.TreePotential;
import util.Statistics;

public class AppliedTreePotential implements IContinuousFunctionState, MultivariateFunction{
	
	private static Random GENERATOR = new Random();
	private TreePotential m_Potential;
	private StructureNet m_StructureNet;
	private Double m_TotalEnergy;
	private static final double m_h = 1E-8;
	public CGMinimizer m_Minimizer;
	private Double m_LattParam;
	private Double m_C11;
	private Double m_C12;
	private Double m_C44;
	private boolean m_relax_Shape;
	
public AppliedTreePotential(TreePotential potential, StructureNet structureNet) {
	m_Potential = potential;
	m_StructureNet = structureNet;
    m_TotalEnergy = null;
  }

//  Get the value predicted by the corresponding tree when it is applied to this specific structure
public double getEnergy() throws NotFiniteValueException {
	if(m_TotalEnergy != null){return m_TotalEnergy;}
    Node topNode = m_Potential.getTopNode();
    double returnValue = 0;
    for (int siteNum = 0; siteNum < m_StructureNet.numDefiningSites(); siteNum++) {
//    	Get the bond with itself
    	StructureNet.Bond bond = m_StructureNet.getBondsByDefiningSiteIncludingBondWithItself()[siteNum][0];
    	returnValue += topNode.getValue(bond, null, null, null);
    }

//  clearKnownValues() whenever we're done with a structure in the training set
    topNode.clearKnownValues();
    m_TotalEnergy = returnValue;
    return returnValue;
}
  
/**
 * 
 * @param node
 * @return Gradients of the  
 * total energy per atom with respect to variation in the value of nodes
 */
public double[] getGradientsEnergyPerAtom(ConstantNode[] nodes) {
	double[] returnArray = new double[nodes.length];
	int c = 0;
    for (ConstantNode cn : nodes) {
    	returnArray[c] = this.getGradientEnergyPerAtom(cn);
    	c++;
    }
    return returnArray;
}

/**
 * 
 * @param node
 * @return Gradient of the 
 * total energy per atom with respect to variation in the value of node
 */
public double getGradientEnergyPerAtom(ConstantNode node) {
	boolean computeAnalytically = false;
	Node topNode = m_Potential.getTopNode();
	double gradient = 0;
	if(computeAnalytically) {
		for (int siteNum = 0; siteNum < m_StructureNet.getStructure().numDefiningSites(); siteNum++) {
		  	StructureNet.Bond bond = m_StructureNet.getBondsByDefiningSiteIncludingBondWithItself()[siteNum][0];
		    try{
		    	gradient += topNode.getDerivative(bond, node, null, null);	    	
			} catch (NotFiniteValueException e) {return Double.NaN;}
		}
	}
	else {
	    try{
	    	double dc = 1E-6;
	    	double e0 = this.getEnergy();
	    	
	    	TreePotential potential = this.getPotential().replaceNode(new ConstantNode(node.getValue()+dc), node);
	    	AppliedTreePotential atp = new AppliedTreePotential(potential, this.getStructureNet());
	    	double e = atp.getEnergy();
	    	double de = e-e0;
	    	
	    	gradient += de/dc;	    	
	    } catch (NotFiniteValueException e) {return Double.NaN;}

	}
	topNode.clearKnownValues();
    topNode.clearKnownDerivatives();
    
	return gradient/this.getStructureNet().numDefiningSites();
}

public TreePotential getPotential() {
	return m_Potential;
}
  
public StructureNet getStructureNet() {
	return m_StructureNet;
}
public Double[] getForcesAnalytical() throws NotFiniteValueException {
  	Node topNode = m_Potential.getTopNode();
  	int numDefiningSites = this.getStructureNet().numDefiningSites();
    double[][] array = new double[numDefiningSites][3];
    for (int siteNum = 0; siteNum < numDefiningSites; siteNum++) {
//	    Get the bond with itself
    	StructureNet.Bond bond = this.getStructureNet().getBondsByDefiningSiteIncludingBondWithItself()[siteNum][0];
    	Structure.Site definingSite = bond.getCentralSiteOfBond().getDefiningSite();
    	
//    	The force on site i is Fi = -dE/dri. Where E is the energy of the system
    	
    	for (int j = 0; j < numDefiningSites; j++) {
        	bond = this.getStructureNet().getBondsByDefiningSiteIncludingBondWithItself()[j][0];
      	  //No need to do deepCopy() because the derivative maps in the nodes consider the specific defining site
      	  //and axis. Moreover, when computing the derivative, sometimes the getValue from the Node class is called. 
      	  //However, this call is independent from the defining site or axis. The Distance node will return the bond
      	  //length because the daxis value is null
	    	array[siteNum][0] -= topNode.getDerivative(bond, null, definingSite, 0);
	    	array[siteNum][1] -= topNode.getDerivative(bond, null, definingSite, 1);
	    	array[siteNum][2] -= topNode.getDerivative(bond, null, definingSite, 2);
    	}
    }
    
    topNode.clearKnownValues();
    topNode.clearKnownDerivatives();
    
    ArrayList<Double> list = new ArrayList<Double>();
    for (int i = 0; i < array.length; i++) {
    	list.add(array[i][0]);
    	list.add(array[i][1]);
    	list.add(array[i][2]);
	}
    
    Double[] returnArray = new Double[list.size()];
    int c = 0;
    for (double d : list) {
    	returnArray[c] = d;
    	c++;
	}
    return returnArray;
}

public Double getForceIsValidAnalytical() throws NotFiniteValueException{
	  //No need to do deepCopy() because the derivative maps in the nodes consider the specific defining site
	  //and axis. Moreover, when computing the derivative, sometimes the getValue from the Node class is called. 
	  //However, this call is independent from the defining site or axis. The Distance node will return the bond
	  //length because the daxis value is null
	  Node topNode = m_Potential.getTopNode();
	  
	  int definingSiteNum = GENERATOR.nextInt(this.getStructureNet().numDefiningSites());
	  StructureNet.Bond bondWithItself = m_StructureNet.getBondsByDefiningSiteIncludingBondWithItself()[definingSiteNum][0];
	  Structure.Site definingSite = bondWithItself.getCentralSiteOfBond().getDefiningSite();
	  
//    This is not the force on site i Fi = -dE/dri. Where E is the energy of the system
//    This is not the force on site i because we need to sum over all the defining sites, similar to getForcesAnalytical()
	  double returnValue = - topNode.getDerivative(bondWithItself, null, definingSite, 0);
	  topNode.clearKnownValues();
	  topNode.clearKnownDerivatives();
	  return returnValue;
}
 
//  From an array containing all the forces for this structure, get the component of the 
//  force at index
public double getForceForwardDifference(int index) throws NotFiniteValueException{
	//index corresponds to this defining site and this axis
	int definingSiteIndex = index/3;
	int axis = index % 3;
	double E = this.getEnergy();
	Double h = m_h;
	Structure.Site definingSite = m_StructureNet.getDefiningSite(definingSiteIndex);
//  	The force along the x-axis on site i is Fxi = -dE/dxi. Where E is the energy of the system, not only of atom i
	double Explush = this.getEnergy(definingSite, axis, h);
	return -(Explush-E)/h;//approximation for -dE/dx
}

//From an array containing all the forces for this structure, get the component of the 
//force at index
public double getForceAnalytical(int index) throws NotFiniteValueException {
	//index corresponds to this defining site and this axis
	int definingSiteIndex = index/3;
	int axis = index % 3;
  	Node topNode = m_Potential.getTopNode();
  	int numDefiningSites = m_StructureNet.numDefiningSites();
    
//	 Get the bond with itself
	StructureNet.Bond bond = m_StructureNet.getBondsByDefiningSiteIncludingBondWithItself()[definingSiteIndex][0];
	Structure.Site definingSite = bond.getCentralSiteOfBond().getDefiningSite();
	
//  The force on site i is Fi = -dE/dri. Where E is the energy of the system
	double returnValue = 0;
	for (int j = 0; j < numDefiningSites; j++) {
    	bond = m_StructureNet.getBondsByDefiningSiteIncludingBondWithItself()[j][0];
  	  //No need to do deepCopy() because the derivative maps in the nodes consider the specific defining site
  	  //and axis. Moreover, when computing the derivative, sometimes the getValue from the Node class is called. 
  	  //However, this call is independent from the defining site or axis. The Distance node will return the bond
  	  //length because the daxis value is null
    	returnValue -= topNode.getDerivative(bond, null, definingSite, axis);
	}
    
    topNode.clearKnownValues();
    topNode.clearKnownDerivatives();
    
    return returnValue;
}

  
public Double[] getForcesForwardDifference() throws NotFiniteValueException {
	  	
	int numDefiningSites = m_StructureNet.numDefiningSites();
	Double[] array = new Double[numDefiningSites*3];
	double e0 = this.getEnergy();
	Double h = m_h;
	
	for (int siteNum = 0; siteNum < numDefiningSites; siteNum++) {
		Structure.Site definingSite = m_StructureNet.getDefiningSite(siteNum);
		
//	  	The force along the x-axis on site i is Fxi = -dE/dxi. Where E is the energy of the system, not only of atom i
		double explush = this.getEnergy(definingSite, 0, h);
		double eyplush = this.getEnergy(definingSite, 1, h);
		double ezplush = this.getEnergy(definingSite, 2, h);
		
		array[3*siteNum] = -(explush-e0)/h;//approximation for -dE/dx
		array[3*siteNum+1] = -(eyplush-e0)/h;//approximation for -dE/dy
		array[3*siteNum+2] = -(ezplush-e0)/h;//approximation for -dE/dz			
	}
				
	return array;
}
  
/**
 * 	You have to comment the line:
 * 	m_KnownBondLengthsNumApprox.put(new InfoHashBond(index, axis), value);
 * 	in the StructureNet class. Otherwise, the forces will be zero
 * @return
 * @throws NotFiniteValueException
 */
  public Double[] getForcesCenteredDifference() throws NotFiniteValueException {
	  	
		int numDefiningSites = m_StructureNet.numDefiningSites();
		Double[] array = new Double[numDefiningSites*3];
		Double h = m_h;
		
		for (int siteNum = 0; siteNum < numDefiningSites; siteNum++) {
			Structure.Site definingSite = m_StructureNet.getDefiningSite(siteNum);
			
//	  	The force along the x-axis on site i is Fxi = -dE/dxi. Where E is the energy of the system, not only of atom i
			double Explush = this.getEnergy(definingSite, 0, h);
			double Eyplush = this.getEnergy(definingSite, 1, h);
			double Ezplush = this.getEnergy(definingSite, 2, h);
			
			
			double Exminush = this.getEnergy(definingSite, 0, -h);
			double Eyminush = this.getEnergy(definingSite, 1, -h);
			double Ezminush = this.getEnergy(definingSite, 2, -h);
			
//			Central-difference approximation: dE/dx ~~ (f(x+h)-f(x-h))/(2h)
			
			array[3*siteNum] = -(Explush-Exminush)/(2*h);//approximation for -dE/dx
			array[3*siteNum+1] = -(Eyplush-Eyminush)/(2*h);//approximation for -dE/dy
			array[3*siteNum+2] = -(Ezplush-Ezminush)/(2*h);//approximation for -dE/dz			
		}
				
	    return array;
  }

//Get the value predicted by the corresponding tree when it is applied to this specific structure
private double getEnergy(Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
	//Need to do deepCopy(), otherwise, the maps in the nodes will return a value that might not correspond to the correct
	//axis. Important because of threading
	Node topNode = this.getPotential().getTopNode().deepCopy();
	double returnValue = 0;
	for (int siteNum = 0; siteNum < m_StructureNet.numDefiningSites(); siteNum++) {
		//Get the bond with itself
		StructureNet.Bond bond = m_StructureNet.getBondsByDefiningSiteIncludingBondWithItself()[siteNum][0];
		returnValue += topNode.getValue(bond, definingSite, axis, daxis);
	}
	//clearKnownValues() whenever we're done with a structure in the training set
	topNode.clearKnownValues();
	return returnValue;
}
	
//private double getEnergy(int siteNum) throws NotFiniteValueException {
//	return this.getEnergy(this.getStructureNet().getDefiningSite(siteNum), null, null);
//}
  
public void clearKnownValues(){
	m_TotalEnergy = null;
	m_LattParam = null;
	m_C11 = null;
	m_C12 = null;
	m_C44 = null;
}

@Override
public int numParameters() {
	//add 9 because of the unit cell vectors
	return 9+3*this.getStructureNet().numDefiningSites();
}

@Override
public double[] getUnboundedParameters(double[] template) {
	
	Vector[] cellVectors  = this.getStructureNet().getStructure().getCellVectors();
	double[] vectorsToArray = new double[9];
	for (int i = 0; i < cellVectors.length; i++) {
		double[] array = cellVectors[i].getCartesianDirection();
		for (int j = 0; j < array.length; j++) {
			vectorsToArray[3*i+j] = array[j];
		}
	}
	
	double[] coords = new double[this.getStructureNet().numDefiningSites()*3];
	int c = 0;
	for (int i = 0; i < coords.length/3; i++) {
		Structure.Site site = this.getStructureNet().getDefiningSite(i);
		for (int j = 0; j < 3; j++) {
			coords[c] = site.getCoords().getCartesianArray()[j];
			c++;
		}
	}
	
	double[] returnArray = new double[this.numParameters()];
	int c1 = 0;
	for (int i = 0; i < vectorsToArray.length; i++) {
		returnArray[c1] = vectorsToArray[i];
		c1++;
	}
	for (int i = 0; i < coords.length; i++) {
		returnArray[c1] = coords[i];
		c1++;
	}
	return returnArray;
}

/**
 * 
 * @return stresses and forces
 * @throws NotFiniteValueException
 */
@Override
public double[] getGradient(double[] template) {
	double[] returnArray = new double[this.numParameters()];
	try {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				double dl = 1E-6;
				double value = 0.0;
				if(m_relax_Shape) {
					AppliedTreePotential atp = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), this.getStructureNet().getNet(i, j, dl));
					value = (atp.getEnergy()-this.getEnergy()) / dl;
				}
				returnArray[3*i+j] = value;
			}
		}
		
		Double[] forcesDouble = this.getForcesAnalytical();
		for (int i = 0; i < forcesDouble.length; i++) {
			returnArray[9+i] = -1.0*forcesDouble[i];
		}
		return returnArray;
	} catch (NotFiniteValueException e) {
		Status.basic("Problem getting gradient in AppliedTreePotential");
		e.printStackTrace();
		Arrays.fill(returnArray, Double.NaN);
		return returnArray;
	}
}

/**
 * Create a new StructureNet with the new coordinates. Then, assign that StructureNet to this AppliedTreePotential
 * @return this
 */
@Override
public IContinuousFunctionState setUnboundedParameters(double[] parameters) {
	
	Vector[] cellVectors = new Vector[3];
	int c = 0;
	while(c < 3) {
		double[] vector = new double[3];
		for (int i = 0; i < 3; i++) {
			vector[i] = parameters[3*c+i];
		}
		cellVectors[c] = new Vector(vector);
		c++;
	}

	//It's unlikely to be a physically meaningful structure if the volume < 1 cubic Angstroms
	//The number of neighbors for this type of cells is too large and building the structure net takes a lot of memory and a long time
	double v = Math.abs(cellVectors[0].crossProductCartesian(cellVectors[1]).innerProductCartesian(cellVectors[2]));
	if(v < 1.0) {
		Status.detail("Cell volume < 1.0 cubic Angstroms, not relaxing structure!");
		return null;
	}
	//It's unlikely to be a physically meaningful structure if the volume > 10^12 cubic Angstroms
	if(v > 1.0E12) {
		Status.detail("Cell volume > 1.0E12 cubic Angstroms, not relaxing structure!");
		return null;
	}
	//It's unlikely to be a physically meaningful structure if the min bond length < 0.1 Angstroms
	if(this.getStructureNet().minOrMaxBondLength(false) < 0.1) {
		Status.detail("Minimum bond length < 0.1 Angstroms, not relaxing structure!");
		return null;
	}
	
	StructureNet newNet = this.getStructureNet().getStructureNet(cellVectors);
	
	Coordinates[] newCoords = new Coordinates[this.getStructureNet().getStructure().numDefiningSites()];
	for (int i = 0; i < this.getStructureNet().getStructure().numDefiningSites(); i++) {
		double[] coords = new double[3];
		for (int j = 0; j < 3; j++) {
			coords[j] = parameters[9+3*i+j];
		}
		newCoords[i] = new Coordinates(coords,CartesianBasis.getInstance());
	}
	newNet = newNet.getStructureNet(newCoords);
	
	this.setNet(newNet);
	
	this.clearKnownValues();
	this.getPotential().getTopNode().clearKnownDerivatives();
	this.getPotential().getTopNode().clearKnownValues();
	
	return this;
}

private void setNet(StructureNet net) {
	m_StructureNet = net;
}
@Override
public double getValue() {
	double energy = Double.NaN;
	try {
		energy = this.getEnergy();
	} catch (NotFiniteValueException e) {
		Status.detail("Problem getting energy for relaxation!");
		if(Math.abs(Status.getLogLevel() - 8) < 1E-8) {
			e.printStackTrace();
		}
	}
	return energy;
}

public Boolean relax(boolean relaxShape) {
//	int maxNumSteps = 50;
//	double minAllowedStep = 1E-5;
//	double maxAllowedStep = Double.MAX_VALUE;
//	double minGrad = 1E-5;
//    GRLinearMinimizer linearMinimizer = new GRLinearMinimizer();
//    linearMinimizer.setMaxNumSteps(maxNumSteps);
//    linearMinimizer.setGradMagConvergence(minGrad);
//    linearMinimizer.setMinAllowedStep(minAllowedStep);
//    linearMinimizer.setMaxAllowedStep(maxAllowedStep);
//    CGMinimizer cgminimizer = new CGMinimizer(linearMinimizer);
//    cgminimizer.setGradMagConvergence(minGrad);
//    cgminimizer.setMinAllowedStep(minAllowedStep);
//    cgminimizer.setMaxAllowedStep(maxAllowedStep);
//    this.m_Minimizer = cgminimizer;
//    
//    m_relax_Shape = relaxShape;
//    CGMinimizer.m_MaxIterations = 50;
//    boolean minimized = false;
//    try {
//    	m_Minimizer.minimize(this);
//    	minimized = true;
//    }catch(NullPointerException e) {
//    	return null;
//    }
//	
//    CGMinimizer.m_MaxIterations = 1;
//    return minimized;
	
	try {
		MultivariateFunction mf = this;
		double[] initialGuess = this.getUnboundedParameters(null);
		ConvergenceChecker<PointValuePair> checker = new SimplePointChecker<PointValuePair>(1e-5, 1e-5);
		NonLinearConjugateGradientOptimizer optimizer = new NonLinearConjugateGradientOptimizer(NonLinearConjugateGradientOptimizer.Formula.POLAK_RIBIERE, checker);
		//Function to be minimized
		ObjectiveFunction of = new ObjectiveFunction(mf);
		OptimizationData ofg = new ObjectiveFunctionGradient(((AppliedTreePotential)mf).getGradient());
		OptimizationData goalType = GoalType.MINIMIZE; 
		OptimizationData ig  = new InitialGuess(initialGuess);
		int maxIterlValue = 500;
		OptimizationData maxIter = new MaxIter(maxIterlValue);
		int maxEvalValue = 500;
		OptimizationData maxEval = new MaxEval(maxEvalValue);
		optimizer.optimize(of,ofg,ig,goalType,maxIter, maxEval);
		return true;
	}catch(NullPointerException  e) {
		return null;
	}catch(MaxCountExceededException e) {
		Status.basic("MaxCountExceededException!");
		return null;
	}

}

/**
 * Central difference approximation of a component of the virial stress. 
 * Computed as the approximation of the derivative of the energy with respect to the strain.
 * @return the component of the Virial stress XX, YY, ZZ, XY, YZ or ZX in units of GPa. Where
 * 0 = X, 1 = Y, 2 = X. Example: 01 = XY
 * @throws NotFiniteValueException 
 */
private double getStressComponent(int direction, int plane) throws NotFiniteValueException {
	
	StructureNet netMinus = this.getStructureNet().getNetForStress(direction, plane, 0);
	StructureNet netPlus = this.getStructureNet().getNetForStress(direction, plane, 1);
	AppliedTreePotential atpMinus = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), netMinus);
	AppliedTreePotential atpPlus = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), netPlus);

	double dE = atpPlus.getEnergy()-atpMinus.getEnergy();
	if(Statistics.compare(dE, 0.0)==0) {
		dE=0.0;
	}
	//Units eV
	double dEdStrain = dE/(2*StructureNet.m_dStrainStress);
	double v0 = this.getStructureNet().getStructure().getDefiningVolume();
	//1 eV/cubicAngstrom = 160.2176620770033 GPa
	double eVperCubicAngstromToGPa = 160.2176620770033; 
	return (dEdStrain/v0)*eVperCubicAngstromToGPa;
}

/**
 *  if(index==0) 	  {direction=0;plane=0;}//XX   
	else if(index==1) {direction=1;plane=1;}//YY  
	else if(index==2) {direction=2;plane=2;}//ZZ   
	else if(index==3) {direction=0;plane=1;}//XY   
	else if(index==4) {direction=1;plane=2;}//YZ   
	else if(index==5) {direction=2;plane=0;}//ZX   
 * @param index
 * @return stress in units of GPa
 * @throws NotFiniteValueException
 */
public double getStressComponent(int index) throws NotFiniteValueException {	
	int direction, plane;
	if(index==0) 	  {direction=0;plane=0;}//XX
	else if(index==1) {direction=1;plane=1;}//YY
	else if(index==2) {direction=2;plane=2;}//ZZ
	else if(index==3) {direction=0;plane=1;}//XY
	else if(index==4) {direction=1;plane=2;}//YZ
	else if(index==5) {direction=2;plane=0;}//ZX
	else{
		Status.basic("Invalid index");
		new Exception().printStackTrace();
		return Double.NaN;
	}

	return this.getStressComponent(direction, plane);
}

/**
 * 	if(index==0) 	  {direction=0;plane=0;}//XX   
	else if(index==1) {direction=1;plane=1;}//YY  
	else if(index==2) {direction=2;plane=2;}//ZZ   
	else if(index==3) {direction=0;plane=1;}//XY   
	else if(index==4) {direction=1;plane=2;}//YZ   
	else if(index==5) {direction=2;plane=0;}//ZX   
 * @return array of components of stress in GPa in this order: XX, YY, ZZ, XY, YZ, ZX
 * @throws NotFiniteValueException
 */
public Double[] getStressComponents() throws NotFiniteValueException {	
	Double[] returnArray = new Double[6];
	for (int i = 0; i < 6; i++) {
		returnArray[i] = this.getStressComponent(i);
	}
	return returnArray;
}

/**
 * Only implemented for cubic systems
 * Bulk modulus Voigt average (upper bound for polycrystalline material) = ( (c11 + c22 + c33) + 2.0*(c12 + c23 + c31) )/9.0; where cij is an element of the
 * * stiffness matrix (i.e., elastic tensor)
 * Bulk modulus Reuss average (lower bound for polycrystalline material) = 1.0/( (s11 + s22 + s33) + 2.0*(s12 + s23 + s31) ); where sij is an element of the 
 * compliance tensor, and the compliance tensor is the inverse of the stiffness tensor
 * Bulk modulus Voig-Reuss-Hill average = (Voigt + Reuss)/2
 * @return Voig-Reuss-Hill average bulk modulus
 * @throws NotFiniteValueException 
 */
public double getBulkModulusVRH() throws NotFiniteValueException {
	double c11 = this.getElasticConstant("C11");
	double c22 = c11;
	double c33 = c11;
	double c12 = this.getElasticConstant("C12");
	double c23 = c12;
	double c31 = c12;
	double c44 = this.getElasticConstant("C44");
	
	double voigt = ( (c11 + c22 + c33) + 2.0*(c12 + c23 + c31) ) / 9.0;
	
	//Values of elastic tensor
    double[][] eTV = {
    		{c11, c12, c12, 0, 0, 0},
    		{c12, c11, c12, 0, 0, 0},
    		{c12, c12, c11, 0, 0, 0},
    		{0, 0, 0, c44, 0, 0},
    		{0, 0, 0, 0, c44, 0},
    		{0, 0, 0, 0, 0, c44}
    };
    //elastic tensor
    RealMatrix eT = new Array2DRowRealMatrix(eTV);
    //compliance tensor
	try {
	    RealMatrix complianceTensor = (new LUDecomposition(eT)).getSolver().getInverse();
	    //Values of compliance tensor
	    double[][] cTV = complianceTensor.getData();
		
		double reuss = 1.0/( (cTV[0][0] + cTV[1][1] + cTV[2][2]) + 2.0*(cTV[0][1] + cTV[1][2] + cTV[2][0]) );
		
		return (voigt + reuss)/2.0;
	} catch (org.apache.commons.math4.linear.SingularMatrixException e) {
		return Double.NaN;
	}
}

public double getPredicted(String property, int index) throws NotFiniteValueException {
	if(property.equals("Energy")) {
		return this.getEnergy();
	}
	else if(property.endsWith("Force")) {
		return this.getForceForwardDifference(index);
	}
	else if(property.equals("Stress")) {
		return this.getStressComponent(index);
	}
	else {
		Status.basic("Haven't implemented computation of "+property+" !");
	}
	return Double.NaN;
}

/**
 * For cubic systems, the net should be close to the 
 * local optimum
 * @return
 */
public double getLatticeParameter() {
	if(m_LattParam!=null) {
		return m_LattParam;
	}
	this.relax(true);
	double lattPar = 0.0;
	StructureNet[] sS = this.getStructureNet().getNetsStrain(false);

	double[] energies = new double[sS.length];
	for (int i = 0; i < sS.length; i++) {
		try {
			energies[i] = (new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(),this.getPotential().getCutoffDistance()), sS[i])).getEnergy()/sS[i].numDefiningSites();
		} catch (NotFiniteValueException e) {
			return Double.NaN;
		}
	}
	
	//volumes per atom
	double[] x = new double [sS.length];
	for (int i = 0; i < x.length; i++) {
		x[i] = sS[i].getStructure().getDefiningVolume()/sS[i].numDefiningSites();
	}
	
	//Energies per atom
	double[] y = energies;
    WeightedObservedPoints obs = new WeightedObservedPoints();
    for (int i = 0; i < x.length; i++) {obs.add(x[i],y[i]);}	  
    int order = 2;
    PolynomialCurveFitter fitter = PolynomialCurveFitter.create(order);
    fitter = fitter.withMaxIterations(10000);
    double[] coeff = new double[order + 1];
    try {
    	coeff = fitter.fit(obs.toList());
    	}catch(org.apache.commons.math3.exception.TooManyIterationsException |
    			org.apache.commons.math3.exception.ConvergenceException e) {
    	}
    //Solve for equilibrium volume by dE/dv = 0
    double v0 = -coeff[1]/(2*coeff[2]);
    lattPar = Math.pow(v0*this.getStructureNet().numDefiningSites(), 1/3.0);
	m_LattParam = lattPar;
	return lattPar;
}

public double[] getLatticeParameterAndElasticConstants() {
	return new double[] {this.getLatticeParameter(),this.getElasticConstant("C11"),this.getElasticConstant("C12"),this.getElasticConstant("C44")};
}
/**
 * For cubic systems, the net should be close to the 
 * local optimum
 * @param elasticConstant (example: C11,C12,C44)
 * @return elastic constant in units of GPa
 */
public double getElasticConstant(String elasticConstant) {
	if(elasticConstant.equals("C11") && m_C11!=null) {
		return m_C11;
	}
	else if(elasticConstant.equals("C12") && m_C12!=null) {
		return m_C12;
	}
	else if(elasticConstant.equals("C44") && m_C44!=null) {
		return m_C44;
	}
	this.relax(true);
	StructureNet net0 = this.getStructureNet();
	
	StructureNet[] sS = new StructureNet[5];
	double[] strains = new double[sS.length];
	double strain = -0.01;
	for (int i = 0; i < strains.length; i++) {
		//In case you want to convert to linear from volumetric strain: linStrain = Math.pow(volStrain + 1, 1.0/3) - 1.0;
		strains[i] = strain;
		strain += 0.005;
	}
	for (int i = 0; i < strains.length; i++) {
		if(elasticConstant.equals("C11") || elasticConstant.equals("C12")) {
			sS[i] = net0.getStructureNetNormalStrain(0, strains[i]);
		}
		else if(elasticConstant.equals("C44")) {
			sS[i] = net0.getStructureNetShear(1, 2, strains[i]);
		}
		else {
			Status.basic("Invalid elastic constant !");
			return Double.NaN;
		}
	}
	
	double[] stresses = new double[sS.length];
	for (int i = 0; i < sS.length; i++) {
		try {
			int index = 0;
			if(elasticConstant.equals("C11")) {
				index = 0;
			}
			else if(elasticConstant.equals("C12")) {
				index = 1;
			}
			else if(elasticConstant.equals("C44")) {
				index = 4;
			}
			else {
				Status.basic("Invalid elastic constant !");
				return Double.NaN;
			}
			AppliedTreePotential atp_stress = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), sS[i]);
			atp_stress.relax(false);
			stresses[i] = atp_stress.getStressComponent(index);
			
		} catch (NotFiniteValueException e) {
			if(elasticConstant.equals("C11")) {
				m_C11 = Double.NaN;
			}
			else if(elasticConstant.equals("C12")) {
				m_C12 = Double.NaN;
			}
			else if(elasticConstant.equals("C44")) {
				m_C44 = Double.NaN;
			}
			return Double.NaN;
		}
	}
	
	double[] x = strains;
	double[] y = stresses;
    WeightedObservedPoints obs = new WeightedObservedPoints();
    for (int i = 0; i < x.length; i++) {obs.add(x[i],y[i]);}	    
    PolynomialCurveFitter fitter = PolynomialCurveFitter.create(1);
    fitter = fitter.withMaxIterations(10000);
    double[] coeff = new double[2];
    try {
    	coeff = fitter.fit(obs.toList());
	}catch(org.apache.commons.math3.exception.TooManyIterationsException |
			org.apache.commons.math3.exception.ConvergenceException e) {
		if(elasticConstant.equals("C11")) {
			m_C11 = Double.NaN;
		}
		else if(elasticConstant.equals("C12")) {
			m_C12 = Double.NaN;
		}
		else if(elasticConstant.equals("C44")) {
			m_C44 = Double.NaN;
		}
		return Double.NaN;
	}
    
    double returnValue = coeff[1];
	if(elasticConstant.equals("C11")) {
		m_C11 = returnValue;
	}
	else if(elasticConstant.equals("C12")) {
		m_C12 = returnValue;
	}
	else if(elasticConstant.equals("C44")) {
		m_C44 = returnValue;
	}

	return returnValue;
}

@Override
public double value(double[] point) {
	this.setUnboundedParameters(point);
	return this.getValue();
}
public Gradient_of_MultivariateFunction getGradient() {
	return new Gradient_of_MultivariateFunction(this);
}
private class Gradient_of_MultivariateFunction implements MultivariateVectorFunction{
	public AppliedTreePotential m_mvi;
	public Gradient_of_MultivariateFunction(MultivariateFunction mvi) {
		m_mvi = (AppliedTreePotential) mvi;
	}
	@Override
	public double[] value(double[] point) throws IllegalArgumentException {
		m_mvi.setUnboundedParameters(point);
		return m_mvi.getGradient(null);
	}
}
}