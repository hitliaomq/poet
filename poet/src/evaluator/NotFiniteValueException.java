package evaluator;

public class NotFiniteValueException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 405839195160166855L;

	public NotFiniteValueException(){
		super();
	}
}
