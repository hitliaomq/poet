package evaluator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;
import gAEngine.IIndividual;
import matsci.io.app.log.Status;
import util.Statistics;

public class DataSet {
	private static final Random GENERATOR = new Random();
	
	private String m_Title;
	private String m_DataDirectoryName;

	//StructureNet contains the features
	private StructureNet[] m_Nets;

	//This map contains an array of a target property for every structure in the dataset
	//This approach gives flexibility when choosing which properties to use for training
	private HashMap<String, double[][]> m_DataMap;
	
	//Contains the minimum energy structure of phases
	private HashMap<String, StructureNet> m_MinEnergyNetMap;


	//Weight of each target property for the training
	private HashMap<String, Double> m_WeightMap;
	
	private HashMap<String, SubSetElement[]> m_LocalIndicesMap;
	private HashMap<String, SubSetElement[]> m_GlobalIndicesMap;
	private HashMap<String, SubSetElement[]> m_AllIndicesMap;
	
	private int m_SizeLocal;
	private int m_SizeGlobal;

	//Flag to know which subset is being used (e.g., All, Local or MiniBatch)
	private String m_SubSet;
	
	private double[][] m_Energies;
	private double[][] m_Forces;
	private double[][] m_Stresses;
	private SubSetElement[] m_LocalIndices_Energy;
	private SubSetElement[] m_LocalIndices_Force;
	private SubSetElement[] m_LocalIndices_Stress;

	private String[] m_Properties;
	
//	//DFT data
//    public static StructureNet Cu_FCC_0;
//	public static final double m_FCC_E0			= -15.00309740; 		//eV
//	public static final double m_FCC_Cu_lattPar = 3.63398494642027; 	//Angstroms
//	public static final double m_FCC_Cu_c11 	= 178.23; 				//GPa
//	public static final double m_FCC_Cu_c12 	= 122.11;
//	public static final double m_FCC_Cu_c44 	= 79.97;
//	
//    public static StructureNet Cu_BCC_0;
//	public static final double m_BCC_E0			= -7.43039145;
//	public static final double m_BCC_Cu_lattPar = 2.88777921978903;
//	public static final double m_BCC_Cu_c11 	= 130.41;
//	public static final double m_BCC_Cu_c12 	= 142.39;
//	public static final double m_BCC_Cu_c44 	= 97.101;
//	
//    public static StructureNet Cu_HCP_0;
//	public static final double m_HCP_E0			= -14.98470991;
//	public static final double m_HCP_Cu_mag_a 	= 2.56297;
//	public static final double m_HCP_Cu_c 		= 8.41447;
//
//	public static final double m_FCC_Mo_lattPar = 4.001506524723162;
//	public static final double m_FCC_Mo_c11 	= 119.25;
//	public static final double m_FCC_Mo_c12 	= 301.32;
//	public static final double m_FCC_Mo_c44 	= -8.00002;
//	
//	public static final double m_BCC_Mo_lattPar = 3.161091638757945;
//	public static final double m_BCC_Mo_c11 	= 465.33;
//	public static final double m_BCC_Mo_c12 	= 161.32;
//	public static final double m_BCC_Mo_c44 	= 113.54;
//	
//	
//	//Reference 1: Mishin et al. (2001). DOI: 10.1103/PhysRevB.63.224106
//	public static final double m_FCC_Cu_lattPar_exper_ref1 = 3.615;
//	public static final double m_FCC_Cu_c11_exper_ref1 	= 170;
//	public static final double m_FCC_Cu_c12_exper_ref1 	= 122.5;
//	public static final double m_FCC_Cu_c44_exper_ref1	= 75.8;
//	
//	//Reference 2: Mendelev et al. (2008). https://doi.org/10.1063/1.4953676
//	public static final double m_FCC_Cu_lattPar_exper_ref2 = 3.640; //article's DFT
//	public static final double m_FCC_Cu_c11_exper_ref2 	= 170.0;
//	public static final double m_FCC_Cu_c12_exper_ref2 	= 123.0;
//	public static final double m_FCC_Cu_c44_exper_ref2	= 76.0;
//
//	//Reference 3: Rassoulinejad-Mousavi et al. https://doi.org/10.1063/1.4953676
//	public static final double m_FCC_Cu_lattPar_exper_ref3 = Double.NaN;
//	public static final double m_FCC_Cu_c11_exper_ref3 	= 168.4;
//	public static final double m_FCC_Cu_c12_exper_ref3 	= 121.4;
//	public static final double m_FCC_Cu_c44_exper_ref3	= 75.4;

	public DataSet(StructureNet[] nets) {
		m_Nets = nets;
		
		m_DataMap = new HashMap<String, double[][]>();
		m_WeightMap = new HashMap<String, Double>();
		m_LocalIndicesMap = new HashMap<String, SubSetElement[]>();
		m_GlobalIndicesMap = new HashMap<String, SubSetElement[]>();
		m_AllIndicesMap = new HashMap<String, SubSetElement[]>();

	}
	
	/**
	 * 
	 * @param property (Examples of keys: Energy, Force, Stress, ElasticConstants_FCC, 
	 * a0_FCC, E0_FCC, E0_Vacancy_FCC_Unrelaxed)
	 * @param value
	 */
	public void setTargetProperty(String property, double[][] value){
		if(property.equals("Energy")) {
			m_Energies = value;
		}
		else if(property.equals("Force")) {
			m_Forces = value;
		}
		else if(property.equals("Stress")) {
			m_Stresses = value;
		}
		SubSetElement[] subSetElements = new SubSetElement[value.length];
		for (int i = 0; i < subSetElements.length; i++) {
			int[] indices = new int[value[i].length];
			for (int j = 0; j < indices.length; j++) {
				indices[j] = j;
			}
			subSetElements[i] = new SubSetElement(i, indices);
		}
		m_DataMap.put(property, value);
		this.setIndices("All", property, subSetElements);
		m_Properties = null;
	}
	
	/**
	 * Set the minimum energy structure of a phase
	 * @param phase
	 * @param net
	 */
	public void setMinEnergyNet(String phase, StructureNet minEnergyNet) {
		if(m_MinEnergyNetMap == null) {
			m_MinEnergyNetMap = new HashMap<String, StructureNet>();
		}
		m_MinEnergyNetMap.put(phase, minEnergyNet);
	}
	
	public StructureNet getMinEnergyNet(String phase) {
		return m_MinEnergyNetMap.get(phase);
	}
	/**
	 * This is important because the algorithm 
	 * can use a subset of the data
	 * @param property (Examples of keys: Energy, Force, Stress, ElasticConstants_FCC, 
	 * a0_FCC, deltaE_BCC_FCC, deltaE_Vacancy_FCC_Unrelaxed)
	 * @param subSet
	 * @return
	 */
	public double[] getTargetProperty(String property) {
		double[][] values =  this.getDataMap().get(property); 
		SubSetElement[] subSetElements = this.getIndices(this.getSubSet(), property);
		return this.getTargetProperty(values, subSetElements);
	}
	
	public double[] getTargetProperty(double[][] values, SubSetElement[] subSetElements) {
		int numPoints = 0;
		for (int i = 0; i < subSetElements.length; i++) {
			numPoints += subSetElements[i].getIndices().length;
		}
		double[] returnArray = new double[numPoints];
		int c = 0;
		for (int i = 0; i < subSetElements.length; i++) {
			int netIndex = subSetElements[i].getNetIndex();
			int[] indices = subSetElements[i].getIndices();
			for (int j = 0; j < indices.length; j++) {
				returnArray[c] = values[netIndex][indices[j]];
				c++;
			}
		}
		return returnArray;
	}
	public double[] getEnergy_Local(){
		return this.getTargetProperty(m_Energies, m_LocalIndices_Energy);
	}
	public double[] getForce_Local(){
		return this.getTargetProperty(m_Forces, m_LocalIndices_Force);
	}
	public double[] getStress_Local(){
		return this.getTargetProperty(m_Stresses, m_LocalIndices_Stress);
	}
	public StructureNet[] getNets() {
		return m_Nets;
	}	
	public String[] getTargetProperties(){
		if(m_Properties != null) {
			return m_Properties;
		}
		Set<String> set = m_DataMap.keySet();
		String[] properties = new String[set.size()];
		int c = 0;
		for (String string : set) {
			properties[c] = string;
			c++;
		}
		m_Properties = properties;
		return properties;
	}
	
	public void setTitle(String title) {
		m_Title = title;
	}
	public String getTitle() {
		return m_Title;
	}
	public void setDataDirectoryName(String title) {
		m_DataDirectoryName = title;
	}
	public String getDataDirectoryName() {
		return m_DataDirectoryName;
	}
	public void setWeight(String property, double weight) {
		m_WeightMap.put(property, weight);
	}
	
	public double getWeight(String property){
		return m_WeightMap.get(property);
	}
	
	public void setIndices(String subSetName, String property, SubSetElement[] subSet) {
		if(subSetName.equals("Local")) {
			if(property.equals("Energy")) {
				m_LocalIndices_Energy = subSet;
			}
			else if(property.equals("Force")) {
				m_LocalIndices_Force = subSet;
			}
			else if(property.equals("Stress")) {
				m_LocalIndices_Stress = subSet;
			}
			m_LocalIndicesMap.put(property, subSet);
		}
		else if(subSetName.equals("MiniBatch")) {
			m_GlobalIndicesMap.put(property, subSet);
		}
		else if(subSetName.equals("All")) {
			m_AllIndicesMap.put(property, subSet);
		}
		
		String subSet0 = this.getSubSet();
		this.setSubset(subSetName);
		double mean = Statistics.getMean(this.getTargetProperty(property));
		double sd = Statistics.getStandardDeviation(this.getTargetProperty(property), subSetName.equals("All"));
		double median = Statistics.getMedian(this.getTargetProperty(property));

		boolean notproperty_0k = property.equals("Energy") || property.equals("Force") || property.equals("Stress");
		if(notproperty_0k) {
			//Print statistics
			Status.basic("Mean "+property+" "+subSetName+" "+this.getTitle()+" = "+mean);
			Status.basic("SD "+property+" "+subSetName+" "+this.getTitle()+" = "+sd);
			Status.basic("Median "+property+" "+subSetName+" "+this.getTitle()+" = "+median);
		}
		this.setSubset(subSet0);
	}
	
	/**
	 * Example: you have an array int[numStructures][2][numProperties] called indices. For indices[i][j][k], i corresponds
	 * to a structure, j=0 contains the index of that structure in m_Nets (call indices[i][0][0]), and j=1 contains k corresponds to the index of a property
	 * in the array of properties. 
	 * @param subSet
	 * @param property
	 * @return
	 */
	public SubSetElement[] getIndices(String subSet, String property){
		if(subSet.equals("Local")) {
			return m_LocalIndicesMap.get(property);
		}
		else if(subSet.equals("MiniBatch")) {
			return m_GlobalIndicesMap.get(property);
		}
		else if(subSet.equals("All")) {
			return m_AllIndicesMap.get(property);
		}
		return null;
	}
	public void setSubset(String subSet) {
		m_SubSet = subSet;
	}
	public String getSubSet() {
		return m_SubSet;
	}
	public HashMap<String, double[][]> getDataMap(){
		return m_DataMap;
	}
	
	private int[] getIndicesUnique(ArrayList<String> selectedIndices, double[][] bins) {
		int indexOtherBin;
		int numAttempts = 0;
		int index;
		String indices;
		do {
			do {
				indexOtherBin = GENERATOR.nextInt(bins.length);
			}while(bins[indexOtherBin].length < 1);
			index = GENERATOR.nextInt(bins[indexOtherBin].length);
			indices = Integer.toString(indexOtherBin)+","+Integer.toString(index);
			if(numAttempts > 100*bins.length) {
				Status.warning(100*bins.length+" attemps to select a non-empty bin! Decrease the number of the minibatch (less than number of training examples).  Stack trace: "+Arrays.toString(new Exception().getStackTrace()));
				break;
			}
			numAttempts++;
		}while(selectedIndices.contains(indices));
		return new int[] {indexOtherBin,index};
	}
	
	private SubSetElement[] generateMiniBatch(String property) {
		String originalSubset = this.getSubSet();
		this.setSubset("All");
		double[] knownValues = this.getTargetProperty(property);
		double[][] knownValuesPerStructure = this.getDataMap().get(property);
		this.setSubset(originalSubset);

		int numBins = this.getSize("MiniBatch");
		double[][] bins = Statistics.getBins(knownValues, numBins);
		double[] miniBatchValues = new double[bins.length];
		
//		//Use all the energies
//		if(property.equals("Energy")) {
//			miniBatchValues = knownValues;
//		}
//		else {
			ArrayList<String> selectedIndices = new ArrayList<String>();
			//Randomly select a value from every bin
			for (int i = 0; i < bins.length; i++) {
				if(bins[i].length < 1) {
					//Use a non-empty randomly selected bin in case this bin is empty
					int[] inds = this.getIndicesUnique(selectedIndices, bins);
					int indexOtherBin = inds[0];
					int index = inds[1];
					selectedIndices.add(Integer.toString(indexOtherBin)+","+Integer.toString(index));
					miniBatchValues[i] = bins[indexOtherBin][index];
				}
				else {
					String indices;
					int index;
					int attempts = 0;
					do {
						//Select a random index from this bin if it has not been selected previously
						index = GENERATOR.nextInt(bins[i].length);
						indices = Integer.toString(i)+","+Integer.toString(index);
						//Consider that all the bins from this bin were selected, so count the attempts
						attempts++;
					}while(selectedIndices.contains(indices) && attempts < 100*bins[i].length);
					if(!(attempts < 100*bins[i].length)) {
						//Use a non-empty randomly selected bin
						int[] inds = this.getIndicesUnique(selectedIndices, bins);
						int indexOtherBin = inds[0];
						index = inds[1];
						selectedIndices.add(Integer.toString(indexOtherBin)+","+Integer.toString(index));
						miniBatchValues[i] = bins[indexOtherBin][index];
					}
					else {
						selectedIndices.add(indices);
						miniBatchValues[i] = bins[i][index];
					}
				}
			}
//		}
		
		int[][] indices = new int[miniBatchValues.length][2];
		for (int i = 0; i < indices.length; i++) {
			indices[i] = Statistics.getIndices(miniBatchValues[i], knownValuesPerStructure);
		}
		
		SubSetElement[] elements = new SubSetElement[indices.length];
		for (int i = 0; i < elements.length; i++) {
			elements[i] = new SubSetElement(indices[i][0], new int[] {indices[i][1]});
		}
		return elements;
	}
	
	/**
	 * Randomly select elements from the global training set (i.e., from the mini-batch)
	 * The global training set is specified by a 2 dimensional array of size
	 * "number of structures in the global training" set by 2
	 * If such array is called globalArray, then globalArray[i][0] contains the structure index of 
	 * the element i, and globalArray[i][1] contains the index of the property of that structure.
	 * For example, if the property is force, and the structure has 32 atoms, then there are 32*3 (3 components of force vector)
	 * possible values for globalArray[i][1], if the property is stress, then there are 6 (6 components of virial stress tensor)
	 * @param property (energy, force, stress, etc)
	 * @return "numLocal" number of randomly selected elements from the global training set (i.e., from the mini-batch)
	 */
	private SubSetElement[] generateLocalSubSet(String property) {
//		The following code selects a random sample from the MiniBatch training set
		int numLocal = this.getSize("Local");
		String subset0 = this.getSubSet();
//		this.setIndices("MiniBatch", property, this.generateMiniBatch(property));//TODO delete
		this.setSubset("MiniBatch");
		int[][] indices = this.getIndices(property);
		this.setSubset(subset0);
		int[] indicesLocal = Statistics.generateRandomIndices(numLocal, indices.length, null);
		SubSetElement[] elements = new SubSetElement[indicesLocal.length];
		for (int i = 0; i < elements.length; i++) {
			elements[i] = new SubSetElement(indices[indicesLocal[i]][0], new int[] {indices[indicesLocal[i]][1]});
		}
		
		return elements;
		
////		The following code selects a localized subset for the local training data
//		String originalSubset = this.getSubSet();
//		this.setSubset("All");
//		double[] knownValues = this.getTargetProperty(property);
//		double[][] knownValuesPerStructure = this.getDataMap().get(property);
//		this.setSubset(originalSubset);
//		
//		int numValues = this.getSize("Local");
//		//numBins = numValues implies random sampling
//		int numBins = (int)(0.2*knownValues.length);
//		double[][] bins = Statistics.getBins(knownValues, numBins);
//		
//		ArrayList<Double> valuesBins = new ArrayList<Double>();
//		
//		int initialBinIndex = GENERATOR.nextInt(bins.length);
//		Integer bin = initialBinIndex;
//		boolean searchDirection = GENERATOR.nextBoolean();		
//
//		do {
//			//Randomly select a bin that is not empty in the search direction defined by searchDirection
//			int numAttempts = 0;
//			do {
//				bin = Statistics.getNearestOccupiedBin(bins, bin, searchDirection);
//				if(bin == null) {
//					searchDirection = !searchDirection;
//					bin = Statistics.getNearestOccupiedBin(bins, initialBinIndex, searchDirection);
//				}
//				if(bin==null) {
//					bin = initialBinIndex;
//				}
//				if(numAttempts > 100*bins.length) {
//					new Exception().printStackTrace();
//					Status.basic(100*bins.length+" attemps to select a non-empty bin!");
//					break;
//				}
//				numAttempts++;
//			}while(bins[bin].length < 1);
//
//			//Randomly select values from bin
//			int numValuesBin = numValues > bins[bin].length ? bins[bin].length : numValues;
//			int[] indicesBin = Statistics.generateRandomIndices(numValuesBin, bins[bin].length);
//			for (int i = 0; i < indicesBin.length; i++) {
//				valuesBins.add(bins[bin][indicesBin[i]]);
//			}
//		}while(valuesBins.size() < numValues);
//		
//		//The values that were selected
//		double[] values = new double[numValues];
//		for (int i = 0; i < values.length; i++) {
//			values[i] = valuesBins.get(i);
//		}
//		//Get the indices corresponding to the values
//		int[][] indices = new int[values.length][2];
//		for (int i = 0; i < indices.length; i++) {
//			indices[i] = Statistics.getIndices(values[i], knownValuesPerStructure);
//		}
//		SubSetElement[] elements = new SubSetElement[indices.length];
//		for (int i = 0; i < elements.length; i++) {
//			elements[i] = new SubSetElement(indices[i][0], new int[] {indices[i][1]});
//		}
//		
//		return elements;
	}

	private void updateGlobalIndices() {
		if(!PotentialPopulation.lock(PotentialPopulation.m_GlobalDirectory+"/global.lock")){
			new Exception().printStackTrace();
			Status.basic("Couldn't lock global!");
			return;
		}
		
		if(PotentialPopulation.m_ProcessorNum==1 || !(new File(PotentialPopulation.m_GlobalDirectory+"/globalIndices")).exists()) {
			try{
			    FileWriter fw = new FileWriter(PotentialPopulation.m_GlobalDirectory+"/globalIndices");
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw);
				String[] properties = this.getTargetProperties();
				for (String property : properties) {
					if(property.equals("Energy")||property.equals("Force")||property.equals("Stress")) {
						SubSetElement[] subSet = this.generateMiniBatch(property);
						this.setIndices("MiniBatch", property, subSet);
						out.println(property);
						for (SubSetElement subSetElement : subSet) {
							out.print(subSetElement.getNetIndex()+" ");
						}
						out.println();
						for (SubSetElement subSetElement : subSet) {
							out.print(subSetElement.getIndices()[0]+" ");
						}
						out.println();
					}
				}
				out.close();
			}catch(IOException e){
				Status.basic("Problem printing global indices!");
				e.printStackTrace();
				return;
			}

		}
		else {
			try{
				BufferedReader br = new BufferedReader(new FileReader(PotentialPopulation.m_GlobalDirectory+"/globalIndices"));
				
				int c = 0;
				int num = 0;
				String[] properties = this.getTargetProperties();
				for (String property : properties) {
					if(property.equals("Energy")||property.equals("Force")||property.equals("Stress")) {
						num++;
					}
				}
				while(c < num) {
					int[] structureIndices = new int[this.getSize("MiniBatch")];
					int[] valueIndices = new int[this.getSize("MiniBatch")];
					
					StringTokenizer tokenizer = new StringTokenizer(br.readLine());
					String property = tokenizer.nextToken();
					tokenizer = new StringTokenizer(br.readLine());

					for (int i = 0; i < structureIndices.length; i++) {
						structureIndices[i] = Integer.parseInt(tokenizer.nextToken());
					}
					tokenizer = new StringTokenizer(br.readLine());
					for (int i = 0; i < valueIndices.length; i++) {
						valueIndices[i] = Integer.parseInt(tokenizer.nextToken());
					}
					SubSetElement[] subSet = new SubSetElement[structureIndices.length];
					for (int i = 0; i < subSet.length; i++) {
						subSet[i] = new SubSetElement(structureIndices[i], new int[] {valueIndices[i]});
					}
					this.setIndices("MiniBatch", property, subSet);
					c++;
				}
				
				br.close();
			}catch(IOException e){
				Status.basic("Problem in setDynamicIndices(PotentialSubPopulation,boolean)!");
				e.printStackTrace();
				return;
			}
		}
		if(!PotentialPopulation.unlock(PotentialPopulation.m_GlobalDirectory+"/global.lock")){
			Status.basic("Couldn't unlock global!");
			new Exception().printStackTrace();
			return;
		}
	}
	
	public void updateDynamicIndices(ArrayList<PotentialSubPopulation> subPopulations, boolean global) {
		if(global) {
			this.updateGlobalIndices();
		}
		else {
			String[] properties = this.getTargetProperties();
			for (String property : properties) {
				if(property.equals("Energy")||property.equals("Force")||property.equals("Stress")) {
					SubSetElement[] subSet = this.generateLocalSubSet(property);
					this.setIndices("Local", property, subSet);
				}
			}
		}
		for (PotentialSubPopulation subPopulation : subPopulations) {
			for (IIndividual indiv : subPopulation.getIndividuals()) {
				((PotentialEvaluator)indiv).clearValues(false);
			}
		}
	}
	
	public int getSize(String subset) {
		if(subset.equals("Local")) {
			return m_SizeLocal;
		}
		if(subset.equals("MiniBatch")) {
			return m_SizeGlobal;
		}
		return Integer.MIN_VALUE;
	}
	public void setSize(String subset, int size) {
		if(subset.equals("Local")) {
			m_SizeLocal = size;
		}
		else if(subset.equals("MiniBatch")) {
			m_SizeGlobal = size;
		}
		else {
			Status.basic("Invalid subset!");
		}
	}
	public int[] getIndicesEnergies(String subSet) {
		SubSetElement[] subSetEnergy = this.getIndices(subSet, "Energy");
		int[] indices = new int[subSetEnergy.length];
		for (int i = 0; i < indices.length; i++) {
			indices[i] = subSetEnergy[i].getNetIndex();
		}
		return indices;
	}
	public int[][] getIndices(String property) {
		SubSetElement[] subSet = this.getIndices(this.getSubSet(), property);
		int[][] indices = new int[subSet.length][2];
		for (int i = 0; i < indices.length; i++) {
			indices[i][0] = subSet[i].getNetIndex();
			indices[i][1] = subSet[i].getIndices()[0];
			if(subSet[i].getIndices().length > 1) {
				return null;
			}
		}
		return indices;
	}
	public double[][] getEnergy(){
		return m_Energies;
	}
	public double[][] getForce(){
		return m_Forces;
	}
	public double[][] getStress(){
		return m_Stresses;
	}
	public SubSetElement[] getLocalIndicesEnergy() {
		return m_LocalIndices_Energy;
	}
	public SubSetElement[] getLocalIndicesForce() {
		return m_LocalIndices_Force;
	}
	public SubSetElement[] getLocalIndicesStress() {
		return m_LocalIndices_Stress;
	}
	public class SubSetElement {
		private int m_NetIndex;
		private int[] m_Indices;
		public SubSetElement(int structureIndex, int[] indices) {
			m_NetIndex = structureIndex;
			m_Indices = indices;
		}
		public int getNetIndex() {
			return m_NetIndex;
		}
		public int[] getIndices() {
			return m_Indices;
		}
	}
}
