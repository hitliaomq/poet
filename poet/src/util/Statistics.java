package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import matsci.io.app.log.Status;

public class Statistics {

protected static final Random GENERATOR = new Random();
public static final double PRECISION = 1E-13;

public static double getMSE(double[] array1, double[] array2){
	int length1 = array1.length;
	if(length1 != array2.length) {return Double.NaN;}
	double mse = 0;
	int c = 0;
	for (double d1 : array1) {
		mse += Math.pow((d1-array2[c]), 2);
		c++;
	}
	mse /= length1;
	return mse;
}

public static double getMedian(double[] array) {
	int arrayLength = array.length;
	int index = arrayLength/2;
	Arrays.sort(array);
	if((arrayLength & 1) == 0 ) {
		return (array[index] + array[index-1])/2.0;
	}
	return array[index];
}
public static double getMAE(double[] array1, double[] array2){
	int length1 = array1.length;
	if(length1 != array2.length) {return Double.NaN;}
	double mae = 0;
	int c = 0;
	for (double d1 : array1) {
		mae += Math.abs(d1-array2[c]);
		c++;
	}
	mae /= length1;
	return mae;
}
public static double getRMSE(double[] array1, double[] array2) {
	return Math.pow(Statistics.getMSE(array1, array2), 0.5);
}

//Standardized mean squared error
public static double getSMSE(double[] array1, double[] array2, boolean population) {
	int length1 = array1.length;
	if(length1 == 0) {return Double.NaN;}
	if(length1 != array2.length) {return Double.NaN;}
	double std1 = Statistics.getStandardDeviation(array1,population);
	double std2 = Statistics.getStandardDeviation(array2,population);
	double mse = 0;
	int c = 0;
	for (double d1 : array1) {
		double scaled1 = d1/std1;
		double scaled2 = array2[c]/std2;
		mse += Math.pow(scaled1-scaled2, 2);
		c++;
	}
	mse /= length1;
	return mse;
}

//Source: 10.1109/TEVC.2008.926486
//Using the maximum minus the minimum value as the scaling factor
//	Equations 21 to 23
//Re scaled RMSE
//actually use 1 minus NRMSE if choose to re scale by the range
public static double getNRMSE1minus(double[] targetValues, double[] predictedValues){	
	double numValues = targetValues.length;
	if(numValues == 0) {return 1.0;}
	if(numValues != predictedValues.length) {return Double.NaN;}
	
	//rescale by maximum minus minimum
	double minTarget = Double.POSITIVE_INFINITY;
	double maxTarget = Double.NEGATIVE_INFINITY;
	for (double d :  targetValues) {
		if(Double.isNaN(d)) {return Double.NaN;}
		if(minTarget > d){minTarget = d;}
		if(maxTarget < d){maxTarget = d;}
	}
	double minPredicted = Double.POSITIVE_INFINITY;
	double maxPredicted = Double.NEGATIVE_INFINITY;
	for (double d : predictedValues) {
		if(Double.isNaN(d)) {return Double.NaN;}
		if(minPredicted > d){minPredicted = d;}
		if(maxPredicted < d){maxPredicted = d;}
	}
	double rangeTarget = maxTarget - minTarget;
	double rangePredicted = maxPredicted - minPredicted;
	double mse = 0;
	for (int i = 0; i < numValues; i++) {
		double scaledTarget = (targetValues[i] - minTarget)/rangeTarget;
		double scaledPredicted = (predictedValues[i] - minPredicted)/rangePredicted;
		mse += Math.pow(scaledTarget-scaledPredicted, 2);
	}
	mse /= numValues;
	double rmse = Math.pow(mse,0.5);
	//actually use 1 minus NRMSE
	double nrmse = (1-rmse)/(1+rmse);
	return (1.0 - nrmse);
}

public static double getPCC1minusAbs(double[] array0, double[] array1) {
	return 1.0 - Math.abs(Statistics.getPCC(array0, array1));
}

public static double getCovariance(double[] array0, double[] array1,boolean population) {
	int length0 = array0.length;
	if(length0 != array1.length) {return Double.NaN;}
	
	double avg0 = Statistics.getMean(array0);
	double avg1 = Statistics.getMean(array1);
	double cov = 0.0;
	int c = 0;
	for (double d0 : array0) {
		double d1 = array1[c];
		cov += (d0-avg0)*(d1-avg1);
		c++;
	}
	if(population) {return cov/length0;}
	return cov/(length0-1.0);
}

public static double getMean(double[] array) {
	double avg = 0.0;
	for (double d : array) {avg += d;}
	return avg/array.length;
}

//Pearson product-moment correlation coefficient (PCC)
//PCC = COV(x,y)/(STD(x)*STD(y))
//Where COV stands for covariance and STD for standard deviation
public static double getPCC(double[] array0, double[] array1) {	
	//Population and sample give same result because terms associated cancel out
	double covariance = Statistics.getCovariance(array0, array1,true);
	double std0 = Statistics.getStandardDeviation(array0,true);
	double std1 = Statistics.getStandardDeviation(array1,true);
	return covariance/(std0*std1);
}

//Source: https://doi.org/10.1103/PhysRevB.81.144119   equations 5 to 9
//Weighted relative rms deviation
//array1 contains the predicted values, array2 contains the targetValues values
public static double getWeightedRelativeRMSD(double[] targetValues, double[] predictedValues){
  int length = targetValues.length;
  if(length != predictedValues.length) {return Double.NaN;}
  double sumAbsReference = 0;
  for (int i = 0; i < length; i++) {
	sumAbsReference += Math.abs(targetValues[i]);
  }
//  Since very small values are inherently inaccurate, we weight
//  the terms in the error calculations by the magnitudes of reference values
  double value = 0;
  for (int i = 0; i < length; i++) {
	  double refi = targetValues[i];
	  double wi = Math.abs(refi)/sumAbsReference;
	  double diffsq = Math.pow((predictedValues[i]-refi), 2);
//	  epsilon is a small number that prevents over-weighting of numerically
//	  small data. It also prevents division by zero
	  double epsilon = 1E-10;
	  double refisq = Math.pow(refi, 2);
	  value += (wi*diffsq/(refisq+epsilon));
  }
  return Math.sqrt(value);
}

public static double getMaxValue(double[] array) {
	if(array.length == 0){return Double.NaN;}
	double maxValue = Double.NEGATIVE_INFINITY;
	for (double d : array) {
		if(Double.isNaN(d)) {return Double.NaN;}
		if(d > maxValue) {maxValue=d;}
	}
	return maxValue;
}
public static double getMinValue(double[] array) {
	if(array.length == 0){return Double.NaN;}
	double minValue = Double.POSITIVE_INFINITY;
	for (double d : array) {
		if(Double.isNaN(d)) {return Double.NaN;}
		if(d < minValue) {minValue=d;}
	}

	return minValue;
}

public static double[] arrayAdd(double[] array, double value) {
	double[] returnArray = new double[array.length];
	int c = 0;
	for (double d : array) {
		returnArray[c] = d+value;
		c++;
	}
	return returnArray;
}

//Population standard deviation
public static double getStandardDeviation(double[] array, boolean population) {
	return Math.pow(Statistics.getVariance(array,population),0.5);
}

//Population variance
public static double getVariance(double[] array, boolean population) {
	double avg = Statistics.getMean(array);
	double value = 0.0;
	for (double d : array) {
		value += (d-avg)*(d-avg);
	}
	if(population) {return value/array.length;}
	return value/(array.length - 1.0);
}

public static double getPCCCosine(double[] array0, double[] array1) {
	int length0 = array0.length;
	if(length0 != array1.length) {return Double.NaN;}
	
	double mean0 = Statistics.getMean(array0);
	double mean1 = Statistics.getMean(array1);
	double[] array0this = Statistics.arrayAdd(array0, -mean0);
	double[] array1this = Statistics.arrayAdd(array1, -mean1);

	double dotProduct = 0.0;
	int c = 0;
	for (double d : array0this) {dotProduct += (d*array1this[c]);c++;}
	double mag0 = 0.0;
	for (double d : array0this) {mag0 += d*d;}
	mag0 = Math.pow(mag0, 0.5);
	double mag1 = 0.0;
	for (double d : array1this) {mag1 += d*d;}
	mag1 = Math.pow(mag1, 0.5);
	
	return dotProduct/(mag0*mag1);
}

/**
 * Don't use R-squared for Nonlinear Models!
 * 
 *<ul> Why Shouldn't You Use R-squared to Evaluate the Fit of Nonlinear Models?

 *<ul>As you can see, the underlying assumptions for R-squared aren�t true for nonlinear regression. Yet, most statistical software packages still calculate R-squared for nonlinear regression. Calculating this statistic in this context is a dubious practice that produces bad outcomes.

Spiess and Neumeyer* performed thousands of simulations for their study that show how using R-squared to evaluate the fit of nonlinear models leads you to incorrect conclusions. You don't want this!

That's why Minitab doesn't offer R-squared for nonlinear regression.

Specifically, this study found the following about using R-squared with nonlinear regression:
	<ul><li>R-squared tends to be uniformly high for both very bad and very good models.
	<li>R-squared and adjusted R-squared do not always increase for better nonlinear models.
	<li>Using R-squared and adjusted R-squared to choose the final model led to the correct model only 28-43% of the time.

</ul></ul>
Source: The Minitab blog November 24, 2017 (http://blog.minitab.com/blog/adventures-in-statistics-2/why-is-there-no-r-squared-for-nonlinear-regression)
</ul>

 * @param target
 * @param predicted
 * @param population
 * @return
 */
public static double getRsquared(double[] target, double[] predicted,boolean population) {
	double variance = Statistics.getVariance(target,population);
	double mse = Statistics.getMSE(target,predicted);
	return 1.0 - (mse/variance);
}


//Relative RMSE
public static double getRelativeRMSE(double[] targetValues, double[] predictedValues){
	  int length1 = targetValues.length;
	  if(length1 != targetValues.length) {return Double.NaN;}
//	  Relative MSE
	  double rmse = 0;
	  for (int i = 0; i < length1; i++) {
		  double ti = targetValues[i];
		  rmse += Math.pow((ti-predictedValues[i])/ti, 2);
	  }
	  rmse /= length1;
	  return Math.pow(rmse,0.5);
}

/**
 * 
 * @param numRandomIndices
 * @param maxIndex (0 <= index < maxIndex)
 * @return
 */
public static int[] generateRandomIndices(int numRandomIndices, int maxIndex, ArrayList<Integer> mustInclude){
	if(numRandomIndices>maxIndex) {return null;}
	ArrayList<Integer> orderedIndices = new ArrayList<Integer>();
	ArrayList<Integer> randomIndices = new ArrayList<Integer>();
	Integer c = 0;
	while (c < maxIndex) {
		orderedIndices.add(c);
		c = new Integer(c+1);
	}
	if(mustInclude != null) {
		for (Integer integer : mustInclude) {
			randomIndices.add(integer);
			orderedIndices.remove(integer);
		}	
	}
	while(orderedIndices.size()>0) {
		int position = GENERATOR.nextInt(orderedIndices.size());
		int randomIndex = orderedIndices.get(position);
		randomIndices.add(randomIndex);
		orderedIndices.remove(position);
	}
	int[] returnArray = new int[numRandomIndices];
	for (int i = 0; i < returnArray.length; i++) {
		returnArray[i] = randomIndices.get(i);
	}
	return returnArray;
}

//Assumes non-negative values in the data set
//Source: doi:10.1186/1471-2288-5-13
//Equation 12
//n is the size of the sample, m is the mean, a is the minimum value, and b is the maximum value
public static double getVarianceEstimate(double n, double m, double a, double b) {

double term0 = Math.pow(a, 2) + Math.pow(m, 2) + Math.pow(b, 2);
double subterm0 = ((n-3)/2);
double subterm1 = (Math.pow(a+m, 2)+Math.pow(m+b, 2))/4;
double term1 = subterm0*subterm1;
double subterm2 = ((a+2*m+b)/4)+((a-2*m+b)/(4*n));
double term2 = -n*Math.pow(subterm2, 2);

double terms =  term0 + term1 + term2;

double varEst = terms/(n-1);

return varEst;
}

/**
 * Compare double values with specified numerical precision
 * 
 * @param a
 * @param b
 * @return 1 if a > b, 0 if equal, -1 if a < b, Integer.MAXVALUE if not valid numbers

 */
public static int compare(double a, double b) {
	if(Math.abs(a-b) < Statistics.PRECISION) {return 0;}
	if(a<b) {return -1;}
	if(a>b) {return 1;}
	return Integer.MAX_VALUE;
}
public static int compare(double a, double b, double precision) {
	if(Math.abs(a-b) < precision) {return 0;}
	if(a<b) {return -1;}
	if(a>b) {return 1;}
	return Integer.MAX_VALUE;
}

public static double[] sort(double[] array, boolean ascending) {
	double[] arrayCopy = new double[array.length];
	for (int i = 0; i < arrayCopy.length; i++) {arrayCopy[i] = array[i];}
	
	for (int i = 0; i < arrayCopy.length; i++) {
		for (int j = i+1; j < arrayCopy.length; j++) {
			if(ascending) {
				if(arrayCopy[i] > arrayCopy[j]) {
					double temp = arrayCopy[i];
					arrayCopy[i] = arrayCopy[j];
					arrayCopy[j] = temp;
				}
			}
			else{
				if(arrayCopy[i] < arrayCopy[j]) {
					double temp = arrayCopy[i];
					arrayCopy[i] = arrayCopy[j];
					arrayCopy[j] = temp;
				}
			}
		}
	}
	return arrayCopy;
}

public static double[][] getBins(double[] array, int numBins) {
	double min = getMinValue(array);
	double max = getMaxValue(array);
	int[] indices = new int[array.length];
	double[][] returnArray = new double[numBins][];
	
	for (int i = 0; i < array.length; i++) {
		indices[i] = Statistics.getBinNumber(array[i], min, max, numBins);
	}
	
	for (int i = 0; i < numBins; i++) {
		ArrayList<Double> list = new ArrayList<Double>();
		for (int j = 0; j < indices.length; j++) {
			if(indices[j]==i) {
				list.add(array[j]);
			}
		}
		returnArray[i] = new double[list.size()];
		for (int j = 0; j < returnArray[i].length; j++) {
			returnArray[i][j] = list.get(j);
		}
	}
	
	return returnArray;
}

/**
 * Uses a uniform distribution
 * @param value
 * @param min
 * @param max
 * @param numBins
 * @return the index of the bin that contains value (lowLimit <= value < upperLimit). 
 * The first bin has index 0 and last one has index numBins-1
 */
private static int getBinNumber(double value, double min, double max, int numBins) {

	double binSize = (max-min)/numBins;
		
	if(Statistics.compare(value, max)==0) {
		return numBins - 1;
	}
	else if(Statistics.compare(value, min) == -1 || Statistics.compare(value, max) == 1) {
		Status.basic("Out of bounds!");
		return Integer.MIN_VALUE;
	}

	for (int i = 1; i < numBins + 1; i++) {
		if(Statistics.compare(value, (min + i*binSize))==-1) {
			//Belongs to bin i, where the first bin is i=1
			return i-1;
		}
	}
	Status.basic("Out of bounds!");
	return Integer.MIN_VALUE;
}

/**
 * 
 * @param value
 * @param array
 * @return
 */
public static int[] getIndices(double value, double[][] array) {
	for (int i = 0; i < array.length; i++) {
		for (int j = 0; j < array[i].length; j++) {
			if(Statistics.compare(value, array[i][j])==0) {
				return new int[] {i,j};
			}
		}
	}
	return null;
}

/**
 * 
 * @param bins
 * @param i
 * @param greater
 * @return
 */
public static Integer getNearestOccupiedBin(double[][] bins, int i, boolean greater) {
	boolean foundOccupiedBin = false;
	Integer returnIndex = null;
	int c = 1;
	if(!greater) {
		c*=-1;
	}
	while(!foundOccupiedBin) {
		try {
			if(bins[i+c].length > 0) {
				returnIndex = i+c;
				foundOccupiedBin = true;
			}		
		}catch(ArrayIndexOutOfBoundsException e) {
			return null;
		}
		if(greater) {
			c++;
		}
		else {
			c--;
		}
	}
	return returnIndex;
}
public static double[] getPercenErrors(double[] target, double[] predicted) {
	double[] percentErrors = new double[target.length];
	for (int i = 0; i < percentErrors.length; i++) {
		double t = target[i];
		percentErrors[i] = 100*(predicted[i]-t)/t;
	}
	return percentErrors;
}

}
