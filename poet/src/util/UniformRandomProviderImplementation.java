package util;

import java.util.Random;

import org.apache.commons.rng.UniformRandomProvider;

public class UniformRandomProviderImplementation implements UniformRandomProvider {
	Random GENERATOR = new Random();
	public UniformRandomProviderImplementation() {
		
	}
	@Override
	public boolean nextBoolean() {
		return GENERATOR.nextBoolean();
	}

	@Override
	public void nextBytes(byte[] arg0) {
		System.out.println("!!");
	}

	@Override
	public void nextBytes(byte[] arg0, int arg1, int arg2) {
		System.out.println("!!");
		
	}

	@Override
	public double nextDouble() {
		return GENERATOR.nextDouble();
	}

	@Override
	public float nextFloat() {
		return GENERATOR.nextFloat();
	}

	@Override
	public int nextInt() {
		return GENERATOR.nextInt();
	}

	@Override
	public int nextInt(int arg0) {
		System.out.println("!!");
		return GENERATOR.nextInt(arg0);
	}

	@Override
	public long nextLong() {
		return GENERATOR.nextLong();
	}

	@Override
	public long nextLong(long arg0) {
		System.out.println("No argument for nextLong(long) !!");
		return GENERATOR.nextLong();
	}

}

