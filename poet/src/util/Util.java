package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import evaluator.StructureNet;
import matsci.Species;
import matsci.io.app.log.Status;
import matsci.io.vasp.POSCAR;
import matsci.location.Coordinates;
import matsci.location.basis.CartesianBasis;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.util.MSMath;

public class Util {

public static Integer find(double[] coordinates, double[][] allCoordinates) {
	double precision = 1E-5;
	for (int j = 0; j < allCoordinates.length; j++) {
		boolean x1 = Statistics.compare(coordinates[0], allCoordinates[j][0],precision)==0;
		boolean y1 = Statistics.compare(coordinates[1], allCoordinates[j][1],precision)==0;
		boolean z1 = Statistics.compare(coordinates[2], allCoordinates[j][2],precision)==0;
		if((x1&y1&z1)) {
			return j;
		}
	}
	Status.basic("Coordinates were not found !");
	return null;
}


/**
 * Get the rotation matrix for a counter-clockwise rotation by an angle around an axis. 
 * Then, you can use this rotation matrix to rotate a vector 
 * by this angle around this axis by multiplying this matrix by the vector
 * @param angle in radians
 * @param axis X=0, Y=1, Z=2
 * @return rotation matrix
 */
public static double[][] getRotationMatrix(double angle,int axis) {
	double cosTheta = Math.cos(angle);
	double sinTheta = Math.sin(angle);
	if(axis==0) {
		return new double[][] {{1, 0, 0}, {0, cosTheta, -sinTheta}, {0, sinTheta,cosTheta}};
	}
	else if(axis==1) {
		return new double[][]{{cosTheta,0,sinTheta},{0,1,0},{-sinTheta,0,cosTheta}};
	}
	else if(axis == 2) {
		return new double[][]{{cosTheta,-sinTheta,0},{sinTheta,cosTheta,0},{0,0,1}};
	}
	return null;
}

public static double getAngle(double[] vector1, double[] vector2) {
	double magV1 = MSMath.magnitude(vector1);
	double magV2 = MSMath.magnitude(vector2);
	if(Statistics.compare(magV1, 0)==0 || Statistics.compare(magV2, 0)==0) {
		return 0.0;
	}
	return Math.acos(MSMath.dotProduct(vector1, vector2)/(magV1*magV2));
}

/**
 * Source: Stack exchange mathematics: "Calculate Rotation Matrix to align Vector A to Vector B in 3d?" 
 * @param normalizedVector1
 * @param normalizedVector2
 * @return
 */
public static double[][] getRotationMatrix(double[] normalizedVector1, double[] normalizedVector2){
	double[][] identity = new double[][] {
		new double[] {1,0,0},
		new double[] {0,1,0},
		new double[] {0,0,1}
	};
	
	double cosangle = MSMath.dotProduct(normalizedVector1, normalizedVector2);
	if(Statistics.compare(cosangle, 1) == 0) {
		return identity;
	}
	if(Statistics.compare(cosangle, -1) == 0) {
		return MSMath.arrayMultiply(identity, -1.0);
	}
		
	double[] v = MSMath.crossProduct(normalizedVector1, normalizedVector2);
	
	double s = MSMath.magnitude(v);
	double c = MSMath.dotProduct(normalizedVector1, normalizedVector2);
	double[][] sscpv = getSkewSymmetricCrossProduct(v);
	double[][] sscpvsq = MSMath.matrixMultiply(sscpv, sscpv);

	double[][] term1 = MSMath.arrayAdd(identity, sscpv);
	double[][] term2 = MSMath.arrayMultiply(sscpvsq, ((1-c)/(s*s)));
	double[][] matrix = MSMath.arrayAdd(term1, term2);
	return matrix;
}

public static double[][] getSkewSymmetricCrossProduct(double[] vector3D) {
	return new double[][] {
		new double[] {0.000000000, -vector3D[2], vector3D[1]},
		new double[] {vector3D[2], 0.000000000, -vector3D[0]},
		new double[] {-vector3D[1], vector3D[0], 0.000000000}
	};
}

/**
 * Map of iterations (0,1,2,...). Each iteration contains maps for energy, coordinates and forces, and forces on cell (-stresses)
 * The keys for each iteration are: Energy, Coordinates_forces, Force_cell
 * @param outcarPath
 * @return map: each key is an iteration number
 * @throws IOException 
 * @throws NumberFormatException 
 */
public static Map<Integer, Map<String, double[][]>> getIterationsOUTCAR(String outcarPathName) throws IOException {
	Map<Integer, Map<String, double[][]>> map = new HashMap<Integer, Map<String, double[][]>>();
	BufferedReader br = new BufferedReader(new FileReader(outcarPathName));
	
	//Import the file into a map
	String line;
	int iteration_step = 1;
	int numions = 0;
	while((line = br.readLine()) != null){
		if(line.contains("ions per type")) {
			String[] a = line.split("\\s+");
			numions = Integer.parseInt(a[5]);
		}
//		if(!line.contains("TOTAL-FORCE")) {
		if(!line.contains("=-STRESS")) {
			continue;			
		}
		for (int i = 0; i < 13; i++) {
			br.readLine();
		}
		line = br.readLine();
		if(!line.contains("in kB")) {
			continue;
		}
		String[] splitStress = line.split("\\s+");
		double[][] stresses = new double[1][6];
		for (int i = 0; i < stresses[0].length; i++) {
			stresses[0][i] = Double.parseDouble(splitStress[i+3]);
		}
		for (int i = 0; i < 44; i++) {
			br.readLine();
		}
		line = br.readLine();
		if(!line.contains("TOTAL-FORCE")) {
			continue;
		}
		br.readLine();
		line = br.readLine();
		ArrayList<String> lines = new ArrayList<String>();
		int c0 = 0;
		while(c0 < numions) {
			lines.add(line);
			line = br.readLine();
			c0++;
		}

		double[][] coordinates_forces = new double[lines.size()][6];
		int c = 0;
		boolean moveToNext = false;
		for (String line1 : lines) {
			String[] split = line1.split("\\s+");
			for (int i = 0; i < 6; i++) {
				try {
					coordinates_forces[c][i] = Double.parseDouble(split[i+1]);
				}catch(NumberFormatException | ArrayIndexOutOfBoundsException e ) {
					//Clear the map
					map.put(iteration_step, new HashMap<String, double[][]>());
					//Analyze the next iteration step
					iteration_step ++;
					moveToNext = true;
					break;
				}
			}
			if(moveToNext) {
				break;
			}
			c++;
		}
		if(moveToNext) {
			continue;
		}
		//Read energy
		for (int i = 0; i < 11; i++) {
			br.readLine();
		}
		line = br.readLine();
		if(!(line.contains("energy") && line.contains("without") && line.contains("entropy=") && line.contains("energy(sigma->0)"))) {
			//Clear the map
			map.put(iteration_step, new HashMap<String, double[][]>());
			//Analyze the next iteration step
			iteration_step ++;
			continue;
		}
		double energy = Double.parseDouble(line.split("\\s+")[7]);
		double[][] energy_array = new double[1][1];
		energy_array[0][0] = energy;
		map.put(iteration_step, new HashMap<String, double[][]>());
		map.get(iteration_step).put("Energy", energy_array);
		//Put energy and force
		map.get(iteration_step).put("Coordinates_forces", coordinates_forces);
		//Put stresses
		map.get(iteration_step).put("Stress", stresses);
		iteration_step++;
	}
	br.close();
	return map;
}
/**
 * 
 * @param absForceLessThan use null if you want the last iteration
 * @param path_parent
 * @param outcarsName
 * @param newDirName
 * @throws NumberFormatException
 * @throws IOException
 */
public static void printIterationsOUTCARs(Double absForceLessThan, String path, String newDirName, double maxEnergy) throws NumberFormatException, IOException {
	for (int c = 1; c < 2; c++) {
		if(!(new File(path+c)).exists()) {
			continue;
		}
		//continue if the job didn't finish successfully or if the folder doesn't exist
		BufferedReader br = new BufferedReader(new FileReader(path+c+"/OUTCAR"));
	    String lastLine = "";
	    String sCurrentLine = null;
	    while ((sCurrentLine = br.readLine()) != null) {
	        lastLine = sCurrentLine;
	    }
	    br.close();
		if(!lastLine.contains("Voluntary context switches:")) {
			continue;
		}
		Map<Integer, Map<String, double[][]>> map = getIterationsOUTCAR(path+c+"/OUTCAR");
		for (int key = 1; key < map.keySet().size() + 1; key++) {
			if(absForceLessThan == null) {
				key = map.keySet().size();
			}
			double[][] coords_forces = map.get(key).get("Coordinates_forces");
			if(coords_forces == null) {
				continue;
			}
			double absMax = Double.NEGATIVE_INFINITY;
			for (int i = 0; i < coords_forces.length; i++) {
				double absMax_i = Statistics.getMaxValue(new double[] {Math.abs(coords_forces[i][3]),Math.abs(coords_forces[i][4]),Math.abs(coords_forces[i][5])});
				if(absMax_i > absMax) {
					absMax = absMax_i;
				}
			}
			boolean b1 = absForceLessThan == null ? true : absMax < absForceLessThan;
			if(!b1) {
				continue;
			}
			//Do not include this cluster if its energy is greater than max energy
			if(Statistics.compare(map.get(key).get("Energy")[0][0], maxEnergy) == 1) {
				break;
			}
			System.out.println(c + " " +map.get(key).get("Energy")[0][0]);

			POSCAR p = new POSCAR(path+c+"/POSCAR");
			StructureBuilder sb = new StructureBuilder(p);
			Species s = sb.getSiteSpecies(0);
			sb.clearSites();
			for (int i = 0; i < coords_forces.length; i++) {
				sb.addSite(new Coordinates(new double[] {coords_forces[i][0],coords_forces[i][1],coords_forces[i][2]}, CartesianBasis.getInstance()), s);
			}
			//Write new POSCAR
			String path1 = path+newDirName;
			(new POSCAR(sb)).writeFile(path1+c+".vasp");
			
		    File energies_file = new File(path1+"E0.data");
		    //create the file if it doesn't exist
		    if(!energies_file.exists()){
		    	energies_file.createNewFile();
		    }
    		PrintWriter out_energies = new PrintWriter(new BufferedWriter(new FileWriter(energies_file,true)));
    		out_energies.println(map.get(key).get("Energy")[0][0]);
    		out_energies.close();
    		
		    File forces_file = new File(path1+"F_coord.data");
		    //create the file if it doesn't exist
		    if(!forces_file.exists()){
		    	forces_file.createNewFile();
		    }
    		PrintWriter out_forces = new PrintWriter(new BufferedWriter(new FileWriter(forces_file,true)));
			for (int i = 0; i < coords_forces.length; i++) {
				for (int j = 0; j < coords_forces[i].length; j++) {
					out_forces.print(Double.toString(coords_forces[i][j])+" ");
				}
				out_forces.println();
			}
    		out_forces.close();

			break;
		}

	}
}
public static void printBrokenBondsPerArea(String[] args) {
	
	String[] miller_indices = new String[] {"1 0 0","1 1 0","1 1 1","2 1 0","2 1 1","2 2 1","3 1 0","3 1 1","3 2 0","3 2 1","3 2 2","3 3 1","3 3 2"};
	Status.basic("Assuming indices in order "+Arrays.toString(miller_indices));
	String data_dir = args[1];//"C:/Users/Username/poet/poet_run/data/DFT_Cu/";
	String surface_dir = data_dir+"lowIndexSurfaces/";
	Structure fcc_struct = new Structure(new POSCAR(data_dir+"0K/FCC.vasp"));

	double distance_neigh = fcc_struct.getDefiningSite(0).distanceFrom(fcc_struct.getNearestNeighbors(fcc_struct.getDefiningSite(0))[0]);
	
	Status.basic("index" +", " + "miller_indices"+", "+"broken bonds per area (1/angs^2)"+", "+"number of broken bonds"+", "+"surface area (angs^2)"+", "+"number of defining sites in the slab"+", "+"number of surface sites");

	for (int i = 0; i < miller_indices.length; i++) {
		Structure slab = StructureNet.removeVacancies(new Structure(new POSCAR(surface_dir+(i+1)+".vasp")));
		int numBrokenBonds = 0;
		int numSurfaceSites = 0;
		for (int j = 0; j < slab.getDefiningSites().length; j++) {
			int numNeigh = slab.getNearbySites(slab.getDefiningSites()[j].getCoords(), distance_neigh*1.1, false).length;
			if(numNeigh < 12) {
				numBrokenBonds += 12 - numNeigh;
				numSurfaceSites += 1;
			}
			else if(numNeigh>12) {
				Status.basic(numNeigh+"!");
			}
		}
		numBrokenBonds /= 2.0;
		numSurfaceSites /= 2.0;
		double surfarea = MSMath.magnitude(MSMath.crossProduct(slab.getCellVectors()[0].getCartesianDirection(), slab.getCellVectors()[1].getCartesianDirection()));
		double brokenBondsPerArea = numBrokenBonds/surfarea;
		Status.basic(i+1 +", " + miller_indices[i]+", "+brokenBondsPerArea+", "+numBrokenBonds+", "+surfarea+", "+slab.numDefiningSites()+", "+numSurfaceSites);
	}
	System.exit(0);

}
/**
 * Assumes conventional cubic structure and only one type of atom
 * @param density in g/cm^3
 * @param molecularWeight in g/mol
 * @param numAtomsPerCell
 * @return cubic lattice parameter in Angstrom
 */
public static double getLatticeParameterCubic(double density, double molecularWeight, int numAtomsPerUnitCell) {	
	double mass = numAtomsPerUnitCell*molecularWeight/(6.0221409E23); //grams
	double volume = mass/density/(1E-30*1E6); //Angstrom^3
	double latticeParameter = Math.pow(volume, 1.0/3.0); // Angstrom
	return latticeParameter;
}
/**
 * Assumes only one type of element
 * @param poscarFullPath
 * @param molecularWeight in g/mol
 * @return density in g/cm^3
 */
public static double getDensity(String poscarFullPath, double molecularWeight) {
	Structure structure = new Structure(new POSCAR(poscarFullPath));
	double mass = structure.numDefiningSites()*molecularWeight/(6.0221409E23); //grams
	double volume = structure.getDefiningVolume()*(1E-30*1E6);//cm^3
	double density = mass/volume;
	return density;

}
}