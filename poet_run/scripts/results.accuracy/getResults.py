#!/usr/bin/python

#Description:
# Generate the images of the graphs (trees and simplified expression) for every snapshot
# Generate the error plots of the last snapshot
# Generate the learning curves

#Arguments:
# The parent directory that contains the runs and the results directories
# The run number

import sys
import subprocess
import os

runNum = sys.argv[1]

#The parent directory that contains the runs and results directories
parentDir = sys.argv[2] # os.getcwd()

#absolute path of the directory that contains the snapshots
snaphotsDirectory = parentDir + "/results/" + runNum +"/globalTrainingFrontierMSE"

#cp .lock files to original file names and remove .lock directories
copyandremovelockfilesPath = parentDir + "/scripts/results.accuracy/copyAndRemoveLockFiles"
subprocess.call(["bash", copyandremovelockfilesPath, runNum, parentDir])

#get the snapshots from the run
getSnapshotsPath = parentDir + "/scripts/results.accuracy/copySnapshot"
subprocess.call(["bash", getSnapshotsPath, runNum, parentDir])

#write the DOT files
#jarPath = parentDir + "/bashscripts/graphs_and_LaTeX/writeDOTsnapshots.jar"
#subprocess.call(["java","-jar", jarPath, snaphotsDirectory])

#TODO uncomment
#write the LaTeX expressions
#mathematicaBash = parentDir + "/scripts/graphs_and_LaTeX/getLaTeX"
#subprocess.call(["bash", mathematicaBash, snaphotsDirectory, parentDir])


#save images of the DOT trees and the simplified LaTeX expressions
#saveImagesScript = parentDir + "/bashscripts/graphs_and_LaTeX/getGraphs.py"
##subprocess.call(["python", saveImagesScript, snaphotsDirectory])

#Arguments
#  1. The absolute path of the directory that contains the directories runs and results
#  2. The name of the global population (e.g., globalTrainingFrontierMSE)
#  3. The error metric
#  4. The name of the directory that contains the file snapshot (e.g., snapshot1_data or snapshotuptodate_data)
#  5. The run number
#  6. Either "predictedVsTarget" or "iterations"

#generate data for the predicted vs target plots of the last snapshot
jarPath = parentDir + "/poet.jar"

subprocess.call(["java", "-Xmx7800m", "-jar", jarPath, parentDir,"printReport",
  "globalTrainingFrontierMSE","MAE","snapshotuptodate_data",runNum,"predictedVsTarget"])

#generate data for plots of error vs iteration (or time)
#subprocess.call(["java", "-Xmx7800m", "-jar", jarPath, parentDir,
#  "globalTrainingFrontierMSE","MAE","snapshotuptodate_data",runNum,"iterations"])
